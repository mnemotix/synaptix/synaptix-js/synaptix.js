/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import uniq from "lodash/uniq";

export default class ModelDefinitionsRegister {
  /**
   * @type {(typeof ModelDefinitionAbstract)[]}
   */
  modelDefinitions = [];

  /**
   *
   * @param {(typeof ModelDefinitionAbstract)[]} modelDefinitions
   */
  constructor(modelDefinitions = []) {
    this.addModelDefinitions(modelDefinitions);
  }

  /**
   * @return {(typeof ModelDefinitionAbstract)[]}
   */
  getModelDefinitions() {
    return this.modelDefinitions;
  }

  /**
   * @param {(typeof ModelDefinitionAbstract)[]} modelDefinitions
   */
  addModelDefinitions(modelDefinitions) {
    for(let modelDefinition of modelDefinitions) {
      if (modelDefinition.substituteModelDefinition()){
        this.substituteModelDefinitions(modelDefinition.substituteModelDefinition(), modelDefinition);
      } else {
        this.modelDefinitions.push(modelDefinition);
      }
    }
  }

  /**
   * @param {typeof ModelDefinitionAbstract} targetedModelDefinition
   * @param {typeof ModelDefinitionAbstract} replacementModelDefinition
   */
  substituteModelDefinitions(targetedModelDefinition, replacementModelDefinition) {
    let indexOfTarget = this.modelDefinitions.indexOf(targetedModelDefinition);

    if (indexOfTarget >= 0) {
      this.modelDefinitions.splice(indexOfTarget, 1, replacementModelDefinition);
    }
  }

  /**
   * @param {string} nodeType
   * @return {typeof ModelDefinitionAbstract}
   */
  getModelDefinitionForNodeType(nodeType) {
    let modelDefinition = this.modelDefinitions.find(modelDefinition => modelDefinition.getNodeType() === nodeType);

    if (!modelDefinition) {
      throw new Error(`ModelDefinition is not found for "${nodeType}" node type. Have you registered such ModelDefinition class in ModelDefinitionsRegister instance ?`);
    }

    return modelDefinition;
  }

  /**
   * @param {string|string[]} rdfType
   * @param {boolean} [throwExceptionIfNotFound=true]
   * @return {typeof ModelDefinitionAbstract}
   */
  getModelDefinitionForRdfType(rdfType, throwExceptionIfNotFound = true) {
    if (typeof rdfType === "string"){
      rdfType = [rdfType];
    }

    let modelDefinition = this.modelDefinitions.find(modelDefinition => rdfType.includes(modelDefinition.getRdfType()));

    if (!modelDefinition && throwExceptionIfNotFound) {
      throw new Error(`ModelDefinition is not found for "${rdfType}" RDF type. Have you registered such ModelDefinition class in ModelDefinitionsRegister instance ?`);
    }

    return modelDefinition;
  }

  /**
   * @param graphQLType
   * @return {boolean}
   */
  isModelDefinitionExistsForGraphQLType(graphQLType){
    return !!this.modelDefinitions.find(modelDefinition => modelDefinition.getGraphQLType() === graphQLType);
  }

  /**
   * @param {string} graphQLType
   * @return {typeof ModelDefinitionAbstract}
   */
  getModelDefinitionForGraphQLType(graphQLType) {
    let modelDefinition = this.modelDefinitions.find(modelDefinition => {
      let types = [graphQLType];

      // This dirty piece of code is here to be compatible with
      // legacy naming (Interface string was only present in GraphQL type,
      // not elsewhere (inputs, edges, modelDefinition...)
      if (graphQLType.includes("Interface")){
        types.push(graphQLType.replace("Interface", ""));
      }

      return types.includes(modelDefinition.getGraphQLType());
    });

    if (!modelDefinition) {
      throw new Error(`ModelDefinition is not found for "${graphQLType}" graphQL type. Have you registered such ModelDefinition class in ModelDefinitionsRegister instance ?`);
    }

    return modelDefinition;
  }

  /**
   * @param {string} documentType
   * @return {typeof ModelDefinitionAbstract}
   */
  getModelDefinitionForDocumentType(documentType) {
    let modelDefinition = this.modelDefinitions.find(modelDefinition => modelDefinition.getIndexType() === documentType);

    if (!modelDefinition) {
      throw new Error(`ModelDefinition is not found for "${documentType}" index type. Have you registered such ModelDefinition class in ModelDefinitionsRegister instance ?`);
    }

    return modelDefinition;
  }

  /**
   * Get a corresponding GraphQL type.
   *
   * @param {string} nodeType
   */
  getGraphQLTypeForNodeType(nodeType) {
    return this.getModelDefinitionForNodeType(nodeType).getGraphQLType();
  }

  /**
   * Get a corresponding GraphQL type.
   *
   * @param {string} rdfType
   */
  getGraphQLTypeForRdfType(rdfType) {
    return this.getModelDefinitionForRdfType(rdfType).getGraphQLType();
  }

  /**
   * Get a ModelDefinition for a RDF instance base URI
   * @param {string} nodesNamespaceURI - Namespace of nodes.
   * @param {string} uri - Node URI
   * @param {function} [nodesTypeFormatter] - This function tweaks the type into a urified version. By default formatter returns the lowercase format.
   * @return {typeof ModelDefinitionAbstract}
   */
  getModelDefinitionForURI({nodesNamespaceURI, uri, nodesTypeFormatter}) {
    let modelDefinition = this.modelDefinitions.find(modelDefinition => modelDefinition.isMatchingURI({
      uri,
      nodesNamespaceURI,
      nodeTypeFormatter: nodesTypeFormatter
    }));

    return modelDefinition;
  }

  /**
   * Get a GraphQL Type for a RDF instance base URI
   *
   * @param {string} nodesNamespaceURI
   * @param {string} uri
   * @param {function} [nodesTypeFormatter] - This function tweaks the type into a urified version. By default formatter returns the lowercase format.
   */
  getGraphQLTypeForURI({nodesNamespaceURI, uri, nodesTypeFormatter}) {
    return this.getModelDefinitionForURI({nodesNamespaceURI, uri, nodesTypeFormatter})?.getGraphQLType();
  }


  /**
   * Returns the mapping of RDF prefixes
   */
  getRdfPrefixesMapping() {
    let mapping = {};

    for (let modelDefinition of this.modelDefinitions) {
      mapping = Object.assign(mapping, modelDefinition.getRdfPrefixesMapping())
    }

    return mapping;
  }

  /**
   * Returns ModelDefinition for logged user related person
   * @return {typeof ModelDefinitionAbstract }
   */
  getModelDefinitionForLoggedUserPerson(){
    return this._modelDefinitionForLoggedUserPerson;
  }

  /**
   * Returns ModelDefinition for logged user related person.
   *
   * @example This is usefull if mnx:Person is specialized in a project (with extra properties).
   *
   * @param {typeof ModelDefinitionAbstract } modelDefinitionForLoggedUserPerson
   */
  setModelDefinitionForLoggedUserPerson(modelDefinitionForLoggedUserPerson){
    return this._modelDefinitionForLoggedUserPerson = modelDefinitionForLoggedUserPerson;
  }

  /**
   * Reconstruct the inheritance hierarchy to gather all model definitions
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @return {typeof ModelDefinitionAbstract[]}
   */
  getInheritedModelDefinitionsFor(modelDefinition) {
    return uniq(this.modelDefinitions.filter(md => md.getParentDefinitions().includes(modelDefinition)));
  }
}
