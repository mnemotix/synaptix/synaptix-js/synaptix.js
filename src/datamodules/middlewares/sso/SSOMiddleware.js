/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

export default class SSOMiddleware {
  /**
   * @param {SSOUser} user - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   * @param {object} requestParams - Parameters passed in registration request body.
   */
  async afterRegister({user, ssoApiClient, datastoreSession, requestParams}){}

  /**
   * @param {SSOUser} user - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   */
  async afterLogin({user, ssoApiClient, datastoreSession}){}

  /**
   * @param {SSOUser} user - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   */
  async afterLogout({user, ssoApiClient, datastoreSession}){}

  /**
   * @param {SSOUser} user - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   */
  async afterAccountValidation({user, ssoApiClient, datastoreSession}){}

  /**
   * @param {SSOUser} userId - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   * @param {object} requestParams - Parameters passed in registration request body.
   * @param {boolean} [permanent] - GDPR "right to erasure"
   */
  async afterUnregister({userId, ssoApiClient, datastoreSession, requestParams, permanent}){}

  /**
   * @param {SSOUser} userId - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   * @param {object} requestParams - Parameters passed in registration request body.
   */
  async afterAccountEnabling({userId, ssoApiClient, datastoreSession, requestParams}){}

  /**
   * @param {SSOUser} userId - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   * @param {object} requestParams - Parameters passed in registration request body.
   */
  async afterAccountDisabling({userId, ssoApiClient, datastoreSession, requestParams}){}

}