An index matcher is an helper class to describe index store queries used to access data. Generally it extends the default class [DefaultIndexMatcher](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/matchers/DefaultIndexMatcher.js)

For example let's create a `BookIndexMatcher`.

```javascript
 import {DefaultIndexMatcher} from "@mnemotix/synaptix.js";
 
 export default class ResourceIndexMatcher extends DefaultIndexMatcher{
   getSortingMapping(){
      // Return a map of sorting aliases (used in graphQL sortings parameters) and the real indices paths 
   }
 
   getFulltextQueryFragment(qs){
     // Return the query used to apply a fulltext search on documents.
   }
 
   getFilterByKeyValue(alias, value){
     // Return the query for a filter alias/value (user in graphQL filters parameters)
   }
 
   getAggregations(){
     // Return the query to formalize aggregations.
   }
 }
```

So for example :

```javascript
import {DefaultIndexMatcher} from "synpatix.js";

export default class BookIndexMatcher extends DefaultIndexMatcher{
  getSortingMapping(){
    return {
      title: 'titles.values.raw',  // --> sort by title
      author: 'authors.fullName'   // --> sort by author name
    };
  }

  getFulltextQueryFragment(qs){
    return {
      "query_string" : {  // --> Basic query string (may be way more complex)
        "query" : qs      //     This is the default behavior when this method is not defined.
      }
    };
  }

  getFilterByKeyValue(key, value){
    switch (key){
      case "author":
        return {
          "nested": {                                         //
            "path": "authors.id",                             // --> Passing filters:["author:AUTHOR_ID"] in graphQL is translated into
            "query": this.getTermFilter("authors.id", value)  //     that query.
          }
        };
      default:
        return super.getFilterByKeyValue(key, value);
    }
  }

  getAggregations(){
    return {
      "authors" : {                               //
        "nested" : {                              //  An aggregation on authors names.
          "path" : "authors"                      //
        },
        "aggs" :{
          "nesting" : {
            "terms": {
              "field": "authors.fullName",
              "size" : 100
            }
          }
        }
      }
    };
  }
}
```

##### Using "pathInIndex" LinkDefinition property.

Note that the property `pathInIndex` can be used with some different ways.

 -  Passing a `string` means that the data is accessible directly in the related property of the document in index.

For example :

```javascript
new LinkDefinition({
    linkName: 'authors',
    pathInIndex: 'authors',
    //...
})
```

Complies with that book document structure :

```json
// Book document
{
  "_id": "B001",
  "_source" : {
    "titles" : [
      {
        "lang" : "fr",
        "value": "..."
      }
    ],
    "authors": [
      {
        "id": "P001",
        "fullName": "..."
      }
    ]
  }
}
```

 - Passing a [UseIndexMatcherOfDefinition](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/toolkit/definitions/LinkDefinition.js#UseIndexMatcherOfDefinition) delegates the query to another index matcher.

For example :

```javascript
import {LinkDefinition, UseIndexMatcherOfDefinition, Ontologies} from "@mnemotix/synaptix.js"

new LinkDefinition({
    linkName: 'authors',
    pathInIndex:  new UseIndexMatcherOfDefinition({
        filterName: 'books',
        useIndexMatcherOf: Ontologies.foaf.ModelDefinitions.PersonDefinition
    })
    //...
})
```

Complies with that book and person document structure :

```json
// Book document
{
  "_id": "B001",
  "_source" : {
    "titles" : [
      {
        "lang" : "fr",
        "value": "..."
      }
    ]
  }
}
```
```json
// Person document
{
  "_id": "P001",
  "_source" : {
    "fullName": "...",
    "books" : [{
      "id": "B001",
        "titles" : [{
          "lang" : "fr",
          "value": "..."
        }
      ]
    }]
  }
}

```