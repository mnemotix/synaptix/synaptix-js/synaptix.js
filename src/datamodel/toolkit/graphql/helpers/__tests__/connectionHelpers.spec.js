/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  connectionFromArray,
  connectionFromArraySlice,
  connectionFromCollection,
  toGlobalId,
  offsetToCursor,cursorToOffset, fromGlobalId, getOffsetWithDefault
} from "../connectionHelpers";
import {Collection} from "../../../models/Collection";

describe("Test graphQL connection helpers", () => {
  const objects = [{id: "foo1"}, {id: "foo2"}, {id: "foo3"} ];
  const edges = objects.map((object, index) => ({
    cursor: "offset:" + index,
    node: object
  }));

  it('should test connectionFromArraySlice without args',  () => {
    expect(connectionFromArraySlice({
      arraySlice : [...objects],
      totalCount: 30
    })).toEqual({
      totalCount: 30,
      aggregations: [],
      edges: edges,
      pageInfo: {
        endCursor: edges[edges.length -1].cursor,
        hasNextPage: false,
        hasPreviousPage: false,
        startCursor: edges[0].cursor
      }
    });
  });

  it('should test connectionFromArraySlice with pagination window',  () => {
    expect(connectionFromArraySlice({
      arraySlice : objects.slice(1), // Caution : Unlike Relay flow, in Synaptix at this step, returned objects are already windowed from data source.
      args: {                        //   |
        first: 1,                    //   |
        after: "offset:1"            // <=|
      },
      totalCount: 30
    })).toEqual({
      totalCount: 30,
      aggregations: [],
      edges: [{
        cursor: "offset:2",
        node: {
          id: "foo2"
        }
      }],
      pageInfo: {
        endCursor: "offset:2",
        hasNextPage: true,
        hasPreviousPage: true,
        startCursor: "offset:2",
      }
    });
  });

  it('should test connectionFromArray without args',  () => {
    expect(connectionFromArray({
      objects,
      args: {}
    })).toEqual({
      totalCount: undefined,
      aggregations: [],
      edges: edges,
      pageInfo: {
        endCursor: edges[edges.length -1].cursor,
        hasNextPage: false,
        hasPreviousPage: false,
        startCursor: edges[0].cursor
      }
    });
  });

  it('should test connectionFromArray with pagination window',  () => {
    expect(connectionFromArray({
      objects : objects.slice(1), // Caution : Unlike Relay flow, in Synaptix at this step, returned objects are already windowed from data source.
      args: {                        //   |
        first: 1,                    //   |
        after: "offset:1"            // <=|
      },
    })).toEqual({
      totalCount: undefined,
      aggregations: [],
      edges: [{
        cursor: "offset:2",
        node: {
          id: "foo2"
        }
      }],
      pageInfo: {
        endCursor: "offset:2",
        hasNextPage: true,
        hasPreviousPage: true,
        startCursor: "offset:2",
      }
    });
  });

  it('should test connectionFromConnection without args',  () => {
    expect(connectionFromCollection({
      collection : new Collection({objects: objects, totalCount: 30}),
    })).toEqual({
      totalCount: 30,
      aggregations: [],
      edges: edges,
      pageInfo: {
        endCursor: edges[edges.length -1].cursor,
        hasNextPage: false,
        hasPreviousPage: false,
        startCursor: edges[0].cursor
      }
    });
  });

  it('should test connectionFromConnection with pagination window',  () => {
    expect(connectionFromCollection({
      collection : new Collection({objects: objects.slice(1), totalCount: 30}), // Caution : Unlike Relay flow, in Synaptix at this step, returned objects are already windowed from data source.
      args: {                        //   |
        first: 1,                    //   |
        after: "offset:1"            // <=|
      },
    })).toEqual({
      totalCount: 30,
      aggregations: [],
      edges: [{
        cursor: "offset:2",
        node: {
          id: "foo2"
        }
      }],
      pageInfo: {
        endCursor: "offset:2",
        hasNextPage: true,
        hasPreviousPage: true,
        startCursor: "offset:2",
      }
    });
  });
});
