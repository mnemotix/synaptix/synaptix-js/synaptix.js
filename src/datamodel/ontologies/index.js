/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {DataModel} from "../DataModel";
import {mnxAgentDataModel, MnxAgentDefinitions} from "./mnx-agent";
import {mnxCommonDataModel, MnxCommonDefinitions} from "./mnx-common";
import {mnxGeoDataModel, MnxGeoDefinitions} from "./mnx-geo";
import {mnxTimeDataModel, MnxTimeDefinitions} from "./mnx-time";
import {mnxProjectDataModel, MnxProjectDefinitions} from "./mnx-project";
import {mnxContributionDataModel, MnxContributionDefinitions} from "./mnx-contribution";
import {mnxSkosDataModel, MnxSkosDefinitions} from "./mnx-skos";
import {mnxAclDataModel, MnxAclDefinitions} from "./mnx-acl";

export let MnxOntologies = {
  mnxCommon: {
    name: "mnx-common",
    ModelDefinitions: MnxCommonDefinitions,
  },
  mnxAgent: {
    name: "mnx-agent",
    ModelDefinitions: MnxAgentDefinitions,
  },
  mnxGeo: {
    name: "mnx-geo",
    ModelDefinitions: MnxGeoDefinitions,
  },
  mnxTime: {
    name: "mnx-time",
    ModelDefinitions: MnxTimeDefinitions,
  },
  mnxProject: {
    name: "mnx-project",
    ModelDefinitions: MnxProjectDefinitions
  },
  mnxContribution: {
    name: "mnx-contribution",
    ModelDefinitions: MnxContributionDefinitions
  },
  mnxSkos: {
    name: "mnx-skos",
    ModelDefinitions: MnxSkosDefinitions
  },
  mnxAcl: {
    name: "mnx-acl",
    ModelDefinitions: MnxAclDefinitions
  },
};

export let MnxDatamodels = {
  mnxCommonDataModel,
  mnxGeoDataModel,
  mnxTimeDataModel,
  mnxContributionDataModel,
  mnxAgentDataModel,
  mnxProjectDataModel,
  mnxSkosDataModel,
  mnxAclDataModel,
  // Bundled
  mnxCoreDataModel: (new DataModel()).mergeWithDataModels([mnxCommonDataModel, mnxGeoDataModel, mnxTimeDataModel, mnxContributionDataModel, mnxSkosDataModel, mnxAclDataModel, mnxAgentDataModel])
};