/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import GraphQLInterfaceDefinition from "../../toolkit/graphql/schema/GraphQLInterfaceDefinition";
import { LinkPath } from "../../toolkit/utils/linkPath";
import TaggingDefinition from "./TaggingDefinition";
import ConceptDefinition from "../mnx-skos/ConceptDefinition";
import {LabelDefinition} from "../../toolkit/definitions";

export default class TaggableDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isInstantiable() {
    return false;
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLInterfaceDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Taggable";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    const hasTaggingLinkDefinition = new LinkDefinition({
      linkName: 'hasTagging',
      symmetricLinkName: "hasEntity",
      rdfObjectProperty: "mnx:hasTagging",
      relatedModelDefinition: TaggingDefinition,
      isPlural: true,
      graphQLPropertyName: "taggings",
      graphQLInputName: "taggingInputs",
      excludeFromIndex: true
    });

    const hasConceptLinkDefinition = new LinkDefinition({
      linkName: 'hasGatheredConcept',
      pathInIndex: "gatheredConcepts",
      relatedModelDefinition: ConceptDefinition,
      isPlural: true,
      linkPath: () => new LinkPath()
        .step({linkDefinition: hasTaggingLinkDefinition})
        .step({linkDefinition: TaggingDefinition.getLink("hasConcept")}),
      graphQLPropertyName: "gatheredConcepts",
      excludeFromInput: true,
      excludeFromIndex: true,
    });

    return [
      ...super.getLinks(),
      hasTaggingLinkDefinition,
      hasConceptLinkDefinition
    ];
  }

  static getLabels() {
    return [
      new LabelDefinition({
        labelName: "gatheredConceptsLabels",
        isPlural: true,
        linkPath: TaggableDefinition
          .getLink("hasGatheredConcept")
          .getLinkPath()
          .clone()
          .property({
            propertyDefinition: new LabelDefinition({
              labelName: 'prefLabel',
              rdfDataProperty: "skos:prefLabel"
            }),
          }),
        isAggregable: true,
      }),
    ]
  }
}
