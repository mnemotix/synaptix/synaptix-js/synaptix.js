/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import chalk from 'chalk';
import env from "env-var";
import {logInfo} from "../logger";
/**
 * @typedef {object} VariableDefinition
 * @property {string} description
 * @property {*} [defaultValue]
 * @property {boolean} [defaultValueInProduction]
 * @property {boolean} [exposeInGraphQL]
 * @property {boolean} [obfuscate]
 */

/**
 * @typedef {Object.<string, VariableDefinition>} EnvironmentDefinition
 */

/**
 * @param {EnvironmentDefinition} environmentDefinition - An object with keys as variable name and values as EnvironmentDefinition
 * @param isProduction
 * @param quiet
 */
export let initEnvironment = (environmentDefinition, isProduction, quiet) => {
  if (typeof isProduction === "undefined") {
    isProduction = env.get("NODE_ENV").asString() === 'production';
  }

  if(typeof quiet === "undefined"){
    quiet = env.get("LOG_LEVEL").asString() === 'quiet';
  }

  if(!quiet){
    logInfo(chalk.underline.bold("Environnement variables:"));
  }

  Object.keys(environmentDefinition).map(environmentVar => {
    let {defaultValue, description, defaultValueInProduction, obfuscate, optional} = environmentDefinition[environmentVar];

    if (!process.env[environmentVar]) {
      if (defaultValue && (defaultValueInProduction || !isProduction || env.get("SIMULATE_PRODUCTION_IN_LOCAL").asBool())) {
        process.env[environmentVar] = typeof defaultValue === "function" ? defaultValue() : defaultValue;
      } else if(!optional){
        throw new Error(`Environment variable ${environmentVar} is not set. ${description}`);
      }
    }

    if (!quiet){
      logInfo(` ${chalk.yellow(environmentVar)}: ${obfuscate ? "****************" : process.env[environmentVar]}`);
    }
  });

  if (!quiet) {
    logInfo("");
  }
};
