/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import LiteralDefinition from "./LiteralDefinition";
import generateId from "nanoid/generate";
import uniqBy from 'lodash/uniqBy';
import upperFirst from 'lodash/upperFirst';
import snakeCase from 'lodash/snakeCase';
import {gatherModelDefinitionHierarchy, GRAPHQL_RDFTYPE_MAPPING} from "./helpers";
import Model from "../models/Model";

export default class ModelDefinitionAbstract {
  /**
   * Is this model definition substituded by another one.
   * Note that this property is automatically mutated by DataModel.
   */
  static isSubstitutedByModelDefinition;

  /**
   * Set false if the related type is not instantiable.
   * @return {boolean}
   */
  static isInstantiable(){
    return true;
  }

  /**
   * Set true if the related type is extensible.
   * If so, a GraphQL interface will automatically be created.
   *
   * @return {boolean}
   */
  static isExtensible(){
    if (this.substituteModelDefinition()){
      return this.substituteModelDefinition().isExtensible();
    }

    return !this.isInstantiable();
  }

  /**
   * Method to simulate multiple inheritance.
   *
   * @return  {typeof ModelDefinitionAbstract[]}  List of parent definitions to inherit from.
   */
  static getParentDefinitions() {
    if (this.substituteModelDefinition()){
      return [...this.substituteModelDefinition().getParentDefinitions(), this.substituteModelDefinition()];
    } else {
      return [];
    }
  }

  /**
   * Get related GraphQLDefinition
   * @return {GraphQLDefinition}
   */
  static getGraphQLDefinition(){}

  /**
   * Method to replace an existing modelDefinition by this one.
   * @return  {typeof ModelDefinitionAbstract}
   */
  static substituteModelDefinition(){}

  /**
   * Get substition model definition
   * @return  {typeof ModelDefinitionAbstract}
   */
  static getSubstitutionModelDefinition(){
    return this.isSubstitutedByModelDefinition;
  }

  /**
   * Is this model definition substituted by another one.
   * @return  {boolean}
   */
  static isSubstituted(){
    return !!this.isSubstitutedByModelDefinition;
  }

  /**
   * Get substit
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   */
  static setSubstitutionModelDefinition(modelDefinition){
    return this.isSubstitutedByModelDefinition = modelDefinition;
  }


  /* istanbul ignore next */

  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    let rdfPrefixesMapping = {
      "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
      "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
    };

    for (let parentDefition of gatherModelDefinitionHierarchy(this)) {
      rdfPrefixesMapping = Object.assign({}, rdfPrefixesMapping, parentDefition.getRdfPrefixesMapping());
    }

    return rdfPrefixesMapping;
  }

  /**
   * Get node type as defined in a triplestore database
   * @returns {string}
   */
  static getRdfType() {
    if (this.substituteModelDefinition()){
      return this.substituteModelDefinition().getRdfType();
    }
  }


  /**
   * Get named graph where related to this type
   * @returns {string}
   */
  static getRdfNamedGraphId() {
  }


  /**
   * Get RDF types that match `owl:sameAs` property.
   * @returns {string[]}
   */
  static getRdfSameAsTypes() {
    return [];
  }

  /**
   * This method returns a function that takes a node type and returns it's URI prefix-styled form.
   *
   * @return {function} - By default `(type) => type.toLowerCase()`
   */
  static getNodeTypeFormatter(){
    /**
     * @param {string} type - node type @see ModelDefinitionAbstract.getNodeType()
     */
    return (type) => type.toLowerCase();
  }

  /**
   * Get node instance base URI.
   *
   * @param {string} nodesNamespaceURI - Namespace of nodes.
   * @param {function} [nodeTypeFormatter] - This function overrides `this::getNodeTypeFormatter()`
   *
   * Examples :
   * - mnx:Person => person
   * - mnx:UserAccount = useraccount
   *
   * @return {string}
   */
  static getRdfInstanceBaseUri({nodesNamespaceURI, nodeTypeFormatter}) {
    let rdfInstanceBaseUri;

    // Sometimes, ModelDefinition have no RDF type.
    if(!this.getRdfType()){
      return "";
    }

    if (!nodeTypeFormatter){
      nodeTypeFormatter = this.getNodeTypeFormatter();
    }

    // Case of absolute RDF type URI.
    // By default we assume to take the URI and just lowercase it.
    //
    // RDF type: `http://ns.mnemotix.com/Person`
    // Instances base URI : `http://ns.mnemotix.com/person`
    //
    // RDF type: `http://ns.mnemotix.com#Person`
    // Instances base URI : `http://ns.mnemotix.com/person`
    // Otherwise, just override this method.
    if (!!this.getRdfType().match(/https?:\/\//)) {
      rdfInstanceBaseUri = this.getRdfType().replace("#", "/");
      let delimiterPos = rdfInstanceBaseUri.lastIndexOf("/");
      rdfInstanceBaseUri = `${rdfInstanceBaseUri.slice(0, delimiterPos + 1)}${nodeTypeFormatter(rdfInstanceBaseUri.slice(delimiterPos + 1))}`;

      // Case of prefixed RDF type URI
      // By default we assume to replace the prefix with `namespace` parameter and lowercase it.
      // RDF type: `mnx:Person`
      // Instances base URI : `${namespace}/person`
      //
      // Otherwise, just override this method.
    } else {
      let delimiterPos = this.getRdfType().indexOf(":");
      rdfInstanceBaseUri = `${nodesNamespaceURI}${nodeTypeFormatter(this.getRdfType().slice(delimiterPos + 1))}`;
    }

    return rdfInstanceBaseUri;
  }

  /**
   * Generate a new id.
   * @param {string} nodesNamespaceURI - Node namespace URI. It can be absolute or prefixed namespace.
   * @param {string} [idPrefix]  - Prefix before random id generation.
   * @param {string} [idValue]   - Set an manual id in place of random id generation.
   * @param {function} [nodeTypeFormatter] - This function overrides `this::getNodeTypeFormatter()`
   * @return {string}
   */
  static generateURI({nodesNamespaceURI, idPrefix, idValue, nodeTypeFormatter}) {
    let baseURI = this.getRdfInstanceBaseUri({nodesNamespaceURI, nodeTypeFormatter});

    return `${baseURI}/${idValue || `${idPrefix || ""}${generateId("0123456789abcdefghijklmnopqrstuvwxy", 14)}`}`
  }

  /**
   * Tests if an URI matches with this model definition
   *
   * @param {string} uri - URI of node (must be absolute).
   * @param {string} nodesNamespaceURI - Namespace of nodes.
   * @param {function} [nodeTypeFormatter] - This function overrides `this::getNodeTypeFormatter()`
the lowercase format.
   * @return {boolean}
   */
  static isMatchingURI({uri, nodesNamespaceURI, nodeTypeFormatter}) {
    let baseUri = this.getRdfInstanceBaseUri({nodesNamespaceURI, nodeTypeFormatter});
    return uri.includes(`${baseUri}/`);
  }

  /**
   * Get node type as defined in a property graph database
   * @returns {string}
   */
  static getNodeType() {
    let rdfType = this.getRdfType();

    // If there is a RDF type, default node type is the RDF class without the prefix.
    if (rdfType) {
      if (rdfType.match(/https?:\/\//)) {
        return rdfType.substring(rdfType.replace("#", "/").lastIndexOf("/") + 1)
      } else {
        return rdfType.substring(rdfType.indexOf(':') + 1)
      }
    }

    throw new Error(`You must implement ${this.name}::static getNodeType()`);
  }

  /**
   * Get index type as defined in index database
   * @returns {string}
   */
  static getIndexType() {
    if (this.substituteModelDefinition()){
      return this.substituteModelDefinition().getIndexType();
    } else {
      for (let parentDefition of gatherModelDefinitionHierarchy(this)) {
        if(parentDefition.getIndexType()) {
          return parentDefition.getIndexType();
        }
      }
    }
  }

  /**
   * Get index filters to add a each index requests.
   * This is usefull when using shared index between siblings ModelDefinitions
   * @returns {array}
   */
  static getIndexFilters() {
    if (this.substituteModelDefinition()){
      return this.substituteModelDefinition().getIndexFilters();
    } else {
      for (let parentDefition of gatherModelDefinitionHierarchy(this)) {
        if(parentDefition.getIndexFilters()) {
          return parentDefition.getIndexFilters();
        }
      }
    }
  }

  /**
   * Returns true if related index is defined
   */
  static isIndexed() {
    return !!this.getIndexType();
  }

  /**
   * Get model class.
   * @return {Model}
   */
  static getModelClass() {
    return Model;
  }

  /**
   * Get GraphQL type.
   * By default, this is the node type.
   *
   * @returns {string}
   */
  static getGraphQLType() {
    return this.getNodeType();
  }

  /**
   * Get GraphQL input type.
   * By default, this is the graphQL type followed by "Input".
   *
   * @returns {string}
   */
  static getGraphQLInputType() {
    return `${this.getGraphQLType()}Input`;
  }

  /**
   * Get links definitions
   * @param {boolean} [skipHeritedLinks=false]
   * @returns {LinkDefinition[]}
   */
  static getLinks(skipHeritedLinks = false) {
    let links = [];

    if (!skipHeritedLinks) {
      for (let parentDefition of gatherModelDefinitionHierarchy(this)) {
        links = [].concat(links, parentDefition.getLinks());
      }

      links.map(link => link.setIsHerited());
      links = uniqBy(links, (link) => link.getLinkName());
    }

    return links;
  }

  /**
   * Get link definition
   * @param {string} linkName
   * @param {boolean} [skipHeritedLinks=false]
   * @return {LinkDefinition}
   */
  static getLink(linkName, skipHeritedLinks = false) {
    let link = this.getLinks(skipHeritedLinks).find((linkDefinition) => linkName === linkDefinition.getLinkName());

    if (!link) {
      let name = this.toString().split('(' || /s+/)[0].split(' ' || /s+/)[1];
    }

    return link;
  }

  /**
   * Get link definition
   * @returns {typeof ModelDefinitionAbstract} targetModelDefinition
   */
  static getFirstLinkForTargetModelDefinition(targetModelDefinition) {
    let link = this.getLinks().find((linkDefinition) => targetModelDefinition === linkDefinition.getRelatedModelDefinition());

    if (!link) {
      let name = this.toString().split('(' || /s+/)[0].split(' ' || /s+/)[1];
      throw new Error(`Link definition ${name} ~> ModelDefinition[${targetModelDefinition.name}] is not defined.`);
    }

    return link;
  }

  /**
   * Get labels definitions
   * @returns {LabelDefinition[]}
   */
  static getLabels() {
    let labels = [];

    for (let parentDefition of gatherModelDefinitionHierarchy(this)) {
      labels = [].concat(labels, parentDefition.getLabels());
    }

    labels.map(label => label.setIsHerited());

    return uniqBy(labels, (label) => label.getLabelName());
  }

  /**
   * Get labels flagged with `isSearchable=true`
   */
  static getSearchableLabels() {
    return this.getLabels().filter(label => label.isSearchable())
  }

  /**
   * Get label definition
   * @returns {LabelDefinition}
   */
  static getLabel(labelName) {
    return this.getLabels().find((labelDefinition) => labelName === labelDefinition.getLabelName());
  }

  /**
   * Get literals definitions
   * @returns {LiteralDefinition[]}
   */
  static getLiterals() {
    let literals = [];

    for (let parentDefition of gatherModelDefinitionHierarchy(this)) {
      literals = [].concat(literals, parentDefition.getLiterals());
    }

    literals.map(literal => literal.setIsHerited());

    return uniqBy(literals, (literal) => literal.getLiteralName());
  }

  /**
   * Get literals flagged with `isSearchable=true`
   */
  static getSearchableLiterals() {
    return this.getLiterals().filter(literal => literal.isSearchable());
  }

  /**
   * Get label definition
   * @returns {LiteralDefinition}
   */
  static getLiteral(literalName) {
    return this.getLiterals().find((literalDefinition) => literalName === literalDefinition.getLiteralName());
  }

  /**
   * This gathers this.getLiterals() and this.getLabels()
   * @return {PropertyDefinitionAbstract[]}
   */
  static getProperties(){
    return [].concat(this.getLiterals(), this.getLabels());
  }

  /**
   * Get property definition
   * @returns {PropertyDefinitionAbstract}
   */
  static getProperty(propertyName) {
    return this.getProperties().find((propertyDefinition) => propertyName === propertyDefinition.getPropertyName());
  }

  /**
   * Returns a literal given an Rdf data property
   * @param dataProperty
   * @return {PropertyDefinitionAbstract}
   */
  static getPropertyFromRdfDataProperty(dataProperty) {
    return this.getProperties().find((propertyDefinition) => {
      return dataProperty === propertyDefinition.getRdfDataProperty() || dataProperty === propertyDefinition.getRdfAliasDataProperty()
    });
  }

  /**
   * Returns a literal given an indexed field
   * @param  {string} field
   * @return {PropertyDefinitionAbstract}
   */
  static getPropertyFromIndexedField(field) {
    return this.getProperties().find((propertyDefinition) => {
      return propertyDefinition.getPathInIndex() === field;
    });
  }

  /**
   * Returns a link given an indexed field
   * @param  {string} field
   * @return {LinkDefinition}
   */
  static getLinkFromIndexedField(field) {
    return this.getLinks().find((linkDefinition) => {
      return linkDefinition.getPathInIndex() === field;
    });
  }

  /**
   * Returns a property given an indexed field
   * @param  {string} field
   * @return {PropertyDefinitionAbstract}
   */
  static getPropertyFromGraphqlPropertyName(field) {
    return this.getProperties().find((propertyDefinition) => {
      return propertyDefinition.getPropertyName() === field.replace("Translations", "");
    });
  }

  /**
   * Returns a link given an indexed field
   * @param  {string} field
   * @return {LinkDefinition}
   */
  static getLinkFromGraphqlPropertyName(field) {
    return this.getLinks().find((linkDefinition) => {
      // GraphQL fields ending with "Count" are automatically generated for plural links.
      return linkDefinition.getGraphQLPropertyName() === field.replace(/Count$/, "");
    });
  }

  /**
   * Returns a link definition given an Rdf object property
   * @param {string} objectProperty
   * @return {LinkDefinition}
   */
  static getLinkFromRdfObjectProperty(objectProperty) {
    return this.getLinks().find((linkDefinition) => {
      return objectProperty === linkDefinition.getRdfObjectProperty() || objectProperty === linkDefinition.getRdfReversedObjectProperty()
    });
  }

  /**
   * Get properties flagged with `isSearchable=true`
   * @return {PropertyDefinitionAbstract[]}
   */
  static getSearchableProperties() {
    return this.getProperties().filter(property => property.isSearchable())
  }


  /**
   * Convert model definition to Sparql variable.
   *
   * @return {string}
   */
  static toSparqlVariable() {
    let typeWithoutPrefix;

    if (!!this.getRdfType().match(/https?:\/\//)) {
      typeWithoutPrefix = this.getRdfType().slice(this.getRdfType().replace("#", "/").lastIndexOf("/") + 1);
    } else {
      typeWithoutPrefix = this.getRdfType().slice(this.getRdfType().indexOf(':') + 1);
    }

    return `?${typeWithoutPrefix.toLowerCase()}`
  }

  /**
   * Returns the list of parents
   */
  static gatherModelDefinitionHierarchy(){
    let hierarchyDefinitions = [];

    for (let parentDefition of this.getParentDefinitions()) {
      hierarchyDefinitions = [].concat(hierarchyDefinitions, [parentDefition], parentDefition.gatherModelDefinitionHierarchy())
    }

    return hierarchyDefinitions;
  }

  /**
   * Test is this model definition is
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   */
  static isEqualOrDescendantOf(modelDefinition){
    return this === modelDefinition || this.gatherModelDefinitionHierarchy().includes(modelDefinition)
  }

  /**
   * Generate GraphQL input properties
   *
   * @param {boolean} excludeInheritedInputProperties - Generate only specific properties.
   * @param {boolean} excludeNestedInputProperties - Exclude links inputs
   * @return {string}
   */
  static generateGraphQLInputProperties({
    excludeInheritedInputProperties,
    excludeNestedInputProperties
  } = {}){
    let properties = "";

    if (!excludeInheritedInputProperties){
      if (!this.isInstantiable()){
        properties += `
  """ 
  Inherited instantiable GraphQL Typename. This is compulsory for input that refers to interfaces.
  @see [Deep object input parsing documentation](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#deep-object-input-parsing)
  """
  inheritedTypename: String
`;
      }

      properties += `
  """ 
  The target node id. This is usefull to create link with exsting objects during creation/update mutations.
  @see [Deep object input parsing documentation](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#deep-object-input-parsing)
  """
  id: ID
`;
    }

    for(let literal of this.getLiterals()){
      if(!literal.isExcludedFromInput() && !literal.getLinkPath() && GRAPHQL_RDFTYPE_MAPPING[literal.getRdfDataType()]){
        properties += `
  """ ${upperFirst(snakeCase(literal.getLiteralName()).replace(/_/g, ' '))} """
  ${literal.getLiteralName()}: ${GRAPHQL_RDFTYPE_MAPPING[literal.getRdfDataType()]}
`
      }
    }

    for(let label of this.getLabels()){
      if(!label.getLinkPath() ){
        properties += `
  """ ${upperFirst(snakeCase(label.getLabelName()).replace(/_/g, ' '))} """
  ${label.getLabelName()}: String
`
      }
    }

    if(!excludeNestedInputProperties){
      for(let link of this.getLinks()){
        if (!link.isExcludedFromInput() && !!link.getGraphQLInputName()) {
          if (!(excludeInheritedInputProperties && link.isHerited())) {
            let relatedModelDefinition = link.getRelatedModelDefinition();
            let comment = `Related ${snakeCase(relatedModelDefinition.getGraphQLType()).replace(/_/g, ' ')}${link.isPlural() ? 's' : ''}`;

            let inputName = link.getGraphQLInputName();
            let inputType = link.isPlural() ? `[${relatedModelDefinition.getGraphQLInputType()}]` : relatedModelDefinition.getGraphQLInputType();
            properties += `
  """ ${comment} """
  ${inputName}: ${inputType}
`;

            if (link.isPlural()) {
              properties += `
  """ Related ${snakeCase(relatedModelDefinition.getGraphQLType()).replace(/_/g, ' ')}${link.isPlural() ? 's' : ''} to delete """
  ${inputName}ToDelete: [ID!]
`
            }
          }
        }
      }
    }

    return properties;
  }
  /**
   * @return {FilterDefinition[]}
   */
  static getFilters(){
    return [];
  }

  /**
   * @param filterName
   * @return {FilterDefinition}
   */
  static getFilter(filterName){
    return this.getFilters().find(filter => filterName === filter.getFilterName())
  }

  /**
   * @return {SortingDefinition[]}
   */
  static getSortings(){
    return [];
  }

  /**
   * @param sortingName
   * @return {SortingDefinition}
   */
  static getSorting(sortingName){
    return this.getSortings().find(sorting => sortingName === sorting.getSortingName())
  }
}
