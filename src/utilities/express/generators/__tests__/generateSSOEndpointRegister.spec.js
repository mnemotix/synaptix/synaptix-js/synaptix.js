import request from 'supertest';
import bodyParser from 'body-parser';
import {generateSSOEndpoint} from '../generateSSOEndpoint';
import SSOApiClient from '../../../../datamodules/drivers/sso/SSOApiClient';
import {SSOEndpointMessageExamples} from '../../../../testing';
import {SSOEndpointRegisterJSONSchemas} from '../__jsonSchemas__/SSOEndpointRegisterJSONSchemas';
import ExpressApp from "../../ExpressApp";
import SSOUser from "../../../../datamodules/drivers/sso/models/SSOUser";
import SSOControllerService from "../../../../datamodules/services/sso/SSOControllerService";

jest.mock('../../../../datamodules/drivers/sso/SSOApiClient');
jest.mock('../../middlewares/attachDatastoreSession');
jest.mock('../../../../datamodules/services/sso/SSOControllerService');

let app;


beforeEach(() => {
  app = new ExpressApp();
  app.use(bodyParser.json());

  generateSSOEndpoint(app, {
    authorizationURL: '/auth',
    tokenURL: '/token',
    logoutURL: '/logout',
    clientID: '123',
    clientSecret: '123',
    baseURL: '/',
    adminUsername: '123',
    adminPassword: '123',
    adminTokenUrl: '/admin/token',
    adminApiUrl: '/api',
    registrationEnabled: true,
    ssoApiClient: new SSOApiClient({
      tokenEndpointUrl: '/api/token',
      apiEndpointUrl: '/api',
      login: "123",
      password: "123"
    })
  });
  SSOApiClient.mockClear();
});

describe('/register endpoint', () => {
  it('works', async () => {
    SSOApiClient.prototype.getUserByUsername.mockImplementation(() => {
      throw new Error('no user');
    });

    let createdUser = new SSOUser({
      user: {
        id: "new-id",
        username: 'test@domain.com',
        email: 'test@domain.com',
        firstName: 'John',
        lastName: 'Doe',
      }
    });

    SSOControllerService.prototype.registerUserAccount.mockImplementation(() => {
      return createdUser;
    });
    let requestJSON = JSON.parse(SSOEndpointMessageExamples['POST /auth/register'].request);

    let response = await request(app.getOriginalApp())
      .post('/auth/register')
      .send(requestJSON);
    expect(response.status).toEqual(200);
    expect(response.body).toEqual(createdUser.toJSON());
    expect(response.body).toMatchSchema(SSOEndpointRegisterJSONSchemas.successResponse);
  });

  describe('input validation', () => {
    it('checks username is not empty', async () => {
      SSOApiClient.prototype.isUsernameExists.mockImplementation(() => false);

      let response = await request(app.getOriginalApp())
        .post('/auth/register')
        .send({
          firstName: 'John',
          lastName: 'Doe',
          password: 'pass'
        });
      expect(response.status).toEqual(400);
      expect(response.body).toEqual({
        errors: {
          username: 'Username is empty'
        }
      });
      expect(response.body).toMatchSchema(SSOEndpointRegisterJSONSchemas.errorResponse);
    });

    it('checks password is not empty', async () => {
      SSOApiClient.prototype.isUsernameExists.mockImplementation(() => false);

      let response = await request(app.getOriginalApp())
        .post('/auth/register')
        .send({
          username: 'test@domain.com',
          firstName: 'John',
          lastName: 'Doe'
        });
      expect(response.status).toEqual(400);
      expect(response.body).toEqual({
        errors: {
          password: 'Password is empty'
        }
      });
      expect(response.body).toMatchSchema(SSOEndpointRegisterJSONSchemas.errorResponse);
    });

    it("checks user doesn't exist already", async () => {
      SSOApiClient.prototype.isUsernameExists.mockImplementation((username) => {
        return username === 'test@domain.com';
      });

      let response = await request(app.getOriginalApp())
        .post('/auth/register')
        .send({
          username: 'test@domain.com',
          firstName: 'John',
          lastName: 'Doe',
          password: 'pass'
        });
      expect(response.status).toEqual(400);
      expect(response.body).toEqual({
        errors: {
          username: 'An account already exists with this email'
        }
      });
      expect(response.body).toMatchSchema(SSOEndpointRegisterJSONSchemas.errorResponse);
    });
  });

  it("follows an unexpected backend error", async () => {
    SSOApiClient.prototype.isUsernameExists.mockImplementation(() => false);
    SSOControllerService.prototype.registerUserAccount.mockImplementation(() => {
      throw new Error('We got an unexpected issue');
    });

    let response = await request(app.getOriginalApp())
      .post('/auth/register')
      .send({
        username: 'test@domain.com',
        firstName: 'John',
        lastName: 'Doe',
        password: 'pass'
      });
    expect(response.status).toEqual(500);
    expect(response.body).toEqual({
      error: 'We got an unexpected issue'
    });
    expect(response.body).toMatchSchema(SSOEndpointRegisterJSONSchemas.errorResponse);
  });
});
