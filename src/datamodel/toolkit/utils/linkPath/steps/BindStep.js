/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Step} from "./Step";

export class BindStep extends Step{
  /**
   * @param {LinkPath} linkPath
   * @param {string} bindAs
   * @param {string} rdfDataPropertyAlias
   */
  constructor({linkPath, bindAs, rdfDataPropertyAlias}){
    super();
    this._linkPath = linkPath || throw new TypeError("linkPath must be provided");
    this._bindAs = bindAs || throw new TypeError("bindAs must be provided");
    this._rdfDataPropertyAlias = rdfDataPropertyAlias || throw new TypeError("rdfDataPropertyAlias must be provided");
  }

  /**
   * @return {LinkPath}
   */
  getLinkPath() {
    return this._linkPath;
  }

  /**
   * @return {string}
   */
  getBindAs() {
    return this._bindAs;
  }

  /**
   * @return {string}
   */
  getRdfDataPropertyAlias() {
    return this._rdfDataPropertyAlias;
  }
}
