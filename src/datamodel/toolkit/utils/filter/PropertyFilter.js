/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


export class PropertyFilter {
  /**
   * @param {*} value
   * @param {PropertyDefinitionAbstract} propertyDefinition
   * @param {string} [lang]
   * @param {boolean} [any=false]  This is the "[field]:*" KQL case.
   * @param {boolean} [isNeq]  This is the "not" KQL case.
   * @param {string} [isLte] This is the "<=" KQL case
   * @param {string} [isLt] This is the "<" KQL case
   * @param {string} [isGte] This is the ">=" KQL case
   * @param {string} [isGt] This is the ">" KQL case
   * @param {boolean} [isStrict] Is the filter strict. That means for strings to comply case sensitivity, diacritics...
   * @see https://www.elastic.co/guide/en/kibana/7.6/kuery-query.html
   */
  constructor({value, propertyDefinition, lang, any, isNeq, isLte, isLt, isGte, isGt, isStrict}) {
    this._value = value;
    this._propertyDefinition = propertyDefinition;
    this._lang = lang;
    this._any = any;
    this._isNeq = isNeq;
    this._isLte = isLte;
    this._isLt = isLt;
    this._isGte = isGte;
    this._isGt = isGt;
    this._isStrict = isStrict;
  }

  /**
   * @return {*}
   */
  get value() {
    return this._value;
  }

  /**
   * @return {PropertyDefinitionAbstract}
   */
  get propertyDefinition() {
    return this._propertyDefinition;
  }

  /**
   * @return {string}
   */
  get lang() {
    return this._lang;
  }

  /**
   * @return {boolean}
   */
  get any(){
    return this._any;
  }

  /**
   * @return {boolean}
   */
  get isNeq() {
    return this._isNeq;
  }

  get isLte() {
    return this._isLte;
  }

  get isLt() {
    return this._isLt;
  }

  get isGte() {
    return this._isGte;
  }

  get isGt() {
    return this._isGt;
  }

  get isStrict() {
    return this._isStrict;
  }

}