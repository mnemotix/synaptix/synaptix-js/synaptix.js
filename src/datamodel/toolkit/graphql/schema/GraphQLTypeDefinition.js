/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import { GraphQLCreateMutation, GraphQLUpdateMutation } from './GraphQLMutation';
import { GraphQLTypeQuery, GraphQLTypeConnectionQuery } from './GraphQLQuery';
import GraphQLDefinition from './GraphQLDefinition';


/**
 * This module defines a base specification for a GraphQL type schema, which can be overriden
 * 
 * It sets some graphQL fields for a type schema :
 * - a type query field
 * - a connection query field
 * - a create mutation field
 * - an update mutation field
 * Each field has its name generated at runtime, and based on the model definition using this graphQL definition
 * By default, for removal mutation, the removeObject field can be used
 * 
 */
export default class GraphQLTypeDefinition extends GraphQLDefinition {

    /**
     * @inheritdoc
     */
    static isTypeGenerated() {
        return true;
    }

    /**
     * @inheritdoc
     */
    static getTypeQuery() {
        return new GraphQLTypeQuery();
    }

    /**
     * @inheritdoc
     */
    static getTypeConnectionQuery() {
        return new GraphQLTypeConnectionQuery();
    }

    /**
     * @inheritdoc
     */
    static getCreateMutation() {
        return new GraphQLCreateMutation();
    }

    /**
     * @inheritdoc
     */
    static getUpdateMutation() {
        return new GraphQLUpdateMutation();
    }

}