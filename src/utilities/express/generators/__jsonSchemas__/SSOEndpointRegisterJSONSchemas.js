/**
 * JSON Schemas (spec http://json-schema.org/) describing the expected JSON format for messages used
 * by the SSO endpoint "register" (request, success and error responses)
 */


const requestJSONSchema = {
  "$id": "http://mnemotix.com/json-schemas/synaptix.js/SSOEndpoint/register/request",
  "type": "object",
  "properties": {
    "username": {"type": "string"},
    "password": {"type": "string"},
    "firstName": {"type": "string"},
    "lastName": {"type": "string"}
  },
  "additionalProperties": false,
  "required": ["username", "password"]
};

const successResponseJSONSchema = {
  "$id": "http://mnemotix.com/json-schemas/synaptix.js/SSOEndpoint/register/successResponse",
  "type": "object",
  "properties": {
    "id": {"type": "string"},
    "username": {"type": "string"},
    "email": {"type": "string"},
    "firstName": {"type": "string"},
    "lastName": {"type": "string"},
    "attributes": {"type": "object"},
  },
  "additionalProperties": false,
  "required": ["id", "email", "firstName", "lastName"]
};

const errorResponseJSONSchema = {
  "$id": "http://mnemotix.com/json-schemas/synaptix.js/SSOEndpoint/register/errorResponse",
  "oneOf": [{
    /* Bad request error */
    "type": "object",
    "properties": {
      "errors": {
        "type": "object",
        "properties": {
          "username": {"type": "string"},
          "password": {"type": "string"},
          "additionalProperties": false
        }
      },
    },
    "additionalProperties": false,
    "required": ["errors"]
  }, {
    /* Unexpected error */
    "type": "object",
    "properties": {
      "error": {"type": "string"}
    },
    "additionalProperties": false,
    "required": ["error"]
  }]
};

export const SSOEndpointRegisterJSONSchemas = {
  request: requestJSONSchema,
  successResponse: successResponseJSONSchema,
  errorResponse: errorResponseJSONSchema
};
