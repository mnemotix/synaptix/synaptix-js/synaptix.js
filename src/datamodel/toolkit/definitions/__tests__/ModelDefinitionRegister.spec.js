/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import ModelDefinitionsRegister from '../ModelDefinitionsRegister';
import {
  BarDefinitionMock,
  NotInstantiableDefinitionMock,
  FooDefinitionMock,
  BazDefinitionMock
} from "../../../__tests__/mocks/definitions";
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";

/** @type {ModelDefinitionsRegister} */
let modelDefinitionsRegister;

describe('ModelDefinitionsRegister', () => {
  beforeEach(() => {
    modelDefinitionsRegister = new ModelDefinitionsRegister([
      NotInstantiableDefinitionMock, BarDefinitionMock, FooDefinitionMock, BazDefinitionMock
    ]);
  });

  it('should return list of model definitions', () => {
    expect(modelDefinitionsRegister.getModelDefinitions()).toEqual([
      NotInstantiableDefinitionMock, BarDefinitionMock, FooDefinitionMock, BazDefinitionMock
    ])
  });

  it('should return list prefixes', () => {
    expect(modelDefinitionsRegister.getRdfPrefixesMapping()).toEqual({
      rdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
      rdfs: 'http://www.w3.org/2000/01/rdf-schema#'
    })
  });

  it('should substitute a model definition', () => {
    class NewBarDefinitionMock extends ModelDefinitionAbstract{}

    modelDefinitionsRegister.substituteModelDefinitions(BarDefinitionMock, NewBarDefinitionMock);
    expect(modelDefinitionsRegister.getModelDefinitions()).toEqual([
      NotInstantiableDefinitionMock, NewBarDefinitionMock, FooDefinitionMock, BazDefinitionMock
    ])
  });

  it('should add a models definition and substitute ones', () => {
    class NewBarDefinitionMock extends ModelDefinitionAbstract{
      static substituteModelDefinition(){
        return BarDefinitionMock;
      }
    }

    class FixDefinitionMock extends ModelDefinitionAbstract{}

    modelDefinitionsRegister.addModelDefinitions([NewBarDefinitionMock, FixDefinitionMock]);
    expect(modelDefinitionsRegister.getModelDefinitions()).toEqual([
      NotInstantiableDefinitionMock, NewBarDefinitionMock, FooDefinitionMock, BazDefinitionMock, FixDefinitionMock
    ])
  });
});




