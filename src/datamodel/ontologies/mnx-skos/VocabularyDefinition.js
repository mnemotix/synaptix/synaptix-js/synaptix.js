/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import ConceptDefinition from "./ConceptDefinition";
import SchemeDefinition from "./SchemeDefinition";
import CollectionDefinition from "./CollectionDefinition";
import {GraphQLTypeDefinition} from "../../toolkit/graphql/schema";
import SkosElementDefinition from "./SkosElementDefinition";
import VocabularyGraphQLDefinition from "./graphql/VocabularyGraphQLDefinition";

export default class VocabularyDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [SkosElementDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return VocabularyGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Vocabulary";
  }


  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Vocabulary';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'vocabulary';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasConcept',
        symmetricLinkName: "hasVocabulary",
        rdfReversedObjectProperty: "mnx:conceptOf",
        relatedModelDefinition: ConceptDefinition,
        rdfObjectProperty: "skos:concept",
        isPlural: true,
        graphQLPropertyName: "concepts",
        graphQLInputName: "conceptInputs"
      }),
      new LinkDefinition({
        linkName: 'hasScheme',
        symmetricLinkName: "hasVocabulary",
        rdfObjectProperty: "mnx:schemeOf",
        relatedModelDefinition: SchemeDefinition,
        isPlural: true,
        graphQLPropertyName: "schemes",
        graphQLInputName: "schemeInputs"
      }),
      new LinkDefinition({
        linkName: 'hasDraftScheme',
        rdfReversedObjectProperty: "mnx:draftSchemeOf",
        relatedModelDefinition: SchemeDefinition,
        graphQLPropertyName: "draftSchemes",
        graphQLInputName: "draftSchemeInputs"
      }),
      new LinkDefinition({
        linkName: 'hasCollection',
        symmetricLinkName: "hasVocabulary",
        rdfReversedObjectProperty: "mnx:collectionOf",
        relatedModelDefinition: CollectionDefinition,
        isPlural: true,
        graphQLPropertyName: "collections",
        graphQLInputName: "collectionInputs"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'prefLabel',
        rdfDataProperty: "skos:prefLabel"
      }),
      new LabelDefinition({
        labelName: 'altLabel',
        rdfDataProperty: "skos:altLabel"
      }),
      new LabelDefinition({
        labelName: 'definition',
        rdfDataProperty: "skos:definition"
      }),
      // Deprecated
      new LabelDefinition({
        labelName: 'title',
        rdfDataProperty: "mnx:title"
      }),
      new LabelDefinition({
        labelName: 'description',
        rdfDataProperty: "mnx:description"
      })
    ];
  }
};