// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // Indicates whether the coverage information should be collected while executing the test
  collectCoverage: true,

  // An array of glob patterns indicating a set of files for which coverage information should be collected
  collectCoverageFrom: [
    'src/**/**/*.js'
  ],

  // The directory where Jest should output its coverage files
  coverageDirectory: "coverage",

  //An array of regexp pattern strings used to skip coverage collection
  coveragePathIgnorePatterns: [
    "/node_modules/",
    "/__generated__/",
    "/__integrations__/",
    "/__tests__/",
    "index.js",
    "/ontologies/",
    "/ontologies-legacy/",
    "/utilities/graphql2json/",
    ".*\.graphql\.js"
  ],

  // A list of reporter names that Jest uses when writing coverage reports
  "coverageReporters": [
    "json-summary",
    "text",
    "lcov"
  ],

  // A list of paths to modules that run some code to configure or set up the testing framework before each test
  setupFilesAfterEnv: [
    "./jest/setup/env.js",
    "./jest/setup/jsonSchemaMatchers.js"
  ],

  // The test environment that will be used for testing
  testEnvironment: "node",

  // The glob patterns Jest uses to detect test files
  testMatch: [
    "**/src/**/__tests__/*.(spec|test).js",
  ],
};
