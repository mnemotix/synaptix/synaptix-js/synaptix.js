/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import {isReadOnlyModeDisabledRule} from "../../middlewares/aclValidation/rules";

export let UnregisterUserAccountMutation = `
"""Unregister account mutation payload"""
type UnregisterUserAccountPayload {
  success: Boolean
}

"""Unregister account mutation input"""
input UnregisterUserAccountInput {
  userId: String
  
  """ GDPR right to erasure option if true. """
  permanent: Boolean
}

extend type Mutation{
  """ 
  This mutation can be used in different ways :
  
    - If userId parameter is passed, a logged and authorized user can remove this one.
    - If userId is not passed, the logged user remove it's account.
    
    - To follow the GDPR "right to erasure" set the "permanent" parameter to true
  """
  unregisterUserAccount(input: UnregisterUserAccountInput!): UnregisterUserAccountPayload
}
`;

export let UnregisterUserAccountMutationResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {boolean} userParams
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    unregisterUserAccount: {
      resolve: async (_, {input: {userId, permanent}}, synaptixSession) => {
        await synaptixSession.getSSOControllerService().unregisterUserAccount({userId, permanent});
        return {
          success: true
        };
      }
    },
  },
};

export let UnregisterUserAccountMutationShieldRules = {
  Mutation: {
    unregisterUserAccount: isReadOnlyModeDisabledRule()
  }
};