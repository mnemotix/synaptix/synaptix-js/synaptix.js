/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import UserAccountDefinition from "../mnx-agent/UserAccountDefinition";
import EntityDefinition from "../mnx-common/EntityDefinition";
import ActionGraphQLDefinition from "./graphql/ActionGraphQLDefinition";
import PersonDefinition from "../mnx-agent/PersonDefinition";
import { LinkPath } from "../../toolkit/utils/linkPath";

export default class ActionDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isInstantiable() {
    return false;
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return ActionGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      prov: "http://www.w3.org/ns/prov#"
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Action";
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["prov:Activity"];
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "action";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    const userAccountLinkDefinition = new LinkDefinition({
      linkName: "hasUserAccount",
      rdfObjectProperty: "prov:wasAssociatedWith",
      relatedModelDefinition: UserAccountDefinition,
      graphQLPropertyName: "userAccount",
      graphQLInputName: "userAccountInput"
    });

    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: "hasEntity",
        rdfObjectProperty: "prov:generated",
        relatedModelDefinition: EntityDefinition,
        graphQLPropertyName: "entities",
        isPlural: true
      }),
      userAccountLinkDefinition,
      new LinkDefinition({
        linkName: "hasPerson",
        relatedModelDefinition: PersonDefinition,
        linkPath: () =>
          new LinkPath()
            .step({
              linkDefinition: userAccountLinkDefinition
            })
            .step({
              linkDefinition: UserAccountDefinition.getLink("hasPerson")
            }),
        graphQLPropertyName: "person"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "startedAtTime",
        rdfDataProperty: "prov:startedAtTime",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
      }),
      new LiteralDefinition({
        literalName: "endedAtTime",
        rdfDataProperty: "prov:endedAtTime",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
      }),
      new LiteralDefinition({
        literalName: "snapshot",
        rdfDataProperty: "prov:value",
        description: "A subgraph snapshot of the action"
      })
    ];
  }
}
