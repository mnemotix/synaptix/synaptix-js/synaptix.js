/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {SynaptixErrorResponse} from "../utilities/express/generators/__jsonGraphQLErrorResponses__/SynaptixErrorResponse";
import {UnexpectedServerErrorResponse} from "../utilities/express/generators/__jsonGraphQLErrorResponses__/UnexpectedServerErrorResponse";
import {UnauthorizedErrorResponse} from "../utilities/express/generators/__jsonGraphQLErrorResponses__/UnauthorizedErrorResponse";
import {FormValidationErrorResponse} from "../utilities/express/generators/__jsonGraphQLErrorResponses__/FormValidationErrorResponse";
import {MalformedRequestResponse} from "../utilities/express/generators/__jsonGraphQLErrorResponses__/MalformedRequestResponse";


export const GraphQLErrorResponses = {
  SynaptixErrorResponse,
  UnauthorizedErrorResponse,
  UnexpectedServerErrorResponse,
  FormValidationErrorResponse,
  MalformedRequestResponse
};