/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import SSOUser from "../../../datamodules/drivers/sso/models/SSOUser";
import env from "env-var";

export default class GraphQLContext {
  /**
   * @param {SSOUser} [user]
   * @param {bool} [anonymous]
   * @param {string} lang
   * @param {object} extra
   */
  constructor({user, anonymous, lang, ...extra}) {
    if (!anonymous && (!user || !(user instanceof SSOUser))) {
      throw new Error("Parameter `user` must be defined in GraphQLContext and be instance of SSOUser");
    }

    this._anonymous = anonymous || false;
    this._user = user;
    this._lang = lang || env.get("DEFAULT_LOCALE").default("fr").asString();
    this._extra = extra;
  }

  /**
   * @return {SSOUser}
   */
  isAnonymous() {
    return this._anonymous;
  }

  /**
   * @return {SSOUser}
   */
  getUser() {
    return this._user;
  }

  /**
   * @return {string}
   */
  getLang() {
    return this._lang;
  }

  /**
   * @return {object}
   */
  getExtra() {
    return this._extra;
  }
}
