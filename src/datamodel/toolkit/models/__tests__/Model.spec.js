/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Model from "../Model";

describe("Model", () => {
  it("Create a model in a legacy way", () => {
    let model = new Model("test:foo/1234", "test:foo/1234", {fooLit: "aer"}, "Foo");

    expect(model.id).toEqual("test:foo/1234");
    expect(model.uri).toEqual("test:foo/1234");
    expect(model.fooLit).toEqual("aer");
    expect(model.type).toEqual("Foo");
  });

  it("Create an extending model with inferred type", () => {
    class Foo extends Model{}

    let model = new Foo("test:foo/1234", "test:foo/1234", {fooLit: "aer"});

    expect(model.id).toEqual("test:foo/1234");
    expect(model.uri).toEqual("test:foo/1234");
    expect(model.fooLit).toEqual("aer");
    expect(model.type).toEqual("Foo");
  });

  it("Create a model with object params", () => {
    let model = new Model({id:"test:foo/1234", uri:"test:foo/1234", props: {fooLit: "aer"}, type: "Foo"});

    expect(model.id).toEqual("test:foo/1234");
    expect(model.uri).toEqual("test:foo/1234");
    expect(model.fooLit).toEqual("aer");
    expect(model.type).toEqual("Foo");
  });
});