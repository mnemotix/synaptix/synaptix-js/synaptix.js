/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {and, or} from "graphql-shield";
import {GraphQLMutation} from "../../../toolkit/graphql/schema";
import {isAdminRule} from "../../mnx-agent/middlewares/aclValidation/rules";
import {isMutationEnabledRule} from "../../../toolkit/graphql/schema/middlewares/aclValidation/rules";
import {isCreatorRule} from "../../mnx-contribution/middlewares/aclValidation/rules/isCreatorRule";

export class RemoveEntityGraphQLMutation extends GraphQLMutation{
  /**
   * @inheritdoc
   */
  generateFieldName() {
    return `removeEntity`;
  }

  /**
   * @inheritdoc
   */
  generateType(modelDefinition) {
    return `
"""Remove entity mutation payload"""
type RemoveEntityPayload {
  deletedId: ID
}

"""Remove object mutation input"""
input RemoveEntityInput {
  objectId: ID!
  permanent: Boolean
}

${this._wrapQueryType(`
  """
    This an alias of removeObject mutation. It is just a matter on name homogenization.
  """
  ${this.generateFieldName()}(input: RemoveEntityInput!): RemoveEntityPayload
`)}   
`;
  }

  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      /**
       * @param {object} _
       * @param {object} args
       * @param {SynaptixDatastoreSession} synaptixSession
       */
      [this.generateFieldName()]: async (_, args, synaptixSession) => {
        const {input: {objectId, permanent}} = args;

        let {id, type} = synaptixSession.parseGlobalId(objectId);

        if(!type){
          type = await synaptixSession.getGraphQLTypeForId(id);
        }

        await synaptixSession.removeObjects({
          modelDefinition: synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(type),
          objectIds: [id],
          args,
          permanentRemoval: permanent
        });

        return {deletedId: objectId};
      }
    })
  }

  getShieldRule(){
    return and(isMutationEnabledRule(), or(isAdminRule(), isCreatorRule()))
  }
}