/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import NetworkLayerAMQP from '../NetworkLayerAMQP';
import amqplib from 'amqplib-mocks';

jest.mock("nanoid/generate", () => {
  let uriCounter = 0;

  return jest.fn().mockImplementation(() => {
    return ++uriCounter;
  });
});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

describe('NetworkLayerAMQP', () => {
  it('throws an exception if instantied with missing parameters', () => {
    expect(() => {
      new NetworkLayerAMQP();
    }).toThrow('You must pass the AMQP broker URL');

    expect(() => {
      new NetworkLayerAMQP("amqp://guest:guest@localhost:5672");
    }).toThrow('You must pass a default Topic exchange name');
  });

  it('throws an exception if method called before connection', async () => {
    let networkLayer = new NetworkLayerAMQP("amqp://guest:guest@localhost:5672", "TopicExchange");

    await expect(networkLayer.request("action", {})).rejects.toThrow('AMQP layer is not connected. Please call NetworkLayerAMQP::connect()');
    await expect(networkLayer.createCallbackQueue("*", () => {
    })).rejects.toThrow('AMQP layer is not connected. Please call NetworkLayerAMQP::connect()');
    await expect(networkLayer.publish("action", {})).rejects.toThrow('AMQP layer is not connected. Please call NetworkLayerAMQP::connect()');
    await expect(networkLayer.sendToQueue("queueName", {})).rejects.toThrow('AMQP layer is not connected. Please call NetworkLayerAMQP::connect()');
    await expect(networkLayer.listen("*", () => {
    })).rejects.toThrow('AMQP layer is not connected. Please call NetworkLayerAMQP::connect()');
  });

  it('should connect and handle basic handlers', async () => {
    let networkLayer = new NetworkLayerAMQP("amqp://guest:guest@localhost:5672", "TopicExchange");
    await networkLayer.connect();

    expect(networkLayer.isConnected()).toBe(true);

    let callbackFn = jest.fn();

    await networkLayer.createCallbackQueue("cbQueue", callbackFn, 'test.create');

    expect(await networkLayer.sendToQueue("cbQueue", {"cmd": "AAA"})).toBeUndefined();

    expect(await networkLayer.publish("test.create", {"cmd": "BBB"})).toBeUndefined();

    expect(callbackFn.mock.calls.length).toBe(2);
    expect(callbackFn.mock.calls[0][0]).toBe(JSON.stringify({"cmd": "AAA"}));
    expect(callbackFn.mock.calls[1][0]).toBe(JSON.stringify({"cmd": "BBB"}));

    await networkLayer.sendToQueue("cbQueue", {"cmd": "AAA"}, {contentType: "application/json"});

    expect(callbackFn.mock.calls[2][0]).toEqual({"cmd": "AAA"});

    let rpcFn1 = jest.fn(async (message, _, {replyTo, correlationId}) => {
      expect(message).toEqual({cmd: "AAA"});
      await networkLayer.sendToQueue(replyTo, {"rep": message.cmd}, {correlationId, contentType: "application/json"})
    });

    let rpcFn2 = jest.fn(async (message, _, {replyTo, correlationId}) => {
      expect(message).toEqual({cmd: "AAA"});
    });

    await networkLayer.listen("test.create.node", [rpcFn1, rpcFn2]);
    expect(await networkLayer.request("test.create.node", {cmd: "AAA"})).toEqual({rep: "AAA"});
    expect(rpcFn1.mock.calls.length).toBe(1);
    expect(rpcFn2.mock.calls.length).toBe(1);

    amqplib.reset();
  });

  it('should listen on everything', async () => {
    let networkLayer = new NetworkLayerAMQP("amqp://guest:guest@localhost:5672", "TopicExchange");
    await networkLayer.connect();

    let rpcFn = jest.fn();

    jest.spyOn(networkLayer, 'createCallbackQueue').mockImplementation((queueName, callbacks, routingKey) => {
      expect(routingKey).toBe('#');
    });

    await networkLayer.listen(null, rpcFn);
    await networkLayer.publish("blabla", {"test": "AAAA"});
    amqplib.reset();
  });

  it('should reconnect after network error', async () => {
    let networkLayer = new NetworkLayerAMQP("amqp://guest:guest@localhost:5672", "TopicExchange");
    await networkLayer.connect();

    expect(networkLayer.isConnected()).toBe(true);

    let reconnectSpy = jest.spyOn(networkLayer, 'reconnect');
    networkLayer._connection.on.withArgs("close").yield();

    expect(networkLayer.isConnected()).toBe(false);

    await sleep(2000);

    expect(reconnectSpy).toHaveBeenCalled();
    expect(networkLayer.isConnected()).toBe(true);

    amqplib.reset();
  });


  it('should not connect twice', async () => {
    let networkLayer = new NetworkLayerAMQP("amqp://guest:guest@localhost:5672", "TopicExchange");
    await networkLayer.connect();

    let reconnectSpy = jest.spyOn(networkLayer, 'reconnect');

    await networkLayer.connect();

    expect(reconnectSpy).not.toHaveBeenCalled();

    amqplib.reset();
  });
});




