/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import {LinkPath} from "../../../toolkit/utils/linkPath/LinkPath";

export default class LinkPathConcatStepDefinitionMock extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'mnx:LinkPathConcatStepDefinitionMock';
  }

  static getIndexType(){
    return "linkPathConcatStepDefinitionMock";
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    const firstNameProperty =  new LiteralDefinition({
      literalName: 'firstName',
      rdfDataProperty: 'foaf:firstName',
      isSearchable: true,
      searchBoost: 50
    });

    const lastNameProperty = new LiteralDefinition({
      literalName: 'lastName',
      rdfDataProperty: 'foaf:lastName',
      isSearchable: true,
      searchBoost: 50
    });

    return [
      ...super.getLiterals(),
      firstNameProperty,
      lastNameProperty,
      new LiteralDefinition({
        literalName: 'fullName',
        linkPath: new LinkPath()
          .concat({
            linkPaths: [
              (new LinkPath()).property({ propertyDefinition: firstNameProperty }),
              (new LinkPath()).property({ propertyDefinition: lastNameProperty }),
            ],
            separator: " "
          }),
        isRequired: true,
        isSearchable: true
      })
    ];
  }
};