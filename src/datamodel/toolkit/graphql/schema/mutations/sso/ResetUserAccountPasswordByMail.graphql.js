/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {allow} from "graphql-shield";
import {object, string} from "yup";

export let ResetUserAccountPasswordByMailMutation = `
"""Reset account mutation payload"""
type ResetUserAccountPasswordByMailPayload {
  success: Boolean
}

"""Reset account mutation input"""
input ResetUserAccountPasswordByMailInput {
  email: String
  redirectUri: String 
}

extend type Mutation{
  """ 
  This mutation input parameters are inforced by Yup middleware and returns a FORM_VALIDATION_ERROR listing input error details.
  
  Possible i18n error keys are :
  
    - IS_REQUIRED
    - EMAIL_MISFORMED
  """
  resetUserAccountPasswordByMail(input: ResetUserAccountPasswordByMailInput!): ResetUserAccountPasswordByMailPayload
}
`;

export let ResetUserAccountPasswordByMailMutationResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {string} redirectUri
     * @param {string} username
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    resetUserAccountPasswordByMail: {
      validationSchema: object().shape({
        input: object().shape({
          email:  string().trim().required("IS_REQUIRED").email("EMAIL_MISFORMED")
        })
      }),
      resolve: async (_, {input: {redirectUri, email}}, synaptixSession) => {
        await synaptixSession.getSSOControllerService().resetPasswordByMail({username: email, redirectUri});
        return {success: true};
      }
    }
  },
};


export let ResetUserAccountPasswordByMailMutationShieldRules = {
  Mutation: {
    resetUserAccountPasswordByMail: allow
  }
};