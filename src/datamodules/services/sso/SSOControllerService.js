/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {logDebug, logError} from "../../../utilities/logger";
import SSOUser from "../../drivers/sso/models/SSOUser";
import {I18nError} from "../../../utilities/error/I18nError";
import env from "env-var";

/**
 * Service to provide SSO features.
 */
export default class SSOControllerService {
  /** @type {SSOApiClient} */
  _ssoApiClient;

  /** @type {SSOMiddleware[]} */
  _ssoMiddlewares;

  /** @type {SynaptixDatastoreSession} */
  _datastoreSession;

  /** @type {string} */
  _ssoClientId;

  /** @type {string} */
  _ssoClientSecret;

  /** @type {string} */
  _ssoTokenEndpointUrl;

  /**
   *
   * @param {SSOApiClient} ssoApiClient
   * @param {SSOMiddleware[]} middlewares
   * @param {SynaptixDatastoreSession} datastoreSession
   * @param {string} ssoClientId
   * @param {string} ssoClientSecret
   * @param {string} ssoTokenEndpointUrl
   */
  constructor({ssoApiClient, ssoMiddlewares, datastoreSession, ssoClientId, ssoClientSecret, ssoTokenEndpointUrl}) {
    this._ssoApiClient = ssoApiClient;
    this._ssoMiddlewares = ssoMiddlewares;
    this._datastoreSession = datastoreSession;
    this._ssoClientId = ssoClientId;
    this._ssoClientSecret = ssoClientSecret;
    this._ssoTokenEndpointUrl = ssoTokenEndpointUrl;
  }

  /**
   * @return {SSOApiClient}
   */
  getApiClient() {
    return this._ssoApiClient;
  }

  /**
   * Register user account
   *
   * @param {string} username
   * @param {string} email
   * @param {string} firstName
   * @param {string} lastName
   * @param {string} nickName
   * @param {string} password
   * @param {string} [isTemporaryPassword]
   * @param {object} [userAttributes] - User attribues to add in SSO store.
   * @param {object} [personInput] - See PersonInput GraphQL definition.
   * @return {SSOUser}
   */
  async registerUserAccount({username, email, firstName, lastName, nickName, password, isTemporaryPassword, userAttributes, personInput}) {
    logDebug(`Account creation for ${username || email}`);
    let user = await this._ssoApiClient.createUser({
      username: username || email,
      email,
      password,
      isTemporaryPassword,
      firstName: firstName || nickName,
      lastName,
      attributes: userAttributes
    });

    logDebug(`Account creation OK`);
    logDebug(`Applying "afterRegister" middlewares.`);

    try {
      if(personInput?.id){
        personInput.id = this._datastoreSession.extractIdFromGlobalId(personInput.id);
      }

      for (let ssoMiddleware of this._ssoMiddlewares || []) {
        await ssoMiddleware.afterRegister({
          user,
          ssoApiClient: this._ssoApiClient,
          datastoreSession: this._datastoreSession,
          requestParams: {
            username,
            email: email || username,
            firstName,
            lastName,
            nickName,
            password,
            isTemporaryPassword,
            personInput
          }
        })
      }
    } catch (e) {
      logDebug(`There is an exception from a SSO middleware, removing user...`);

      await this._ssoApiClient.removeUser(user.getId());

      logDebug(`User removed.`);

      throw e;
    }

    return user;
  }

  /**
   * Login user.
   *
   * Caution : In order to work your SSO client must check "Direct Access Grants Enabled".
   *
   * @param username
   * @param password
   * @param {boolean} [skipCookie] - Don't set cookie in response.
   * @return {SSOUser}
   */
  async login({username, password, skipCookie = false} = {}) {
    let user = await this._ssoApiClient.loginUser({
      username,
      password,
      clientId: this._ssoClientId,
      clientSecret: this._ssoClientSecret,
      tokenEndpointUrl: this._ssoTokenEndpointUrl
    });

    if (!skipCookie){
      user.toCookie(this._datastoreSession.getResponse());
      
      if (env.get("USE_AUTH_HEADER").default("0").asBool()) {
        user.setAuthHeader(this._datastoreSession.getResponse());
      } 
    }

    return user;
  }

  /**
   * Logout user
   * @return {boolean}
   */
  async logout() {
    if (this._datastoreSession.getContext().isAnonymous()) {
      throw new I18nError("This is an anonymous session, user can't be logged out", "USER_MUST_BE_AUTHENTICATED");
    } else {
      try {
        await this._ssoApiClient.logoutUser(this._datastoreSession.getLoggedUserId());
      } catch (e) {
        logError(e.message);
      }
      SSOUser.clearCookie(this._datastoreSession.getResponse());
    }
  }

  /**
   * Reset user password
   * @param {string} oldPassword
   * @param {string} newPassword
   * @return {boolean}
   */
  async resetPassword({oldPassword, newPassword}) {
    if (this._datastoreSession.getContext().isAnonymous()) {
      throw new I18nError("This is an anonymous session, user password can't be renewed", "USER_MUST_BE_AUTHENTICATED");
    } else {
      // Test old password
      await this._ssoApiClient.loginUser({
        username: this._datastoreSession.getLoggedUsername(),
        password: oldPassword,
        clientId: this._ssoClientId,
        clientSecret: this._ssoClientSecret,
        tokenEndpointUrl: this._ssoTokenEndpointUrl
      });


      return this._ssoApiClient.resetUserPassword({
        password: newPassword,
        userId: this._datastoreSession.getLoggedUserId()
      });
    }
  }

  /**
   * Reset user password given a username by sending him a mail
   * @param {string} username
   * @param {string} [redirectUri]
   * @param {string} [clientId]
   * @return {boolean}
   */
  async resetPasswordByMail({username, redirectUri, clientId}) {
    try {
      let user = await this._ssoApiClient.getUserByUsername(username);
      logDebug(`Password reset by mail for userId ${user.getId()}`);

      let rsp = await this._ssoApiClient.resetUserPasswordByMail({
        redirectUri,
        clientId: clientId || this._ssoClientId,
        userId: user.getId()
      });
    } catch (e) {
      // If user is not in SSO, mute the error for security reason
      if (e instanceof I18nError && e.i18nKey === "USER_NOT_IN_SSO") {
        logDebug(`Password reset impossible, ${username} is not in SSO.`);
        return true;
      }

      throw e;
    }

  }

  /**
   * Login a user without password, by using impersonation
   * 
   * @param {string} username Username of user for which we request an access token
   * @param {string} impersonatorUsername Username of the user providing token to exchange
   * @param {string} impersonatorPassword Password of the user providing token to exchange
   * @param {boolean} [skipCookie]
   * @return {boolean}
   */
  async loginByImpersonation({username, impersonatorUsername, impersonatorPassword, skipCookie = false} = {}) {
    let impersonatorUser = await this._ssoApiClient.loginUser({
      username: impersonatorUsername,
      password: impersonatorPassword,
      clientId: this._ssoClientId,
      clientSecret: this._ssoClientSecret,
      tokenEndpointUrl: this._ssoTokenEndpointUrl
    });

    // Get an access token for user with username, in exchange of token given by impersonator
    const user = await this._ssoApiClient.impersonate({
      username,
      impersonator: impersonatorUser,
      clientId: this._ssoClientId,
      clientSecret: this._ssoClientSecret,
      tokenEndpointUrl: this._ssoTokenEndpointUrl
    })

    if (!skipCookie){
      user.toCookie(this._datastoreSession.getResponse());
      
      if (env.get("USE_AUTH_HEADER").default("0").asBool()) {
        user.setAuthHeader(this._datastoreSession.getResponse());
      } 
    }

    return user
  }

  /**
   * Unregister user account
   *
   * @param {string} [userId] - UserId to unregister. Current logged user id is token if not provided.
   * @param {boolean} [permanent] - GDPR "right to erasure"
   * @return {boolean}
   */
  async unregisterUserAccount({userId, permanent}) {
    if (this._datastoreSession.getContext().isAnonymous()) {
      throw new I18nError("This is an anonymous session, user can't be logged out", "USER_MUST_BE_AUTHENTICATED");
    } else {
      let clearCookie = !userId;
      let user;

      if (!userId) {
        user = await this._datastoreSession.getContext().getUser();
        userId = this._datastoreSession.getLoggedUserId();
      } else {
        user = await this._ssoApiClient.getUserById(userId);
      }

      logDebug(`Account deletion for userId ${userId}`);

      await this._ssoApiClient.removeUser(userId);

      if (clearCookie) {
        SSOUser.clearCookie(this._datastoreSession.getResponse());
      }

      logDebug(`Account removal OK`);
      logDebug(`Applying "afterUnregister" middlewares.`);

      for (let ssoMiddleware of this._ssoMiddlewares || []) {
        await ssoMiddleware.afterUnregister({
          user,
          ssoApiClient: this._ssoApiClient,
          datastoreSession: this._datastoreSession,
          permanent
        })
      }
    }
  }

  /**
   * Disable user account
   *
   * @param {string} [userId] - UserId to unregister. Current logged user id is token if not provided.
   * @return {boolean}
   */
  async disableUserAccount({userId}) {
    let user = await this._ssoApiClient.getUserById(userId);

    logDebug(`Account disabling for userId ${userId}`);
    await this._ssoApiClient.disableUser(userId);

    logDebug(`Account disabling OK`);
    logDebug(`Applying "afterAccountDisabling" middlewares.`);

    for (let ssoMiddleware of this._ssoMiddlewares || []) {
      await ssoMiddleware.afterAccountDisabling({
        user,
        ssoApiClient: this._ssoApiClient,
        datastoreSession: this._datastoreSession
      })
    }
  }

  /**
   * Disable user account
   *
   * @param {string} [userId] - UserId to unregister. Current logged user id is token if not provided.
   * @return {boolean}
   */
  async enableUserAccount({userId}) {
    let user = await this._ssoApiClient.getUserById(userId);

    logDebug(`Account enabling for userId ${userId}`);
    await this._ssoApiClient.enableUser(userId);

    logDebug(`Account disabling OK`);
    logDebug(`Applying "afterAccountDisabling" middlewares.`);

    for (let ssoMiddleware of this._ssoMiddlewares || []) {
      await ssoMiddleware.afterAccountEnabling({
        user,
        ssoApiClient: this._ssoApiClient,
        datastoreSession: this._datastoreSession
      })
    }
  }

  /**
   * @param userId
   * @return {boolean}
   */
  async isUserDisabled({userId}) {
    return (await this._ssoApiClient.getUserById(userId)).isDisabled();
  }

  /**
   * Apply an afterRegister mechanism for a user.
   *
   * @param {SSOUser} user
   * @param {object} [requestParams]
   */
  async synchronizeUserAccount({user, requestParams}){
    for (let ssoMiddleware of this._ssoMiddlewares || []) {
      await ssoMiddleware.afterRegister({
        user,
        ssoApiClient: this._ssoApiClient,
        datastoreSession: this._datastoreSession,
        requestParams
      })
    }
  }
}
