/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import IndexControllerService from "../IndexControllerService";
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../../../datamodel/toolkit/graphql/GraphQLContext";
import BarDefinitionMock from "../../../../datamodel/__tests__/mocks/definitions/BarDefinitionMock";
import Model from "../../../../datamodel/toolkit/models/Model";
import {
  PropertyFilter
} from "../../../../datamodel/toolkit/utils/filter";
import ModelDefinitionsRegister from "../../../../datamodel/toolkit/definitions/ModelDefinitionsRegister";
import LinkPathConcatStepDefinitionMock
  from "../../../../datamodel/__tests__/mocks/definitions/LinkPathConcatStepDefinitionMock";
import {FragmentDefinition} from "../../../../datamodel/toolkit/definitions/FragmentDefinition";

jest.mock("nanoid/generate");

let indexControllerService = new IndexControllerService({
  networkLayer: new NetworkLayerAMQP("amqp://", "fake"),
  graphQLcontext: new GraphQLContext({ anonymous: true, lang: "fr" }),
  modelDefinitionsRegister: new ModelDefinitionsRegister([BarDefinitionMock]),
  typesPrefix: "types-prefix-"
});

describe("IndexControllerService", () => {
  let querySpyFn = jest.spyOn(
    indexControllerService.getIndexPublisher(),
    "query"
  );

  it("should get a node", async () => {
    querySpyFn.mockImplementation(() => ({
      total: 1,
      hits: [
        {
          _id: "test:bar/1",
          _source: {
            barLiteral1: "literal 1 content",
            barLabel1: [
              {
                value: "label 1 content",
                lang: "fr"
              }
            ],
            barLabel2: [
              {
                value: "label 2 content",
                lang: "fr"
              }
            ]
          }
        }
      ]
    }));

    let node = await indexControllerService.getNode({
      id: "test:bar/1",
      modelDefinition: BarDefinitionMock
    });

    expect(querySpyFn).toHaveBeenCalledWith({
      source: {
        _source: {
          includes: "*"
        },
        query: {
          bool: {
            must: [
              {
                ids: {
                  values: ["test:bar/1"]
                }
              }
            ]
          }
        },
        track_total_hits: true,
      },
      types: "bar"
    });

    expect(node).toEqual(
      new Model(
        "test:bar/1",
        "test:bar/1",
        {
          barLiteral1: "literal 1 content",
          barLabel1: [
            {
              value: "label 1 content",
              lang: "fr"
            }
          ],
          barLabel2: [
            {
              value: "label 2 content",
              lang: "fr"
            }
          ]
        },
        "Bar"
      )
    );
  });

  it("should get a list of nodes with basic search", async () => {
    querySpyFn.mockImplementation(() => ({
      total: 1,
      hits: [
        {
          _id: "test:bar/1",
          _source: {
            barLiteral1: "literal 1 content",
            barLabel1: [
              {
                value: "label 1 content",
                lang: "fr"
              }
            ],
            barLabel2: [
              {
                value: "label 2 content",
                lang: "fr"
              }
            ]
          }
        },
        {
          _id: "test:bar/2",
          _source: {
            barLiteral1: "literal 1 content 2"
          }
        }
      ]
    }));

    let nodes = await indexControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "hello"
    });

    expect(querySpyFn).toHaveBeenCalledWith({
      source: {
        _source: {
          includes: "*"
        },
        query: {
          bool: {
            should: [
              {
                match: {
                  barLiteral1: {
                    boost: 3,
                    query: "hello",
                    fuzziness: 0
                  }
                }
              },
              {
                match: {
                  barLiteral2: {
                    query: "hello",
                    fuzziness: 0
                  }
                }
              },
              {
                match: {
                  barLabel1: {
                    query: "hello",
                    fuzziness: 0
                  }
                }
              },
              {
                simple_query_string: {
                  query:  "hello",
                  fields: ["barLiteral1", "barLiteral2", "barLabel1"],
                  default_operator: "and",
                  boost: 50
                }
              }
            ],
            minimum_should_match: 1
          }
        },
        size: 1000,
        sort: ["_score"],
        track_total_hits: true,
      },
      types: "bar"
    });

    expect(nodes).toEqual([
      new Model(
        "test:bar/1",
        "test:bar/1",
        {
          barLiteral1: "literal 1 content",
          barLabel1: [
            {
              value: "label 1 content",
              lang: "fr"
            }
          ],
          barLabel2: [
            {
              value: "label 2 content",
              lang: "fr"
            }
          ]
        },
        "Bar"
      ),
      new Model(
        "test:bar/2",
        "test:bar/2",
        {
          barLiteral1: "literal 1 content 2"
        },
        "Bar"
      )
    ]);
  });

  it("should get a list of nodes with basic search and pagination", async () => {
    querySpyFn.mockImplementation(() => ({
      total: 1,
      hits: []
    }));

    await indexControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "hello",
      limit: 10,
      offset: 15
    });

    expect(querySpyFn).toHaveBeenCalledWith({
      source: {
        _source: {
          includes: "*"
        },
        query: {
          bool: {
            should: [
              {
                match: {
                  barLiteral1: {
                    query: "hello",
                    fuzziness: 0,
                    boost: 3,
                  }
                }
              },
              {
                match: {
                  barLiteral2: {
                    query: "hello",
                    fuzziness: 0
                  }
                }
              },
              {
                match: {
                  barLabel1: {
                    query: "hello",
                    fuzziness: 0
                  }
                }
              },
              {
                simple_query_string: {
                  query:  "hello",
                  fields: ["barLiteral1", "barLiteral2", "barLabel1"],
                  default_operator: "and",
                  boost: 50
                }
              }
            ],
            minimum_should_match: 1
          }
        },
        from: 15,
        size: 10,
        sort: ["_score"],
        track_total_hits: true,
      },
      types: "bar"
    });
  });

  it("should get a list of nodes with phrase prefix search", async () => {
    querySpyFn.mockImplementation(() => ({
      total: 1,
      hits: []
    }));

    await indexControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "^hello.*"
    });

    expect(querySpyFn).toHaveBeenCalledWith({
      source: {
        _source: {
          includes: "*"
        },
        query: {
          bool: {
            should: [
              {
                match_bool_prefix: {
                  barLiteral1: {
                    "boost": 3,
                    query: "hello"
                  }
                }
              },
              {
                match_bool_prefix: {
                  barLiteral2: {
                    query: "hello"
                  }
                }
              },
              {
                match_bool_prefix: {
                  barLabel1: {
                    query: "hello"
                  }
                }
              },
              {
                simple_query_string: {
                  query:  "hello",
                  fields: ["barLiteral1", "barLiteral2", "barLabel1"],
                  default_operator: "and",
                  boost: 50
                }
              }
            ],
            minimum_should_match: 1
          }
        },
        size: 1000,
        sort: ["_score"],
        track_total_hits: true,
      },
      types: "bar"
    });
  });

  it("should get a list of nodes with regexp search", async () => {
    querySpyFn.mockImplementation(() => ({
      total: 1,
      hits: []
    }));

    await indexControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "/hello [^there]/"
    });

    expect(querySpyFn).toHaveBeenCalledWith({
      source: {
        _source: {
          includes: "*"
        },
        query: {
          bool: {
            should: [
              {
                regexp: {
                  barLiteral1: {
                    value: "hello [^there]",
                    flags: "ALL"
                  }
                }
              },
              {
                regexp: {
                  barLiteral2: {
                    value: "hello [^there]",
                    flags: "ALL"
                  }
                }
              },
              {
                regexp: {
                  barLabel1: {
                    value: "hello [^there]",
                    flags: "ALL"
                  }
                }
              }
            ],
            minimum_should_match: 1
          }
        },
        size: 1000,
        sort: ["_score"],
        track_total_hits: true,
      },
      types: "bar"
    });
  });

  it("should count a list of node", async () => {
    querySpyFn.mockImplementation(() => ({
      total: 1566,
      hits: []
    }));

    let count = await indexControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "/hello [^there]/",
      justCount: true
    });

    expect(count).toBe(1566);
  });

  it("should use runtime fields", async () => {
    querySpyFn.mockImplementation(() => ({
      total: 1,
      hits: [{
        "_id": "id_0",
        "_source": {
          "types": [
            "http://ns.mnemotix.com/ontologies/2019/8/generic-model/LinkPathConcatStepDefinitionMock",
          ],
          "fullName": [
            "Mathieu",
            "Rogelja"
          ],
        },
        "fields": {
          "fullName": [
            "Mathieu Rogelja"
          ]
        }
      }]
    }));

    const objects = await indexControllerService.getNodes({
      modelDefinition: LinkPathConcatStepDefinitionMock,
      limit: 1,
      fragmentDefinitions: [new FragmentDefinition({
        modelDefinition: LinkPathConcatStepDefinitionMock,
        properties: LinkPathConcatStepDefinitionMock.getProperties()
      })],

    });

    // Tests here MUST check that :
    //  - In ES query, "fullName.keyword" field is overrided (not just "fullName" field because broke fulltext search)
    //  - In ES document parsing, "fullname" property is overrided with a cancatenated string (whereas it's an array in ES)
    expect(querySpyFn).toHaveBeenCalledWith({
      source: {
        _source: {
          includes: ["types", "firstName", "lastName", "fullName"]
        },
        query: {
          bool: {
          }
        },
        fields: ["fullName.keyword_not_normalized"],
        runtime_mappings: {
          "fullName.keyword_not_normalized": {
            script: "if(params['_source'].containsKey('fullName')) { def field = params['_source']['fullName']; if(field instanceof List){ emit(field.stream().distinct().collect(Collectors.toList()).join(' ')); } else { emit(field); }}",
            type: "keyword"
          }
        },
        size: 1,
        track_total_hits: true,
      },
      types: "linkPathConcatStepDefinitionMock"
    });

    expect(objects[0].fullName).toEqual("Mathieu Rogelja")
  });

  it("should convert ids filters", async () => {
    let query = indexControllerService._convertFiltersIntoBoolQueryFragments({
      idsFilters: ["test/1234", "test/2334"]
    });
    expect(query).toEqual({
      should: [],
      must: [],
      mustNot: [],
      filter: [{ ids: { values: ["test/1234", "test/2334"] } }]
    });
  });

  it("should get a list of nodes in right order", async () => {
    querySpyFn.mockImplementation(() => ({
      total: 1,
      hits: [
        {
          _id: "test:bar/1",
          _source: {
          }
        },
        {
          _id: "test:bar/2",
          _source: {
          }
        },
        {
          _id: "test:bar/3",
          _source: {
          }
        }
      ]
    }));

    let nodes = await indexControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      idsFilters: [ "test:bar/2", "test:bar/1", "test:bar/3"]
    });

    expect(nodes).toEqual([
      new Model(
        "test:bar/2",
        "test:bar/2",
        {},
        "Bar"
      ),
      new Model(
        "test:bar/1",
        "test:bar/1",
        {},
        "Bar"
      ),
      new Model(
        "test:bar/3",
        "test:bar/3",
        {},
        "Bar"
      )
    ]);
  });

  it("should convert properties filters", async () => {
    let barLabel1LabelDefinition = BarDefinitionMock.getLabel("barLabel1");
    let barLiteral1LiteralDefinition = BarDefinitionMock.getLiteral(
      "barLiteral1"
    );
    let barLiteral2LiteralDefinition = BarDefinitionMock.getLiteral(
      "barLiteral2"
    );

    let query = indexControllerService._convertFiltersIntoBoolQueryFragments({
      propertyFilters: [
        new PropertyFilter({
          propertyDefinition: barLiteral1LiteralDefinition,
          value: "Derek",
          isStrict: true
        }),
        new PropertyFilter({
          propertyDefinition: barLiteral1LiteralDefinition,
          value: "Salmon",
          isNeq: true
        }),
        new PropertyFilter({
          propertyDefinition: barLabel1LabelDefinition,
          any: true
        }),
        new PropertyFilter({
          propertyDefinition: barLabel1LabelDefinition,
          any: true,
          isNeq: true
        }),
        new PropertyFilter({
          propertyDefinition: barLiteral2LiteralDefinition,
          value: "azer",
          isGt: true
        }),
        new PropertyFilter({
          propertyDefinition: barLiteral2LiteralDefinition,
          value: "azer",
          isGte: true
        }),
        new PropertyFilter({
          propertyDefinition: barLiteral2LiteralDefinition,
          value: "azer",
          isLt: true
        }),
        new PropertyFilter({
          propertyDefinition: barLiteral2LiteralDefinition,
          value: "azer",
          isLte: true
        }),
        new PropertyFilter({
          propertyDefinition: barLiteral2LiteralDefinition,
          any: true,
        }),
        new PropertyFilter({
          propertyDefinition: barLiteral2LiteralDefinition,
          any: true,
          isNeq: true
        })
      ]
    });

    expect(query).toEqual({
      should: [],
      must: [],
      mustNot: [],
      filter: [
        { term: { "barLiteral1.keyword_not_normalized": "Derek" } },
        { bool: { must_not: { term: { "barLiteral1.keyword": "Salmon" } } } },
        { exists: { field: "barLabel1" } },
        { bool: { must_not: { exists: { field: "barLabel1" } } } },
        { range: { barLiteral2: { gt: "azer" } } },
        { range: { barLiteral2: { gte: "azer" } } },
        { range: { barLiteral2: { lt: "azer" } } },
        { range: { barLiteral2: { lte: "azer" } } },
        { exists: { field: "barLiteral2" } },
        { bool: { must_not: { exists: { field: "barLiteral2" } } } },
      ]
    });
  });


  it("should get a list of nodes with basic search with nested localized label", async () => {
    querySpyFn.mockImplementation(() => ({
      total: 1,
      hits: [
        {
          _id: "test:bar/1",
          _source: {
            barLiteral1: "literal 1 content",
            barLabel1: [
              {
                value: "label 1 content",
                lang: "fr"
              }
            ],
            barLabel2: [
              {
                value: "label 2 content",
                lang: "fr"
              }
            ]
          }
        },
        {
          _id: "test:bar/2",
          _source: {
            barLiteral1: "literal 1 content 2"
          }
        }
      ]
    }));

    process.env.ES_NESTED_LOCALIZED_LABEL = 1;

    let nodes = await indexControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "hello"
    });

    process.env.ES_NESTED_LOCALIZED_LABEL = 0;

    expect(querySpyFn).toHaveBeenCalledWith({
      source: {
        _source: {
          includes: "*"
        },
        query: {
          bool: {
            should: [
              {
                match: {
                  barLiteral1: {
                    boost: 3,
                    query: "hello",
                    fuzziness: 0
                  }
                }
              },
              {
                match: {
                  barLiteral2: {
                    query: "hello",
                    fuzziness: 0
                  }
                }
              },
              {
                nested: {
                  path: "barLabel1",
                  query: {
                    match: {
                      "barLabel1.value": {
                        query: "hello",
                        fuzziness: 0
                      }
                    }
                  }
                }
              },
              {
                simple_query_string: {
                  query:  "hello",
                  fields: ["barLiteral1", "barLiteral2", "barLabel1"],
                  default_operator: "and",
                  boost: 50
                }
              }
            ],
            minimum_should_match: 1
          }
        },
        size: 1000,
        sort: ["_score"],
        track_total_hits: true,
      },
      types: "bar"
    });

    expect(nodes).toEqual([
      new Model(
        "test:bar/1",
        "test:bar/1",
        {
          barLiteral1: "literal 1 content",
          barLabel1: [
            {
              value: "label 1 content",
              lang: "fr"
            }
          ],
          barLabel2: [
            {
              value: "label 2 content",
              lang: "fr"
            }
          ]
        },
        "Bar"
      ),
      new Model(
        "test:bar/2",
        "test:bar/2",
        {
          barLiteral1: "literal 1 content 2"
        },
        "Bar"
      )
    ]);

  });

  it("should get a list of nodes with basic search and pagination with nested localized label", async () => {
    querySpyFn.mockImplementation(() => ({
      total: 1,
      hits: []
    }));

    process.env.ES_NESTED_LOCALIZED_LABEL = 1;

    await indexControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "hello",
      limit: 10,
      offset: 15
    });

    process.env.ES_NESTED_LOCALIZED_LABEL = 0;

    expect(querySpyFn).toHaveBeenCalledWith({
      source: {
        _source: {
          includes: "*"
        },
        query: {
          bool: {
            should: [
              {
                match: {
                  barLiteral1: {
                    query: "hello",
                    fuzziness: 0,
                    boost: 3,
                  }
                }
              },
              {
                match: {
                  barLiteral2: {
                    query: "hello",
                    fuzziness: 0
                  }
                }
              },
              {
                nested: {
                  path: "barLabel1",
                  query: {
                    match: {
                      "barLabel1.value": {
                        query: "hello",
                        fuzziness: 0
                      }
                    }
                  }
                }
              },
              {
                simple_query_string: {
                  query:  "hello",
                  fields: ["barLiteral1", "barLiteral2", "barLabel1"],
                  default_operator: "and",
                  boost: 50
                }
              }
            ],
            minimum_should_match: 1
          }
        },
        from: 15,
        size: 10,
        sort: ["_score"],
        track_total_hits: true,
      },
      types: "bar"
    });
  });

  it("should get a list of nodes with phrase prefix search with nested localized label", async () => {
    querySpyFn.mockImplementation(() => ({
      total: 1,
      hits: []
    }));

    process.env.ES_NESTED_LOCALIZED_LABEL = 1;

    await indexControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "^hello.*"
    });

    process.env.ES_NESTED_LOCALIZED_LABEL = 0;

    expect(querySpyFn).toHaveBeenCalledWith({
      source: {
        _source: {
          includes: "*"
        },
        query: {
          bool: {
            should: [
              {
                match_bool_prefix: {
                  barLiteral1: {
                    "boost": 3,
                    query: "hello"
                  }
                }
              },
              {
                match_bool_prefix: {
                  barLiteral2: {
                    query: "hello"
                  }
                }
              },
              {
                nested: {
                  path: "barLabel1",
                  query: {
                    match_bool_prefix: {
                      "barLabel1.value": {
                        query: "hello"
                      }
                    }
                  }
                }
              },
              {
                simple_query_string: {
                  query:  "hello",
                  fields: ["barLiteral1", "barLiteral2", "barLabel1"],
                  default_operator: "and",
                  boost: 50
                }
              }
            ],
            minimum_should_match: 1
          }
        },
        size: 1000,
        sort: ["_score"],
        track_total_hits: true,
      },
      types: "bar"
    });
  });

  it("should get a list of nodes with regexp search with nested localized label", async () => {

    querySpyFn.mockImplementation(() => ({
      total: 1,
      hits: []
    }));

    process.env.ES_NESTED_LOCALIZED_LABEL = 1;

    await indexControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "/hello [^there]/"
    });

    process.env.ES_NESTED_LOCALIZED_LABEL = 0;

    expect(querySpyFn).toHaveBeenCalledWith({
      source: {
        _source: {
          includes: "*"
        },
        query: {
          bool: {
            should: [
              {
                regexp: {
                  barLiteral1: {
                    value: "hello [^there]",
                    flags: "ALL"
                  }
                }
              },
              {
                regexp: {
                  barLiteral2: {
                    value: "hello [^there]",
                    flags: "ALL"
                  }
                }
              },
              {
                nested: {
                  path: "barLabel1",
                  query: {
                    regexp: {
                      "barLabel1.value": {
                        value: "hello [^there]",
                        flags: "ALL"
                      }
                    }
                  }
                }
              }
            ],
            minimum_should_match: 1
          }
        },
        size: 1000,
        sort: ["_score"],
        track_total_hits: true,
      },
      types: "bar"
    });
  });
});
