


/**
 * Query sent to the RDF store for getting all informations about models to generate
 * 
 * @param {*} modelName 
 */
export const RDF_MODELS_QUERY = (prefix, modelName) => {

    const uriFilter = (modelName)
        ? `FILTER REGEX(str(?model), "${prefix}${modelName}") .`
        : `FILTER (STRSTARTS(str(?model), "${prefix}")) .`;
    
    const query = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sesame: <http://www.openrdf.org/schema/sesame#>

SELECT DISTINCT ?model ?superClassName ?equivalentClassName ?propName ?propType ?valueType ?valueName ?cardinalityType ?cardinality ?comment
WHERE {
    ${uriFilter}
    {
        # Get a class
        ?model a owl:Class .
    } UNION {
        # Get name of super class
        ?model sesame:directSubClassOf ?superClassName .
    } UNION {
        # Get equivalent classes
        { ?model owl:equivalentClass ?equivalentClassName }
    } UNION {
        # Get equivalent classes
        { ?model rdfs:comment ?comment }
    } UNION {
        # Get type and name of properties
        VALUES ?propType { owl:DatatypeProperty owl:ObjectProperty }
        ?propName a ?propType .
        {
            # Get type and name of properties range, for properties with no restrictions 
            ?propName rdfs:domain ?model.
            OPTIONAL { ?propName rdfs:range ?valueType. }
            OPTIONAL { ?propName rdfs:ranlabelge ?valueName. }
        } UNION {
            # For the one with restrictions, get type, name of property range, and cardinality if indicated
            ?propName rdfs:domain ?model ;
            OPTIONAL { ?propName rdfs:label ?valueName }
            OPTIONAL { 
                ?propName rdfs:range ?rangeRestriction. 
                ?rangeRestriction a owl:Restriction.
                {
                VALUES ?cardinalityType { owl:cardinality owl:minQualifiedCardinality owl:maxQualifiedCardinality }
                ?rangeRestriction  owl:onClass ?valueType ;
                                                        ?cardinalityType ?cardinality .
            }
            }
        }
    }
    
}
ORDER BY ?model ?propType
`;

    // console.log(query);
    return query;
}