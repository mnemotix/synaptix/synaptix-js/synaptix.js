/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
export class Model {
  /** @type {string}*/
  id;
  /** @type {string} */
  uri;
  /** @type {string} */
  type;

  /**
   * @param {string} id
   * @param {string} [uri]
   * @param {object} [props={}]
   * @param {string} [type]
   */
  constructor(id, uri = null, props = {}, type = null) {
    if (arguments.length === 1 && typeof arguments[0] === "object"){
      ({id, uri, props, type} = arguments[0]);
    }

    this.id = id;
    this.uri = uri || id;
    this.type = type || this.constructor.name;

    Object.keys(props).map(prop => {
      this[prop] = props[prop];
    });
  }

  /**
   * @param type
   */
  setType(type){
    this.type = type;
  }
}

export default Model;