/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {I18nError} from "../../../../utilities/error/I18nError";

/**
 * Convert a prefixed URI into an absolute URI
 *
 * @param {string} uri - The URI to analyze.
 * @param {object} prefixes - A map of prefix/namespace URI
 * @return {*}
 */
export function replacePrefixToAbsoluteNamespace(uri, prefixes = {}) {
  if(typeof uri === "string"){
    let delimiter = uri.indexOf(':');
    if (delimiter >= 0) {
      let prefix = uri.slice(0, delimiter);
      if (prefixes[prefix]) {
        return uri.replace(`${prefix}:`, prefixes[prefix]);
      }
    }
  }

  return uri;
}

/**
 * Convert a prefixed URI into an absolute URI
 *
 * @param {string} uri - The URI to analyze.
 * @param {object} prefixes - A map of prefix/namespace URI
 * @return {*}
 */
export function replaceAbsoluteNamespaceToPrefix(uri, prefixes = {}) {
  if(typeof uri === "string") {
    let ns = Object.entries(prefixes).find(([prefix, namespaceUri]) => (uri || "").includes(namespaceUri));

    if (ns) {
      let [prefix, namespaceUri] = ns;
      return uri.replace(namespaceUri, `${prefix}:`);
    }
  }
  return uri;
}

/**
 * Convert a triple prefixed URI into an absolute URI
 *
 * @param {array} triple - Triple (https://github.com/RubenVerborgh/SPARQL.js)
 * @param {object} [prefixes={}] - Prefix mappings
 */
export function replacePrefixToAbsoluteNamespaceInTriple(triple, prefixes = {}) {
  let {subject, predicate, object} = triple;

  if (!subject || !predicate || !object){
    throw new I18nError(`There is a definition problem with triple ${JSON.stringify(triple)}. ${!subject ? '"subject"' : !predicate ? '"predicate"' : '"object"'} part is not defined. Check related ModelDefinition.`)
  }

  return {
    subject: replacePrefixToAbsoluteNamespace(subject, prefixes),
    predicate: replacePrefixToAbsoluteNamespace(predicate, prefixes),
    object: replacePrefixToAbsoluteNamespace(object, prefixes),
  };
}

/**
 * Convert list of triples prefixed URI into an absolute URI
 *
 * @param {array} triples - Array of triples (https://github.com/RubenVerborgh/SPARQL.js)
 * @param {object} [prefixes={}] - Prefix mappings
 */
export function replacePrefixToAbsoluteNamespaceInTriples(triples, prefixes = {}) {
  return triples.map((triple) => {
    return replacePrefixToAbsoluteNamespaceInTriple(triple, prefixes)
  });
}

/**
 * Parse a list of patterns find triples prefixed URI and convert them into an absolute URI
 * @param patterns
 * @param prefixes
 */
export function normalizePatterns(patterns, prefixes = {}) {
  return patterns.map(pattern => {
    if (pattern.type === "bgp") {
      return {
        type: "bgp",
        triples: replacePrefixToAbsoluteNamespaceInTriples(pattern.triples, prefixes)
      }
    }

    return pattern;
  })
}

/**
 * Normalize a list of triples
 * @param triples
 * @param prefixes
 * @param graphId
 */
export function normalizeTriples(triples, prefixes = {}, graphId) {
  if (triples?.length > 0){
    if (graphId){
      triples = [
        {
          type: "graph",
          triples: replacePrefixToAbsoluteNamespaceInTriples(triples, prefixes),
          name: replacePrefixToAbsoluteNamespace(graphId, prefixes)
        }
      ];
    } else {
      triples = [
        {
          type: "bgp",
          triples: replacePrefixToAbsoluteNamespaceInTriples(triples, prefixes)
        }
      ];
    }
  }

  return triples;
}

/**
 * Converts a list of triples into OPTIONAL
 * @param triples
 * @param prefixes
 * @return {*}
 */
export function convertWhereTriplesToOptional(triples, prefixes){
  return (triples || []).map(triple => {
    return {
      type: "optional",
      patterns: triple.type === "optional" ? normalizePatterns(triple.patterns, prefixes) : [
        {
          type: "bgp",
          triples: replacePrefixToAbsoluteNamespaceInTriples(Array.isArray(triple) ? triple : [triple], prefixes)
        }
      ]
    }
  })
}