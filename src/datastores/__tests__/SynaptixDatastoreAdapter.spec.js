/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import SynaptixDatastoreAdapter from '../SynaptixDatastoreAdapter';
import ModelDefinitionsRegister from "../../datamodel/toolkit/definitions/ModelDefinitionsRegister";
import SynaptixDatastoreSession from "../SynaptixDatastoreSession";
import NetworkLayerAMQP from "../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../datamodel/toolkit/graphql/GraphQLContext";

let modelDefinitionsRegister = new ModelDefinitionsRegister();
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let context = new GraphQLContext({
  anonymous: true, lang: 'fr'
});

fdescribe('SynaptixDatastoreAdapter', () => {
  it('throws an exception if extended with missing abstract method definitions', () => {
    expect(() => {
      new SynaptixDatastoreAdapter({});
    }).toThrow("An instance of class ModelDefinitionsRegister must be passed as param modelDefinitionsRegister");

    expect(() => {
      new SynaptixDatastoreAdapter({modelDefinitionsRegister});
    }).toThrow("A class  definition must be passed as param SynaptixDatastoreSessionClass");
  });

  it('throws an exception if not extended and init method called', () => {
    let adapter = new SynaptixDatastoreAdapter({
      modelDefinitionsRegister,
      SynaptixDatastoreSessionClass: SynaptixDatastoreSession,
      networkLayer
    });

    expect(() => {
      adapter.init();
    }).not.toThrow("You must implement SynaptixDatastoreAdapter::init()");
  });

  it('should return a session', () => {
    let adapter = new SynaptixDatastoreAdapter({
      modelDefinitionsRegister,
      SynaptixDatastoreSessionClass: SynaptixDatastoreSession,
      networkLayer
    });

    expect(adapter.getSession({context})).toBeInstanceOf(SynaptixDatastoreSession);
  });
});




