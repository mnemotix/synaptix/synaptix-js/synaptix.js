import {validationResult} from 'express-validator/check';


/**
 * This function is a expressJS middleware to be used along with 'express-validator' sanitization
 * middlewares (express-validator/check for example). This middleware must be used **after** express-validator middlewares.
 * If some validation checks failed, this middleware will send an HTTP response with error code 400 Bad Request, and a JSON
 * content describing the errors, following this convention :
 *
 *    HTTP/1.1 400 Bad Request
 *    {
 *      "errors": {
 *        "restParam1": "restParam1 is empty",
 *        "restParam2": "restParam2 should be at least 5 characters long"
 *      }   
 *    }
 *
 * The error messages are the ones defined when declaring the checks.
 *
 */
export function sendValidationErrorsJSON(req, res, next) {
  let validationRes = validationResult(req);
  if (!validationRes.isEmpty()) {
    let errorResponse = {errors: {}};
    validationRes.array().map((error) => {
      errorResponse.errors[error.param] = error.msg;
    });
    res.status(400).json(errorResponse);
  } else {
    next();
  }
}
