/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
require('util').inspect.defaultOptions.depth = null
export {default as DataModulePublisher} from "./datamodules/drivers/DataModulePublisher";
export {default as DataModuleConsumer} from "./datamodules/drivers/DataModuleConsumer";

export {
  SSOApiClient,
  MinioConsumer
} from "./datamodules/drivers";

export {
  IndexControllerService,
  GraphControllerService
} from "./datamodules/services";

export {
  GraphMiddleware,
  LegacyGraphMiddleware,
  MnxAclGraphMiddleware,
  MnxActionGraphMiddleware
} from "./datamodules/middlewares/graph";

export {
  default as SSOMiddleware
} from "./datamodules/middlewares/sso/SSOMiddleware";

export {
  logDebug, logError, logInfo, logWarning, logException
} from "./utilities/logger";

export {
  formatGraphQLError,
  generateGraphQLEndpoint,
  generateSSOEndpoint,
  initEnvironment,
  launchApplication,
  ExpressApp
} from "./utilities";

export {
  DatastoreAdapterAbstract,
  DatastoreSessionAbstract,
  SynaptixDatastoreAdapter,
  SynaptixDatastoreSession,
  SynaptixDatastoreRdfAdapter,
  SynaptixDatastoreRdfSession
} from "./datastores";

export {generateDefaultDatastoreAdapter} from "./utilities/express/generators/generateDefaultDatastoreAdapter";

export {
  default as NetworkLayerAbstract,
  default as NetworkLayer
} from "./network/NetworkLayer";

export {
  default as NetworkLayerAMQP
} from "./network/amqp/NetworkLayerAMQP";

export {MnxOntologies, MnxDatamodels} from "./datamodel/ontologies";

export {
  GraphQLContext, GraphQLPaginationArgs, GraphQLConnectionArgs, GraphQLConnectionDefinitions
} from "./datamodel/toolkit/graphql";

export {FragmentDefinition} from "./datamodel/toolkit/definitions/FragmentDefinition";

export {
  createModelFromGraphstoreNode, createModelFromIndexSyncDocument
} from "./datamodel/toolkit/models";

export {
  generateSchema,
  mergeSchemas
} from "./datamodel/toolkit/graphql/schema/utilities/generateSchema"

export {
  mergeResolvers, getObjectsResolver, getObjectsCountResolver, getLocalizedLabelResolver, getLinkedObjectsResolver,
  getLinkedObjectResolver, getObjectResolver, generateTypeResolvers, createObjectInConnectionResolver,
  updateObjectResolver, getMeResolver, createEdgeResolver, getLinkedObjectsCountResolver, getTypeResolver,
  generateInterfaceResolvers
} from "./datamodel/toolkit/graphql/helpers/resolverHelpers"

export {
  getFragmentDefinitionsRegisterFromGqlInfo,
} from "./datamodel/toolkit/graphql/helpers/fragmentHelpers"

export {
  ModelDefinitionsRegister,
  ModelDefinitionAbstract,
  UseIndexMatcherOfDefinition,
  LinkDefinition,
  LabelDefinition,
  LiteralDefinition,
  FilterDefinition,
  SortingDefinition,
  Link
} from "./datamodel/toolkit/definitions";

export {LinkPath, LinkStep, PropertyStep, PropertyFilterStep, UnionStep, MustExistFilterStep, MustNotExistFilterStep, ConcatStep} from "./datamodel/toolkit/utils/linkPath";
export {QueryFilter, LinkFilter, PropertyFilter, IdFilter} from "./datamodel/toolkit/utils/filter";
export {Sorting} from "./datamodel/toolkit/utils/Sorting";

export {
  generateCreateMutationDefinitionForType,
  generateCreateMutationDefinition,
  generateUpdateMutationDefinitionForType,
  generateUpdateMutationDefinition,
  generateCreateMutationResolverForType,
  generateCreateMutationResolver,
  generateUpdateMutationResolverForType,
  generateUpdateMutationResolver
} from "./datamodel/toolkit/graphql/schema/mutations/generators";

export {
  generateConnectionForType,
  generateConnectionResolverFor,
  filteringArgs,
  connectionArgs,
  generateConnectionArgs
} from "./datamodel/toolkit/graphql/schema/types/generators";

export {
  GraphQLDefinition,
  GraphQLInterfaceDefinition,
  GraphQLTypeDefinition,
  GraphQLExtensionDefinition,
  GraphQLProperty,
  GraphQLQuery,
  GraphQLTypeQuery,
  GraphQLTypeConnectionQuery,
  GraphQLMutation,
  GraphQLCreateMutation,
  GraphQLUpdateMutation,
  GraphQLRemoveMutation
} from "./datamodel/toolkit/graphql/schema";

export {default as EntityDefinition} from "./datamodel/ontologies/mnx-common/EntityDefinition";

export {Model} from "./datamodel/toolkit/models/Model";

export {DataModel} from "./datamodel/DataModel";

export {
  sendValidationErrorsJSON as sendValidationErrorsJSONExpressMiddleware
}from "./utilities/express/utils/sendValidationErrorsJSON";
export {
  attachDatastoreSession as attachDatastoreSessionExpressMiddleware
}from "./utilities/express/middlewares/attachDatastoreSession";


export {I18nError} from "./utilities/error/I18nError";
export {ApiValidationError} from "./utilities/error/ApiValidationError";
export {FormValidationError} from "./datamodel/toolkit/graphql/schema/middlewares/formValidation/FormValidationError";

// Generic
export {askSparqlRule, mergeRules, isAuthenticatedRule, isReadOnlyModeDisabledRule, isMutationEnabledRule} from "./datamodel/toolkit/graphql/schema/middlewares/aclValidation/rules";
export {isCreatorRule, isLoggedUserObjectCreator} from "./datamodel/ontologies/mnx-contribution/middlewares/aclValidation/rules/isCreatorRule";
export {isInUserGroupRule, isAdminRule} from "./datamodel/ontologies/mnx-agent/middlewares/aclValidation/rules";
export {replaceFieldValueIfRuleFailsMiddleware} from "./datamodel/toolkit/graphql/schema/middlewares/fieldReplacement/replaceFieldValueIfRuleFailsMiddleware";
export {restrictEntitiesWithPublicPolicyMiddleware} from "./datamodel/toolkit/graphql/schema/middlewares/addFilter/restrictEntitiesWithPublicPolicyMiddleware";
export {securizeEntitiesMiddlewares} from "./datamodel/ontologies/mnx-acl/middlewares/securizeEntitiesMiddlewares";

// Tools
// TODO: checkDatamodel currently disabled because of an error on rdflib import...
// TODO: this routine must be rewrited for analysing owl ontology file and not graph endpoint anymore
// export {checkDataModel} from './datamodel/toolkit/bin/ontologyChecker/checkDatamodel';

/// RETROCOMPATIBILITY

export {connectionFromArray, cursorToOffset, offsetToCursor} from "./datamodel/toolkit/graphql/helpers/connectionHelpers";
export {
  filteringArgs as paginationArgs
} from "./datamodel/toolkit/graphql/schema/types/generators";
export {
  SSOApiClient as SSOSyncClient
} from "./datamodules/drivers";
export {Model as ModelAbstract} from "./datamodel/toolkit/models/Model";
export {
  default as SSOActionMiddleware
} from "./datamodules/middlewares/sso/SSOMiddleware";
export {
  getFragmentDefinitionsRegisterFromGqlInfo as getFragmentDefinitionsFromGraphQLResolveInfo
} from "./datamodel/toolkit/graphql/helpers/fragmentHelpers"
