/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import FooDefinitionMock from "./FooDefinitionMock";
import BarDefinitionMock from "./BarDefinitionMock";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import {NotInstantiableDefinitionMock} from "./index";

export default class BazDefinitionMock extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Baz";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasFoo',
        pathInIndex: "foo",
        rdfObjectProperty: "mnx:hasFoo",
        relatedModelDefinition: FooDefinitionMock,
        isCascadingUpdated: true,
        graphQLInputName: "fooInput"
      }),
      new LinkDefinition({
        linkName: 'hasBar',
        rdfObjectProperty: "mnx:hasBar",
        relatedModelDefinition: BarDefinitionMock,
        symmetricLinkName: "hasBaz",
        isCascadingUpdated: true,
        graphQLInputName: "barInput"
      }),
      new LinkDefinition({
        linkName: 'hasNotInstantiable',
        rdfObjectProperty: "mnx:hasNotInstantiable",
        relatedModelDefinition: NotInstantiableDefinitionMock
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'bazLabel1',
        rdfDataProperty: "mnx:bazLabel1"
      })
    ];
  }
};