/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import GraphControllerService from "../GraphControllerService";
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../../../datamodel/toolkit/graphql/GraphQLContext";
import generateId from "nanoid/generate";
import {LegacyGraphMiddleware} from "../../../middlewares/graph/LegacyGraphMiddleware";
jest.mock("nanoid/generate");
import {Link} from "../../../../datamodel/toolkit/definitions/LinkDefinition";
import {BarDefinitionMock, BazDefinitionMock} from "../../../../datamodel/__tests__/mocks/definitions";
import {Model} from "../../../../datamodel/toolkit/models/Model";

let graphControllerService = new GraphControllerService({
  networkLayer: new NetworkLayerAMQP("amqp://", "fake"),
  graphQLcontext: new GraphQLContext({anonymous: true, lang: "fr"}),
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
  stripNewlineCharInSparqlQuery: false,
  middlewares: [new LegacyGraphMiddleware()]
});

describe('GraphControllerServiceWithLegacyCrudMiddleware', () => {
  let selectSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'select');
  let countSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'count');
  let constructSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'construct');
  let createTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'insertTriples');
  let updateTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'updateTriples');
  let askTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'ask');
  let deleteTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'deleteTriples');


  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
        return ++uriCounter;
      });
  });


  let now = Date.now();
  Date.now = jest.fn().mockImplementation(() => now);

  it('should create an edge', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.createEdge(BarDefinitionMock, "test:bar/1", BarDefinitionMock.getLink("hasBaz"), "test:baz/1");

    expect(createTriplesSpyFn).toHaveBeenCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:bar/1",
          "type": "mnx:Bar"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA { <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>. }`
    });
  });

  it('shoud create a simple node', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      lang: 'fr'
    })).toEqual(new Model('test:bar/1', 'http://ns.mnemotix.com/instances/bar/1', {
      barLiteral1: "John",
      barLiteral2: "Doe",
      barLabel1: [{
        lang: "fr",
        value: "Blabla en français"
      }],
      creationDate: now,
      lastUpdate: now
    }, 'Bar'));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:bar/1",
          "type": "mnx:Bar"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr.
}`
    });
  });

  it('shoud create a node and connect it to an existing target node', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      links: [
        new Link({
          linkDefinition: BarDefinitionMock.getLink('hasBaz'),
          targetId: 'test:baz/1'
        })
      ],
      lang: 'fr',
    })).toEqual(new Model('test:bar/1', 'http://ns.mnemotix.com/instances/bar/1', {
      barLiteral1: "John",
      barLiteral2: "Doe",
      barLabel1: [{
        lang: "fr",
        value: "Blabla en français"
      }],
      creationDate: now,
      lastUpdate: now
    }, 'Bar'));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:bar/1",
          "type": "mnx:Bar"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr;
  mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>.
}`
    });
  });

  it('shoud update a node', async () => {
    updateTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.updateNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        id: "test:bar/1",
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      lang: 'fr'
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE {
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2;
  mnx:barLabel1 ?barLabel1.
}
INSERT {
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr.
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1. }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel1 ?barLabel1.
  FILTER((LANG(?barLabel1)) = "fr")
 }
}`,
      messageContext: {indexable : []},
    });
  });

  it('shoud create a node and connect it to an existing target node with reversed property on link', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      links: [
        (new Link({
          linkDefinition: BazDefinitionMock.getLink('hasBar'),
          targetId: 'test:baz/1'
        })).reverse({
          targetModelDefinition: BazDefinitionMock
        })
      ],
      lang: 'fr',
    })).toEqual(new Model('test:bar/1', 'http://ns.mnemotix.com/instances/bar/1', {
      barLiteral1: "John",
      barLiteral2: "Doe",
      barLabel1: [{
        lang: "fr",
        value: "Blabla en français"
      }],
      creationDate: now,
      lastUpdate: now
    }, 'Bar'));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:bar/1",
          "type": "mnx:Bar"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/baz/1> mnx:hasBar <http://ns.mnemotix.com/instances/bar/1>.
}`
    });
  });

  it('shoud create a node and connect it to an new target node with reversed property on link', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      links: [
        (new Link({
          linkDefinition: BazDefinitionMock.getLink('hasBar'),
          targetObjectInput: {
            startDate: 123456,
          }
        })).reverse({
          targetModelDefinition: BazDefinitionMock
        })
      ],
      lang: 'fr',
    })).toEqual(new Model('test:bar/1', 'http://ns.mnemotix.com/instances/bar/1', {
      barLiteral1: "John",
      barLiteral2: "Doe",
      barLabel1: [{
        lang: "fr",
        value: "Blabla en français"
      }],
      creationDate: now,
      lastUpdate: now
    }, 'Bar'));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:bar/1",
          "type": "mnx:Bar"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/baz/2> rdf:type mnx:Baz;
  mnx:hasBar <http://ns.mnemotix.com/instances/bar/1>.
}`
    });
  });

  it('shoud delete single edge', async () => {
    deleteTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.deleteEdges([{
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/1",
      targetId: "test:baz/1",
      linkDefinition: BarDefinitionMock.getLink("hasBaz")
    }]);

    expect(deleteTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE DATA { <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>. }`,
      messageContext: {},
    });
  });

  it.only("shoud delete multiple edges", async () => {
    deleteTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.deleteEdges([{
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/1",
      targetId: "test:bazAccount/1",
      linkDefinition: BarDefinitionMock.getLink("hasBaz")
    }, {
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/1",
      targetId: "test:foo/2",
      linkDefinition: BarDefinitionMock.getLink("hasFoo")
    }]);

    expect(deleteTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE DATA {
 <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/bazAccount/1>;
  mnx:hasFoo <http://ns.mnemotix.com/instances/foo/2>.
}`,
      messageContext: {}
    });
  });

  it('shoud remove a node', async () => {
    deleteTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));
    updateTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.removeNode({
      modelDefinition: BarDefinitionMock,
      id: "test:bar/1"
    });

    expect(updateTriplesSpyFn).not.toBeCalled();
    expect(deleteTriplesSpyFn).not.toBeCalled();
  });
});