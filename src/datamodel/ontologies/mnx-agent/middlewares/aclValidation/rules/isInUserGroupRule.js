/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {rule} from 'graphql-shield';
import env from "env-var";
import {ShieldError} from "../../../../../../utilities/error/ShieldError";

/**
 * @param {Model} userAccount
 * @param {string} userGroupId
 * @param {SynaptixDatastoreSession} synaptixSession
 */
export let isUserAccountInUserGroup = ({userAccount, userGroupId, synaptixSession}) => {
  const fullFeatureEnabled = env.get("FULL_FEATURE_ENABLED").asBool();
  return fullFeatureEnabled || synaptixSession.isLoggedUserInGroup(userGroupId);
};


/**
 * @param {string|function} userGroupId
 * @return {Rule}
 */
export let isInUserGroupRule = ({userGroupId}) => rule()(
  /**
   * @param parent
   * @param args
   * @param {SynaptixDatastoreSession} synaptixSession
   * @return {*}
   */
  async (parent, args, synaptixSession) => {
    if (typeof userGroupId === "function") {
      userGroupId = userGroupId();
    }

    let userAccount = await synaptixSession.getLoggedUserAccount();

    if (!userAccount || !(await isUserAccountInUserGroup({userAccount, userGroupId, synaptixSession}))){
      return new ShieldError(`Not allowed !  (Blocked by \`hasUserGroupRule\` 🛡 rule)`, "USER_NOT_ALLOWED", 401);
    }

    return true;
  });