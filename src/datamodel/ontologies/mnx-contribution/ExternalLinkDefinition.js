/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import EntityDefinition from "../mnx-common/EntityDefinition";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";
import AccessPolicyDefinition from "../mnx-acl/AccessPolicyDefinition";
import {LinkPath} from "../../toolkit/utils/linkPath";

export default class ExternalLinkDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:ExternalLink";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'external-link';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    let parentLinks = super.getLinks();
    let indexOf = parentLinks.findIndex((link) => link.getLinkName() === "hasAccessPolicy");
    if(indexOf > 0){
      parentLinks.splice(indexOf, 1);
    }

    const hasEntityLinkDefinition = new LinkDefinition({
        linkName: 'hasEntity',
        rdfObjectProperty: "mnx:isExternalLinkOf",
        relatedModelDefinition: EntityDefinition,
        isPlural: true,
        graphQLPropertyName: "entities",
        graphQLInputName: "entityInputs"
      });

    return [
      ...parentLinks,
      hasEntityLinkDefinition,
      new LinkDefinition({
        linkName: 'hasAccessPolicy',
        relatedModelDefinition: AccessPolicyDefinition,
        graphQLPropertyName: "accessPolicy",
        linkPath: () => new LinkPath()
          .step({linkDefinition: hasEntityLinkDefinition})
          .step({linkDefinition: EntityDefinition.getLink("hasAccessPolicy")})
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'label',
        rdfDataProperty: "rdfs:label"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'link',
        rdfDataProperty: "mnx:link"
      })
    ];
  }
};