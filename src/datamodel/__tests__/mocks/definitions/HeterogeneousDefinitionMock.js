/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import BazDefinitionMock from "./BazDefinitionMock";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import NotInstantiableDefinitionMock from "./NotInstantiableDefinitionMock";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import FooDefinitionMock from "./FooDefinitionMock";
import {LinkPath} from "../../../toolkit/utils/linkPath";
import PersonDefinition from "../../../ontologies/mnx-agent/PersonDefinition";
import {BarDefinitionMock} from "./index";

export default class HeterogeneousDefinitionMock extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'mnx:Hetero';
  }

  static getIndexType(){
    return 'hetero';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasBar',
        rdfObjectProperty: "mnx:hasBar",
        relatedModelDefinition: BarDefinitionMock,
        graphQLPropertyName: "bar",
        graphQLInputName: "barInputs"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'heteroLiteral1',
        rdfDataProperty: 'mnx:heteroLiteral1',
      }),
      new LiteralDefinition({
        literalName: 'barLiteral1',
        linkPath: new LinkPath()
          .step({linkDefinition: HeterogeneousDefinitionMock.getLink("hasBar")})
          .property({
            propertyDefinition: BarDefinitionMock.getLiteral("barLiteral1"),
            rdfDataPropertyAlias: 'mnx:barLiteral1'
          }),
        isSearchable: true,
        inIndexOnly: true
      }),
    ];
  }
};