/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import BazDefinitionMock from "./BazDefinitionMock";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import {LinkPath} from "../../../toolkit/utils/linkPath/LinkPath";

export default class LinkPathPropertyDefinitionMock extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'mnx:LinkPathPropertyBindingMock';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasBaz',
        relatedModelDefinition: BazDefinitionMock,
        rdfReversedObjectProperty: "mnx:hasBaz",
        isPlural: true,
        isCascadingRemoved: true,
        graphQLInputName: "bazInputs"
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'mustExistProperty',
        linkPath: new LinkPath()
          .mustExistPropertyBinding({
            rdfDataPropertyAlias: "mnx:mustExistProperty",
            bindAs: "mustExistProperty",
            linkPath: new LinkPath()
              .step({linkDefinition: this.getLink("hasBaz")})
              .step({linkDefinition: BazDefinitionMock.getLink("hasFoo")})
          }),
      }),
      new LiteralDefinition({
        literalName: 'mustNotExistProperty',
        linkPath: new LinkPath()
          .mustNotExistPropertyBinding({
            rdfDataPropertyAlias: "mnx:mustNotExistProperty",
            bindAs: "mustNotExistProperty",
            linkPath: new LinkPath()
              .step({linkDefinition: this.getLink("hasBaz")})
          }),
      })
    ];
  }
};