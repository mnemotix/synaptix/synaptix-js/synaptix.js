/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import ModelDefinitionAbstract from '../ModelDefinitionAbstract';
import LinkDefinition from "../LinkDefinition";
import LabelDefinition from "../LabelDefinition";
import LiteralDefinition from "../LiteralDefinition";
import Model from "../../models/Model";
import EntityDefinition from "../../../ontologies/mnx-common/EntityDefinition";

const mockedGeneratedID = "blabla";

jest.mock("nanoid/generate", () => {
  return jest.fn().mockImplementation(() => mockedGeneratedID);
});

class NotInstantiableModelDefinition extends ModelDefinitionAbstract {
  static getParentDefinitions(){
    return [EntityDefinition];
  }

  static getRdfType() {
    return "foo:NotInstantiableType";
  }

  static isInstantiable(){
    return false;
  }

  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'notInstantiableTypeLiteral1',
        rdfDataProperty: 'mnx:notInstantiableTypeLiteral1',
      })
    ];
  }
}

class InheritedModelDefinition extends ModelDefinitionAbstract {
  static getRdfType() {
    return "foo:InheritedType";
  }

  static getParentDefinitions(){
    return [NotInstantiableModelDefinition];
  }

  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'inheritedTypeLiteral1',
        rdfDataProperty: 'mnx:inheritedTypeLiteral1',
      })
    ];
  }
}

class PrefixedBarModelDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "foo": "http://foo.org/"
    };
  }

  static getRdfType() {
    return "foo:Bar";
  }

  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'fooLink1',
        pathInIndex: 'fooLink1',
        rdfObjectProperty: "mnx:fooLink1",
        relatedModelDefinition: AbsoluteBarModelDefinition,
        graphQLInputName: "fooLinkInput"
      }),
      new LinkDefinition({
        linkName: 'barLink1',
        pathInIndex: 'barLink1',
        rdfObjectProperty: "mnx:barLink1",
        relatedModelDefinition: AbsoluteHashedBarModelDefinition,
        graphQLInputName: "barLinkInput",
        isPlural: true
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'fooLabel1',
        pathInIndex: 'fooLabel1',
        rdfDataProperty: "mnx:fooLabel1",
        isSearchable: true,
        inputFormOptions: 'type: "textarea"'
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'fooLiteral1',
        rdfDataProperty: 'mnx:fooLiteral1',
        isSearchable: true,
        inputFormOptions: "type: \"image\""
      })
    ];
  }
}

class AbsoluteBarModelDefinition extends ModelDefinitionAbstract {
  static getRdfType() {
    return "http://foo.org/Bar";
  }
}

class AbsoluteHashedBarModelDefinition extends ModelDefinitionAbstract {
  static getRdfType() {
    return "http://foo.org#Bar";
  }
}

class TweakedAbsoluteBarModelDefinition extends ModelDefinitionAbstract {
  static getRdfType() {
    return "http://foo.org/Bar";
  }

  static getRdfInstanceBaseUri({nodesNamespaceURI}) {
    return `${nodesNamespaceURI}bar`;
  }

  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'fooLink2',
        pathInIndex: 'fooLink2',
        rdfObjectProperty: "mnx:fooLink2",
        relatedModelDefinition: AbsoluteBarModelDefinition,
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'fooLabel2',
        pathInIndex: 'fooLabel2',
        rdfDataProperty: "mnx:fooLabel2"
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'fooLiteral2',
        rdfDataProperty: 'mnx:fooLiteral2'
      })
    ];
  }
}

class TweakedFormattedAbsoluteBarModelDefinition extends ModelDefinitionAbstract {
  static getRdfType() {
    return "http://foo.org/Bar";
  }

  static getNodeTypeFormatter() {
    return (type) => `TWEAKED_${type}`
  }
}

describe('ModelDefinitionAbstract', () => {

  it('should return a Model as model class by default', () => {
    expect(ModelDefinitionAbstract.getModelClass()).toEqual(Model);
  });

  it('throws an exception if instantied as abstract', () => {
    expect(() => {
      let modelDefinition = ModelDefinitionAbstract.getNodeType();
    }).toThrow("You must implement ModelDefinitionAbstract::static getNodeType()");
  });

  it('tests RDF base instance', () => {
    expect(PrefixedBarModelDefinition.getRdfInstanceBaseUri({
      nodesNamespaceURI: "http://instances.foo.org/",
    })).toBe("http://instances.foo.org/bar");

    expect(PrefixedBarModelDefinition.getRdfInstanceBaseUri({
      nodesNamespaceURI: "http://instances.foo.org/",
      nodeTypeFormatter: (type) => type.toUpperCase()
    })).toBe("http://instances.foo.org/BAR");

    expect(TweakedAbsoluteBarModelDefinition.getRdfInstanceBaseUri({
      nodesNamespaceURI: "http://instances.foo.org/",
    })).toBe("http://instances.foo.org/bar");

    expect(TweakedAbsoluteBarModelDefinition.getRdfInstanceBaseUri({
      nodesNamespaceURI: "http://instances.foo.org/",
      nodeTypeFormatter: (type) => type.toUpperCase() // <-- done nothing cause getRdfInstanceBaseUri is overrided
    })).toBe("http://instances.foo.org/bar");

    expect(TweakedFormattedAbsoluteBarModelDefinition.getRdfInstanceBaseUri({
      nodesNamespaceURI: "http://instances.foo.org/",
    })).toBe("http://foo.org/TWEAKED_Bar");

    expect(TweakedFormattedAbsoluteBarModelDefinition.getRdfInstanceBaseUri({
      nodesNamespaceURI: "http://instances.foo.org/",
      nodeTypeFormatter: (type) => type.toUpperCase() // <-- Must override
    })).toBe("http://foo.org/BAR");

    // Base URI must not be converted.
    expect(AbsoluteBarModelDefinition.getRdfInstanceBaseUri({
      nodesNamespaceURI: "http://instances.foo.org/",
    })).toBe("http://foo.org/bar");

    expect(AbsoluteBarModelDefinition.getRdfInstanceBaseUri({
      nodesNamespaceURI: "http://instances.foo.org/",
      nodeTypeFormatter: (type) => type.toUpperCase()
    })).toBe("http://foo.org/BAR");

    // Base URI must not be converted except `#` becoming `/`
    expect(AbsoluteHashedBarModelDefinition.getRdfInstanceBaseUri({
      nodesNamespaceURI: "http://instances.foo.org/",
    })).toBe("http://foo.org/bar");

    expect(AbsoluteHashedBarModelDefinition.getRdfInstanceBaseUri({
      nodesNamespaceURI: "http://instances.foo.org/",
      nodeTypeFormatter: (type) => type.toUpperCase()
    })).toBe("http://foo.org/BAR");
  });

  it('tests matching uris', () => {
    expect(PrefixedBarModelDefinition.isMatchingURI({
      nodesNamespaceURI: "http://instances.foo.org/",
      uri: "http://instances.foo.org/bar/0001"
    })).toBe(true);

    expect(TweakedAbsoluteBarModelDefinition.isMatchingURI({
      nodesNamespaceURI: "http://instances.foo.org/",
      uri: "http://instances.foo.org/bar/0001"
    })).toBe(true);

    // Must returns false because node base URI can't be predicted without
    // overriding AbsoluteBarModelDefinition::getRdfInstanceBaseUri()
    expect(AbsoluteBarModelDefinition.isMatchingURI({
      nodesNamespaceURI: "http://instances.foo.org/",
      uri: "http://instances.foo.org/bar/0001"
    })).toBe(false);

    // Must returns false because node base URI can't be predicted without
    // overriding AbsoluteBarModelDefinition::getRdfInstanceBaseUri()
    expect(AbsoluteHashedBarModelDefinition.isMatchingURI({
      nodesNamespaceURI: "http://instances.foo.org/",
      uri: "http://instances.foo.org/bar/0001"
    })).toBe(false);

  });

  it('tests generating uris', () => {
    expect(PrefixedBarModelDefinition.generateURI({
      nodesNamespaceURI: "http://instances.foo.org/",
    })).toBe(`http://instances.foo.org/bar/${mockedGeneratedID}`);

    expect(PrefixedBarModelDefinition.generateURI({
      nodesNamespaceURI: "http://instances.foo.org/",
      idPrefix: "prefix_"
    })).toBe(`http://instances.foo.org/bar/prefix_${mockedGeneratedID}`);

    expect(AbsoluteBarModelDefinition.generateURI({
      nodesNamespaceURI: "http://instances.foo.org/",
    })).toBe(`http://foo.org/bar/${mockedGeneratedID}`);

    expect(TweakedAbsoluteBarModelDefinition.generateURI({
      nodesNamespaceURI: "http://instances.foo.org/",
    })).toBe(`http://instances.foo.org/bar/${mockedGeneratedID}`);

    // Tests that `#` becoming `/`
    expect(AbsoluteHashedBarModelDefinition.generateURI({
      nodesNamespaceURI: "http://instances.foo.org/",
    })).toBe(`http://foo.org/bar/${mockedGeneratedID}`);

    // Tests that manual id
    expect(PrefixedBarModelDefinition.generateURI({
      nodesNamespaceURI: "http://instances.foo.org/",
      idValue: "MY_ID"
    })).toBe(`http://instances.foo.org/bar/MY_ID`);
  });

  it('tests node name', () => {
    expect(PrefixedBarModelDefinition.getNodeType()).toBe("Bar");
    expect(AbsoluteBarModelDefinition.getNodeType()).toBe("Bar");
    expect(AbsoluteHashedBarModelDefinition.getNodeType()).toBe("Bar");
    expect(TweakedAbsoluteBarModelDefinition.getNodeType()).toBe("Bar");
  });

  it('tests to SPARQL variable', () => {
    expect(PrefixedBarModelDefinition.toSparqlVariable()).toBe("?bar");
    expect(AbsoluteBarModelDefinition.toSparqlVariable()).toBe("?bar");
    expect(AbsoluteHashedBarModelDefinition.toSparqlVariable()).toBe("?bar");
    expect(TweakedAbsoluteBarModelDefinition.toSparqlVariable()).toBe("?bar");
  });

  it('test model definitions simple inheritance', () => {
    class HeritedDefinition extends ModelDefinitionAbstract {
      static getParentDefinitions() {
        return [PrefixedBarModelDefinition]
      }
    }

    expect(HeritedDefinition.getLinks().length).toBe(2);
    expect(HeritedDefinition.getLabels().length).toBe(1);
    expect(HeritedDefinition.getLiterals().length).toBe(1);
  });

  it('test model definitions multiple inheritance', () => {
    class MultiHeritedDefinition extends ModelDefinitionAbstract {
      static getParentDefinitions() {
        return [PrefixedBarModelDefinition, TweakedAbsoluteBarModelDefinition]
      }
    }

    expect(MultiHeritedDefinition.getLinks().length).toBe(3);
    expect(MultiHeritedDefinition.getLabels().length).toBe(2);
    expect(MultiHeritedDefinition.getLiterals().length).toBe(2);
  });

  it('test model definitions nested inheritance', () => {
    class MultiHeritedDefinition extends ModelDefinitionAbstract {
      static getParentDefinitions() {
        return [PrefixedBarModelDefinition, TweakedAbsoluteBarModelDefinition]
      }
    }

    class NestedHeritedDefinition extends ModelDefinitionAbstract {
      static getParentDefinitions() {
        return [MultiHeritedDefinition]
      }

      static getLinks() {
        return [
          ...super.getLinks(),
          new LinkDefinition({
            linkName: 'fooLink3',
            pathInIndex: 'fooLink3',
            rdfObjectProperty: "mnx:fooLink3",
            relatedModelDefinition: AbsoluteBarModelDefinition,
          })
        ]
      }

      /**
       * @inheritDoc
       */
      static getLabels() {
        return [
          ...super.getLabels(),
          new LabelDefinition({
            labelName: 'fooLabel3',
            pathInIndex: 'fooLabel3',
            rdfDataProperty: "mnx:fooLabel1"
          })
        ];
      }

      /**
       * @inheritDoc
       */
      static getLiterals() {
        return [
          ...super.getLiterals(),
          new LiteralDefinition({
            literalName: 'fooLiteral1',
            rdfDataProperty: 'mnx:fooLiteral1'
          })
        ];
      }
    }

    expect(NestedHeritedDefinition.getLinks().length).toBe(4);
    expect(NestedHeritedDefinition.getLabels().length).toBe(3);
    expect(NestedHeritedDefinition.getLiterals().length).toBe(3);
  });

  it('should return searchable labels', () => {
    expect(PrefixedBarModelDefinition.getSearchableLabels().length).toBe(1);
  });

  it('should return searchable literals', () => {
    expect(PrefixedBarModelDefinition.getSearchableLiterals().length).toBe(1);
  });


  it('should return generated graphQL input properties', () => {
    expect(PrefixedBarModelDefinition.generateGraphQLInputProperties()).toEqual(`
  """ 
  The target node id. This is usefull to create link with exsting objects during creation/update mutations.
  @see [Deep object input parsing documentation](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#deep-object-input-parsing)
  """
  id: ID

  """ Foo literal 1 """
  fooLiteral1: String

  """ Foo label 1 """
  fooLabel1: String

  """ Related bar """
  fooLinkInput: BarInput

  """ Related bars """
  barLinkInput: [BarInput]

  """ Related bars to delete """
  barLinkInputToDelete: [ID!]
`);
  });

  it('should return generated graphQL input properties for not instantiable type', () => {
    expect(NotInstantiableModelDefinition.generateGraphQLInputProperties()).toEqual(`
  """ 
  Inherited instantiable GraphQL Typename. This is compulsory for input that refers to interfaces.
  @see [Deep object input parsing documentation](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#deep-object-input-parsing)
  """
  inheritedTypename: String

  """ 
  The target node id. This is usefull to create link with exsting objects during creation/update mutations.
  @see [Deep object input parsing documentation](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#deep-object-input-parsing)
  """
  id: ID

  """ See also """
  seeAlso: String

  """ Comment """
  comment: String

  """ Not instantiable type literal 1 """
  notInstantiableTypeLiteral1: String

  """ Related external links """
  externalLinkInputs: [ExternalLinkInput]

  """ Related external links to delete """
  externalLinkInputsToDelete: [ID!]

  """ Related access policy """
  accessPolicyInput: AccessPolicyInput

  """ Related access targets """
  readWriteAccessTargetInputs: [AccessTargetInput]

  """ Related access targets to delete """
  readWriteAccessTargetInputsToDelete: [ID!]

  """ Related access targets """
  readOnlyAccessTargetInputs: [AccessTargetInput]

  """ Related access targets to delete """
  readOnlyAccessTargetInputsToDelete: [ID!]
`);

    expect(InheritedModelDefinition.generateGraphQLInputProperties()).toEqual(`
  """ 
  The target node id. This is usefull to create link with exsting objects during creation/update mutations.
  @see [Deep object input parsing documentation](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#deep-object-input-parsing)
  """
  id: ID

  """ See also """
  seeAlso: String

  """ Comment """
  comment: String

  """ Not instantiable type literal 1 """
  notInstantiableTypeLiteral1: String

  """ Inherited type literal 1 """
  inheritedTypeLiteral1: String

  """ Related external links """
  externalLinkInputs: [ExternalLinkInput]

  """ Related external links to delete """
  externalLinkInputsToDelete: [ID!]

  """ Related access policy """
  accessPolicyInput: AccessPolicyInput

  """ Related access targets """
  readWriteAccessTargetInputs: [AccessTargetInput]

  """ Related access targets to delete """
  readWriteAccessTargetInputsToDelete: [ID!]

  """ Related access targets """
  readOnlyAccessTargetInputs: [AccessTargetInput]

  """ Related access targets to delete """
  readOnlyAccessTargetInputsToDelete: [ID!]
`);

  });
});




