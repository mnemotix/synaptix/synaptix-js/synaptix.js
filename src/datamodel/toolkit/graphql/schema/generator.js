/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  mergeResolvers,
  generateTypeResolvers,
  getLocalizedLabelResolver,
  getLinkedObjectResolver,
  getLinkedObjectsResolver,
  getLinkedObjectsCountResolver,
  generateInterfaceResolvers,
  logWarning,
  getTypeResolver,
  getTypesResolver,
} from "../helpers/resolverHelpers";
import {
  generateConnectionResolverFor,
  generateConnectionForType,
  connectionArgs,
  filteringArgs,
} from "./types/generators";
import uniqBy from "lodash/uniqBy";
import uniq from "lodash/uniq";
import EntityDefinition from "../../../ontologies/mnx-common/EntityDefinition";
import { GRAPHQL_RDFTYPE_MAPPING } from "../../definitions/helpers";
import upperFirst from "lodash/upperFirst";
import snakeCase from "lodash/snakeCase";

/**
 * Entry point for generating a GraphQL type schema from a model definition and its graphql definition
 *
 * @param {typeof ModelDefinitionAbstract} modelDefinition Model definition corresponding to schema to generate
 * @param {ModelDefinitionsRegister} modelDefinitionsRegister
 * @param {Object} options Generator options
 * Options can have following properties :
 * - literalTypeMapper : a custom mapper for converting a literal type to graphQL type, which will be merged to default generator type mapper
 *
 * @returns {Objects} An object with following keys :
 * - typeDef: GraphQL schema type as a string
 * - resolvers: GraphQL resolvers corresponding to schema
 * - rules : GraphQL shield rules for this schema
 */
export const generateGraphQLSchema = ({
  modelDefinition,
  modelDefinitionsRegister,
  options,
}) => {
  let graphQLDefinition = modelDefinition.getGraphQLDefinition();

  let modelProperties = [].concat(
    ...modelDefinition.getLabels().map((label) => label.getLabelName()),
    ...modelDefinition.getLiterals().map((label) => label.getLiteralName()),
    ...modelDefinition.getLinks().map((label) => label.getGraphQLPropertyName())
  );

  // Check each overriden property in hierarchy have a name corresponding to existing property
  let overridenProperties = gatherOverridenProperties(modelDefinition);
  let overridenUnexistingProperty = overridenProperties.find(
    (prop) => !modelProperties.includes(prop.getName())
  );
  if (overridenUnexistingProperty) {
    throw new Error(
      `${
        graphQLDefinition.name
      } declares an overriden property ${overridenUnexistingProperty.getName()} which does not exist in type ${modelDefinition.getGraphQLType()}\nDeclare it in getExtraProperties hook !`
    );
  }

  // Check each extra property in hierarchy does not override an existing property
  let extraProperties = gatherExtraProperties(modelDefinition);
  let extraExistingProperty = extraProperties.find((propName) =>
    modelProperties.includes(propName)
  );
  if (extraExistingProperty) {
    throw new Error(
      `${
        graphQLDefinition.constructor.name
      } declares extra property "${extraExistingProperty} which is already existing in type ${modelDefinition.getGraphQLType()}\nDeclare it in getOverridenProperties hook !`
    );
  }

  let typeDef = generateTypeDef({
    modelDefinition,
    modelDefinitionsRegister,
    options,
  });
  let resolvers = generateResolvers(modelDefinition);
  let rules = generateResolverRules(modelDefinition);
  return { typeDef, resolvers, rules };
};

/**
 * Generate GraphQL schema type definition
 * @param {typeof ModelDefinitionAbstract} modelDefinition Model definition corresponding to schema to generate
 * @param {ModelDefinitionsRegister} modelDefinitionsRegister
 * @param {Object} options Generator options
 */
const generateTypeDef = ({
  modelDefinition,
  modelDefinitionsRegister,
  options,
}) => {
  let graphQLDefinition = modelDefinition.getGraphQLDefinition();

  let graphQLPieces = [generateGraphQLType(modelDefinition, options)];

  /**
   * Generate typedef only if :
   *  - Model definition is not substituded by another one.
   *  - GraphQL definition explicitly mention to not generate it.
   */
  if (!modelDefinition.isSubstituted() && graphQLDefinition.isTypeGenerated()) {
    let graphQLConnection = generateGraphQLConnection(modelDefinition);
    let graphQLInput = generateGraphQLInput({
      modelDefinition,
      modelDefinitionsRegister,
      options,
    });

    let queries = [
      graphQLDefinition.getTypeQuery(),
      graphQLDefinition.getTypeConnectionQuery(),
      ...graphQLDefinition.getExtraQueries(),
    ];
    let mutations = [
      graphQLDefinition.getCreateMutation(),
      graphQLDefinition.getUpdateMutation(),
      graphQLDefinition.getRemoveMutation(),
      ...graphQLDefinition.getExtraMutations(),
    ];

    graphQLPieces = graphQLPieces.concat(
      graphQLConnection,
      graphQLInput,
      queries
        .filter((query) => query != null)
        .map((query) => query.generateType(modelDefinition)),
      mutations
        .filter((mutation) => mutation != null)
        .map((mutation) => mutation.generateType(modelDefinition))
    );
  }

  graphQLPieces = graphQLPieces.concat([
    graphQLDefinition.getExtraGraphQLCode(),
  ]);

  return graphQLPieces.join("\n");
};

/**
 * Generate GraphQL resolvers
 *
 */
const generateResolvers = (modelDefinition) => {
  let graphQLDefinition = modelDefinition.getGraphQLDefinition();

  let resolvers = [generateTypeResolver(modelDefinition)];

  if (graphQLDefinition.isTypeGenerated()) {
    let queries = [
      graphQLDefinition.getTypeQuery(),
      graphQLDefinition.getTypeConnectionQuery(),
      ...graphQLDefinition.getExtraQueries(),
    ];
    let mutations = [
      graphQLDefinition.getCreateMutation(),
      graphQLDefinition.getUpdateMutation(),
      graphQLDefinition.getRemoveMutation(),
      ...graphQLDefinition.getExtraMutations(),
    ];

    resolvers = resolvers.concat(
      queries
        .filter((query) => query != null)
        .map((query) => query.generateResolver(modelDefinition)),
      mutations
        .filter((mutation) => mutation != null)
        .map((mutation) => mutation.generateResolver(modelDefinition))
    );
  }

  if (!!graphQLDefinition.getExtraResolvers()) {
    resolvers.push(graphQLDefinition.getExtraResolvers());
  }

  return mergeResolvers(...resolvers);
};

/**
 * Generate shield rules for this model
 * Check coherency of rules
 */
const generateResolverRules = (modelDefinition) => {
  const nodeType = modelDefinition.getGraphQLType();
  const graphQLDefinition = modelDefinition.getGraphQLDefinition();
  let definedRules = graphQLDefinition.getRules(nodeType);
  if (!definedRules || Object.keys(definedRules).length == 0) {
    return {};
  }

  let forbiddenRootKey = Object.keys(definedRules).find(
    (key) => !["Query", "Mutation", nodeType].includes(key)
  );
  if (forbiddenRootKey) {
    throw new Error(
      `${nodeType} defines rule in ${graphQLDefinition.name} with key ${forbiddenRootKey}\n Please use Query, Mutation or/and ${nodeType} here`
    );
  }

  let fieldsNames = [
    graphQLDefinition.getTypeQuery(),
    graphQLDefinition.getTypeConnectionQuery(),
    ...graphQLDefinition.getExtraQueries(),
    graphQLDefinition.getCreateMutation(),
    graphQLDefinition.getUpdateMutation(),
    graphQLDefinition.getRemoveMutation(),
    ...graphQLDefinition.getExtraMutations(),
  ]
    .filter((field) => !!field)
    .map((field) => field.generateFieldName(modelDefinition));

  let fieldsRulesNames = [];
  if (!!definedRules.Query) {
    fieldsRulesNames = fieldsRulesNames.concat(Object.keys(definedRules.Query));
  }
  if (!!definedRules.Mutation) {
    fieldsRulesNames = fieldsRulesNames.concat(
      Object.keys(definedRules.Mutation)
    );
  }

  let incorrectRuleField = fieldsRulesNames.find((ruleName) => {
    // batchUpdate*** mutations are dynamics and so not foundable in fieldsRulesNames
    return !ruleName.match(/^batchUpdate/) && !fieldsNames.includes(ruleName);
  });
  if (incorrectRuleField) {
    throw new Error(
      `The rule (Query or Mutation).${incorrectRuleField} defined in ${graphQLDefinition.name} does not match query/mutation fields for type ${nodeType}`
    );
  }

  if (!!definedRules[nodeType]) {
    let propertiesNames = [
      ...modelDefinition
        .getProperties()
        .map((property) => property.getPropertyName()),
      ...modelDefinition
        .getLinks()
        .map((property) => property.getGraphQLPropertyName()),
      ...graphQLDefinition
        .getExtraProperties()
        .map((property) => property.getName()),
    ];
    let incorrectRuleProperty = Object.keys(definedRules[nodeType]).find(
      (ruleName) => !propertiesNames.includes(ruleName)
    );
    if (incorrectRuleProperty) {
      throw new Error(
        `The rule ${nodeType}.${incorrectRuleProperty} defined in ${graphQLDefinition.name} does not match query/mutation fields for type ${nodeType}`
      );
    }
  }
  return definedRules;
};

/**
 * Generate GraphQL type with properties
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {object} options
 */
const generateGraphQLType = (modelDefinition, options) => {
  const graphQLDefinition = modelDefinition.getGraphQLDefinition();
  const nodeType = modelDefinition.getGraphQLType();
  const extendsAnotherType = !graphQLDefinition.isTypeGenerated();

  const overridenProperties = gatherOverridenProperties(modelDefinition);
  const overridenPropertiesNames = overridenProperties.map((prop) =>
    prop.getName()
  );

  let literals = modelDefinition
    .getLiterals()
    .filter(
      (literal) =>
        !(extendsAnotherType && literal.isHerited()) &&
        !overridenPropertiesNames.includes(literal.getLiteralName())
    );
  let labels = modelDefinition
    .getLabels()
    .filter(
      (label) =>
        !(extendsAnotherType && label.isHerited()) &&
        !overridenPropertiesNames.includes(label.getLabelName())
    );
  let links = modelDefinition
    .getLinks()
    .filter(
      (link) =>
        !(extendsAnotherType && link.isHerited()) &&
        !overridenPropertiesNames.includes(link.getGraphQLPropertyName())
    );

  let typeProperties = [];

  typeProperties.push(`
""" Identifier """
id: ID!

""" URI """
uri: String
`);

  typeProperties = typeProperties.concat(
    generateLiteralsProperties(literals, options),
    generateLabelsProperties(labels),
    generateLinksProperties(links)
  );
  typeProperties = typeProperties.concat(
    overridenProperties.map((prop) => prop.getTypeRepresentation()),
    gatherExtraProperties(modelDefinition).map((prop) =>
      prop.getTypeRepresentation()
    )
  );

  // If type extension but no properties, we don't generate empty schema extension (to avoid graphQL to throw an error)
  if (extendsAnotherType && typeProperties.length === 0) {
    logWarning(
      `${modelDefinition} extends type ${nodeType}, but there are no properties defined`
    );
    return "";
  }

  let typeDeclarations = [];

  /**
   * If model definition is substituted by another one. Don't create GraphQL type at this point to avoid duplication error.
   * The substitute model will create it further.
   */
  if (!modelDefinition.isSubstituted()) {
    if (extendsAnotherType) {
      typeDeclarations.push(`extend type ${nodeType}`);
    } else if (!modelDefinition.isInstantiable()) {
      typeDeclarations.push(`interface ${nodeType}Interface`);
    } else {
      const parentHierarchy = modelDefinition.gatherModelDefinitionHierarchy();
      let parentInterfaces = parentHierarchy.reduce((acc, parentDefinition) => {
        if (
          !parentDefinition.isInstantiable() ||
          parentDefinition.isExtensible()
        ) {
          const interfaceName = `${parentDefinition.getGraphQLType()}Interface`;
          if (!acc.includes(interfaceName)) {
            acc.push(interfaceName);
          }
        }
        return acc;
      }, []);

      /**
       * If model definition is defined as extensible, add a GraphQL Interface.
       */
      if (modelDefinition.isExtensible()) {
        typeDeclarations.push(`interface ${nodeType}Interface`);
        parentInterfaces.push(`${nodeType}Interface`);
      }

      if (parentInterfaces?.length > 0) {
        typeDeclarations.push(
          `type ${nodeType} implements ${uniq(parentInterfaces).join(" & ")}`
        );
      } else {
        typeDeclarations.push(`type ${nodeType}`);
      }
    }
  }

  const generatedType = uniq(typeDeclarations)
    .map(
      (typeDeclaration) => `
${typeDeclaration} {
${typeProperties.join("\n")}
}`
    )
    .join("\n");

  return generatedType;
};

/**
 * Generate GraphQL input
 * @param {typeof ModelDefinitionAbstract} modelDefinition Model definition corresponding to schema to generate
 * @param {ModelDefinitionsRegister} modelDefinitionsRegister
 * @param {boolean} [excludeFormDirectives]
  @param {Object} options Generator options
 */
const generateGraphQLInput = ({
  modelDefinition,
  modelDefinitionsRegister,
  excludeFormDirectives,
  options,
} = {}) => {
  if (!modelDefinition.isSubstituted()) {
    let properties = gatherInputProperties({
      modelDefinition,
      excludeFormDirectives,
      options,
    });

    // If a model definition is extensible, we must add all extended type inputs in this one to fake a kind of union input.
    // @see https://github.com/graphql/graphql-spec/blob/master/rfcs/InputUnion.md

    // This is a temporary workaround waited this RFC be in official specs.
    if (
      modelDefinition !== EntityDefinition &&
      modelDefinition.isExtensible()
    ) {
      const inheritedModelDefinitions = modelDefinitionsRegister.getInheritedModelDefinitionsFor(
        modelDefinition
      );

      for (const inheritedModelDefinition of inheritedModelDefinitions) {
        properties = Object.assign(
          properties,
          gatherInputProperties({
            modelDefinition: inheritedModelDefinition,
            excludeFormDirectives,
            excludeInheritedInputProperties: !inheritedModelDefinition.substituteModelDefinition(), // If a model definition is substituted, it is removed from register. So it's properties
            appendIntoDescriptions: ` (from inherited type ${inheritedModelDefinition.getGraphQLType()} to workaround [UnionType](https://github.com/graphql/graphql-spec/blob/master/rfcs/InputUnion.md) pattern)`,
          })
        );
      }
    }

    return `
input ${modelDefinition.getGraphQLType()}Input {
${Object.entries(properties).map(
  ([prop, { description, type }]) => `
""" ${description} """
${prop}: ${type}
`
)}

${gatherExtraProperties(modelDefinition)
  .map((prop) => prop.getInputTypeRepresentation())
  .join("\n")}
}`;
  }
};

/**
 * Generate GraphQL connection for type
 */
const generateGraphQLConnection = (modelDefinition) => {
  const nodeType = modelDefinition.getGraphQLType();
  const graphQLType = modelDefinition.isInstantiable()
    ? nodeType
    : `${nodeType}Interface`;
  return `
${generateConnectionForType(graphQLType)}
`;
};

/**
 * Generate type / interface resolver, as well as connection resolver
 *
 */
const generateTypeResolver = (modelDefinition) => {
  const extraGraphQLProperties = gatherExtraProperties(modelDefinition);
  const overridenGraphQLProperties = gatherOverridenProperties(modelDefinition);
  let overridenPropertiesNames = overridenGraphQLProperties.map((prop) =>
    prop.getName()
  );

  const literalsResolvers = modelDefinition
    .getLiterals()
    .filter(
      (literal) => !overridenPropertiesNames.includes(literal.getLiteralName())
    )
    // .filter(literal => !literal.isHerited())
    .map((literal) => ({
      [literal.getLiteralName()]: (object) => object[literal.getLiteralName()] || literal.getDefaultValue(),
    }));

  const labelsResolvers = modelDefinition
    .getLabels()
    .filter((label) => !overridenPropertiesNames.includes(label.getLabelName()))
    // .filter(label => !label.isHerited())
    .map((label) => ({
      [label.getLabelName()]: getLocalizedLabelResolver.bind(this, label),
      [label.getLabelName() + "Translations"]: (
        object,
        args,
        synaptixSession
      ) => {
        const contextLang = synaptixSession.getLang();
        const translations = object[label.getLabelName()] || [];
        translations.sort(({ lang: langA }, { lang: langB }) => {
          if (langA === contextLang) return -1;
          if (langB === contextLang) return 1;
          if (langA < langB) return -1;
          if (langA > langB) return 1;
          return 0;
        });
        return translations;
      },
    }));

  // WARNING: check with Mathieu about MnxProject package...
  let links = modelDefinition
    .getLinks()
    .filter(
      (link) =>
        !overridenPropertiesNames.includes(link.getGraphQLPropertyName())
    );
  const linksResolvers = links
    // .filter(link => !link.isHerited())  # See PersonDefinition.getLink("hasAffiliation"). Link if overidden from AgentInterface and we want to override the resolver too.
    .reduce((acc, link) => {
      if (link.isPlural()) {
        acc[link.getGraphQLPropertyName()] = getLinkedObjectsResolver.bind(
          this,
          link
        );
        acc[
          `${link.getGraphQLPropertyName()}Count`
        ] = getLinkedObjectsCountResolver.bind(this, link);
      } else {
        acc[link.getGraphQLPropertyName()] = getLinkedObjectResolver.bind(
          this,
          link
        );
      }

      return acc;
    }, {});

  // Add resolvers on overriden properties if defined
  const overridenPropertiesResolvers = overridenGraphQLProperties
    .filter((prop) => !!prop.getTypeResolver())
    .map((prop) => ({
      [prop.getName()]: prop.getTypeResolver(),
      ...(prop.getCountResolver()
        ? { [`${prop.getName()}Count`]: prop.getCountResolver() }
        : {}),
    }));

  // Add resolvers on extra properties if defined
  const extraPropertiesResolvers = extraGraphQLProperties
    .filter((prop) => !!prop.getTypeResolver())
    .map((prop) => ({
      [prop.getName()]: prop.getTypeResolver(),
    }));

  const resolvers = mergeResolvers(
    ...literalsResolvers,
    ...labelsResolvers,
    ...overridenPropertiesResolvers,
    ...extraPropertiesResolvers,
    linksResolvers
  );
  const nodeType = modelDefinition.getGraphQLType();
  let graphQLType, graphQLTypeResolver;
  let graphQLConnectionResolvers = generateConnectionResolverFor(nodeType);

  if (!modelDefinition.isInstantiable()) {
    graphQLType = `${nodeType}Interface`;
    graphQLTypeResolver = generateInterfaceResolvers(nodeType);
    graphQLConnectionResolvers = generateConnectionResolverFor(graphQLType);
  } else if (modelDefinition.isExtensible()) {
    graphQLType = `${nodeType}Interface`;
    graphQLTypeResolver = generateInterfaceResolvers(nodeType);
  } else {
    graphQLType = nodeType;
    graphQLTypeResolver = generateTypeResolvers(nodeType);
  }

  return {
    [graphQLType]: mergeResolvers(graphQLTypeResolver, resolvers),
    ...graphQLConnectionResolvers,
  };
};

/**
 * Generate literals GraphQL properties, without overriden ones
 * @param {LiteralDefinition[]} literals
 * @param {object} options
 * @return {string}
 */
const generateLiteralsProperties = (literals, options) => {
  let graphqlLiterals = [];

  // Build literal type converter
  const literalTypeConverter = {
    ...GRAPHQL_RDFTYPE_MAPPING,
    ...(options && options.literalTypeMapper ? options.literalTypeMapper : {}),
  };

  for (let literal of literals) {
    // Check that literal rdfDatatype is handled by GraphQL generator
    if (!literalTypeConverter[literal.getRdfDataType()]) {
      throw new Error(
        `Literal type ${literal.getRdfDataType()} not referenced in converter type`
      );
    }

    graphqlLiterals.push({
      name: literal.getLiteralName(),
      datatype: literalTypeConverter[literal.getRdfDataType()],
      description: literal.getDescription(),
      isPlural: literal.isPlural() || false,
    });
  }

  return `
${graphqlLiterals
  .map(
    (literal) =>
      `""" ${literal.description || upperFirstCase(literal.name)}"""
${literal.name}: ${
        literal.isPlural ? `[${literal.datatype}]` : literal.datatype
      }
`
  )
  .join("\n")}`;
};

/**
 * Generate labels GraphQL properties, without overriden ones
 * @param {LabelDefinition[]} labels
 */
const generateLabelsProperties = (labels) => {
  return `
${labels
  .map((label) => {
    const description =
      label.getDescription() || upperFirstCase(label.getLabelName());
    return `""" ${description} """
${label.getLabelName()}(langFlag: Boolean): ${
      label.isPlural() ? "[String]" : "String"
    }
""" ${description} (multi-language version)"""
${label.getLabelName()}Translations: [Translation]`;
  })
  .join("\n")}`;
};

/**
 * Generate links GraphQL properties, without overriden ones
 * @param {LinkDefinition[]} links
 */
const generateLinksProperties = (links) => {
  let graphqlLinks = [];

  for (let link of links) {
    let targetType;

    // WARNING : trick for compatibility with Synaptix core model... to fix !
    if (link.getLinkName() === "hasAction") {
      targetType = "ActionInterface";
    } else {
      targetType = link.getRelatedModelDefinition().getGraphQLType();
      targetType =
        targetType +
        (link.getRelatedModelDefinition().isInstantiable() ? "" : "Interface");
    }
    graphqlLinks.push({
      name: link.getGraphQLPropertyName(),
      isPlural: link.isPlural(),
      targetType,
      description: link.getDescription(),
    });
  }

  let graphQLGeneratedLinks = [];

  for (let link of graphqlLinks) {
    graphQLGeneratedLinks.push(`""" ${
      link.description || upperFirstCase(link.name)
    }"""
${
  link.isPlural
    ? `${link.name}(${connectionArgs}, ${filteringArgs}): ${link.targetType}Connection`
    : `${link.name}: ${link.targetType}`
}
`);
    if (link.isPlural) {
      graphQLGeneratedLinks.push(`""" Count for ${link.name} """
 ${link.name}Count(${filteringArgs}): Int
            `);
    }
  }
  return graphQLGeneratedLinks.join("\n");
};

/**
 * Return concatenation of extra graphQL properties, for this model definition and its parents
 * @param {ModelDefinition} modelDefinition
 * @returns {GraphQLProperty[]}
 */
const gatherExtraProperties = (modelDefinition) => {
  const parentHierarchy = modelDefinition.gatherModelDefinitionHierarchy();
  let extraGraphQLProperties = modelDefinition
    .getGraphQLDefinition()
    .getExtraProperties();
  extraGraphQLProperties = extraGraphQLProperties.concat(
    ...parentHierarchy.map((modelDefinition) =>
      !!modelDefinition.getGraphQLDefinition()
        ? modelDefinition.getGraphQLDefinition().getExtraProperties()
        : []
    )
  );
  extraGraphQLProperties = uniqBy(extraGraphQLProperties, (property) =>
    property.getName()
  );

  return extraGraphQLProperties;
};

/**
 * Return concatenation of overriden graphQL properties, for this model definition and its parents
 * @param {ModelDefinition} modelDefinition
 * @returns {GraphQLProperty[]}
 */
const gatherOverridenProperties = (modelDefinition) => {
  const parentHierarchy = modelDefinition.gatherModelDefinitionHierarchy();
  let overridenGraphQLProperties = modelDefinition
    .getGraphQLDefinition()
    .getOverridenProperties();
  overridenGraphQLProperties = overridenGraphQLProperties.concat(
    ...parentHierarchy.map((modelDefinition) =>
      !!modelDefinition.getGraphQLDefinition()
        ? modelDefinition.getGraphQLDefinition().getOverridenProperties()
        : []
    )
  );
  return overridenGraphQLProperties;
};

/**
 * @param modelDefinition
 * @param excludeInheritedInputProperties
 * @param excludeFormDirectives
 * @param excludeNestedInputProperties
 * @param {Object} options Generator options
 */
const gatherInputProperties = ({
  modelDefinition,
  excludeInheritedInputProperties,
  excludeFormDirectives,
  excludeNestedInputProperties,
  appendIntoDescriptions = "",
  options,
}) => {
  // Build literal type converter for input properties
  const literalTypeConverter = {
    ...GRAPHQL_RDFTYPE_MAPPING,
    ...(options && options.literalTypeMapper ? options.literalTypeMapper : {}),
  };

  let properties = {};

  if (!modelDefinition.isSubstituted()) {
    properties.id = {
      description: `The target node id. This is usefull to create link with exsting objects during creation/update mutations.
  @see [Deep object input parsing documentation](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#deep-object-input-parsing)`,
      type: `ID`,
    };

    if (!excludeInheritedInputProperties) {
      if (!modelDefinition.isInstantiable()) {
        properties.inheritedTypename = {
          description: `Inherited instantiable GraphQL Typename. This is compulsory for input that refers to interfaces.
  @see [Deep object input parsing documentation](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#deep-object-input-parsing)`,
          type: "String",
        };
      }
    }

    for (let literal of modelDefinition.getLiterals()) {
      if (
        !literal.isExcludedFromInput() &&
        !literal.getLinkPath() &&
        literalTypeConverter[literal.getRdfDataType()]
      ) {
        let type = literalTypeConverter[literal.getRdfDataType()];

        if (literal.isPlural()) {
          type = `[${type}]`;
        }

        if (!(excludeInheritedInputProperties && literal.isHerited())) {
          properties[literal.getLiteralName()] = {
            description: upperFirst(
              snakeCase(literal.getLiteralName()).replace(/_/g, " ") +
                appendIntoDescriptions
            ),
            type: type,
          };
        }
      }
    }

    for (let label of modelDefinition.getLabels()) {
      if (!(excludeInheritedInputProperties && label.isHerited())) {
        let type = "String";

        if (label.isPlural()) {
          type = `[${type}]`;
        }

        if (!label.getLinkPath()) {
          const description = upperFirst(
            snakeCase(label.getLabelName()).replace(/_/g, " ") +
              appendIntoDescriptions
          );

          properties[label.getLabelName()] = {
            description,
            type: type,
          };

          properties[label.getLabelName() + "Translations"] = {
            description,
            type: "[TranslationInput]",
          };
        }
      }
    }

    if (!excludeNestedInputProperties) {
      for (let link of modelDefinition.getLinks()) {
        if (!link.isExcludedFromInput() && !!link.getGraphQLInputName()) {
          if (!(excludeInheritedInputProperties && link.isHerited())) {
            let relatedModelDefinition = link.getRelatedModelDefinition();
            let comment =
              `Related ${snakeCase(
                relatedModelDefinition.getGraphQLType()
              ).replace(/_/g, " ")}${link.isPlural() ? "s" : ""}` +
              appendIntoDescriptions;

            let inputName = link.getGraphQLInputName();
            let inputType = link.isPlural()
              ? `[${relatedModelDefinition.getGraphQLInputType()}]`
              : relatedModelDefinition.getGraphQLInputType();

            properties[inputName] = {
              description: comment,
              type: inputType,
            };

            if (link.isPlural()) {
              properties[`${inputName}ToDelete`] = {
                description:
                  `Related ${snakeCase(
                    relatedModelDefinition.getGraphQLType()
                  ).replace(/_/g, " ")}${
                    link.isPlural() ? "s" : ""
                  } to delete ` + appendIntoDescriptions,
                type: "[ID!]",
              };
            }
          }
        }
      }
    }
  }

  return properties;
};

/**
 * Helper for upper first letter of string in parameter
 *
 * @param {*} str
 */
const upperFirstCase = (str) => `${str[0].toUpperCase()}${str.slice(1)}`;
