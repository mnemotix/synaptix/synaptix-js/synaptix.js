/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  updateObjectResolver,
  createObjectInConnectionResolver, batchUpdateObjectResolver
} from '../../../helpers/resolverHelpers';

import {
  generateCreateMutationDefinition,
  generateCreateMutationResolver,
  generateCreateMutationDefinitionForType,
  generateCreateMutationResolverForType,
  generateUpdateMutationDefinition,
  generateUpdateMutationDefinitionForType,
  generateUpdateMutationResolver,
  generateUpdateMutationResolverForType
} from "../generators";

import ModelDefinitionsRegister from "../../../../definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../../../../../network/amqp/NetworkLayerAMQP";
import {PubSub} from "graphql-subscriptions";
import SynaptixDatastoreRdfSession from "../../../../../../datastores/SynaptixDatastoreRdfSession";
import GraphQLContext from "../../../GraphQLContext";
import PersonDefinition from "../../../../../ontologies/mnx-agent/PersonDefinition";

let modelDefinitionsRegister = new ModelDefinitionsRegister([PersonDefinition]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let pubSubEngine = new PubSub();

let synaptixSession = new SynaptixDatastoreRdfSession({
  modelDefinitionsRegister,
  networkLayer,
  context: new GraphQLContext({anonymous: true, lang: "fr"}),
  pubSubEngine,
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
});

let createSpyFn = jest.spyOn(synaptixSession, "createObject").mockImplementation(() => ({}));
let updateSpyFn = jest.spyOn(synaptixSession, "updateObject").mockImplementation(() => ({}));


describe("Test graphQL mutation generators", () => {

  it('tests create mutation generator', async () => {
    expect(generateCreateMutationDefinitionForType("Person")).toBe(simpleCreateGql);
    expect(generateCreateMutationDefinition({modelDefinition: PersonDefinition})).toBe(simpleCreateGql);

    expect(generateCreateMutationDefinitionForType("Person", undefined, undefined, "My description")).toBe(withDescriptionCreateGql);
    expect(generateCreateMutationDefinition({modelDefinition: PersonDefinition, description: "My description"})).toBe(withDescriptionCreateGql);

    expect(generateCreateMutationDefinitionForType("Person", "extraProp: String")).toBe(withExtraInputCreateGql);
    expect(generateCreateMutationDefinition({modelDefinition: PersonDefinition, extraInputArgs: "extraProp: String"})).toBe(withExtraInputCreateGql);

    expect(generateCreateMutationDefinitionForType("Person", "extraProp: String", "PersonFoo")).toBe(withEdgeChangedCreateGql);
    expect(generateCreateMutationDefinition({modelDefinition: PersonDefinition, extraInputArgs:"extraProp: String", edgeType: "PersonFoo"})).toBe(withEdgeChangedCreateGql);
  });

  it('tests update mutation generator', async () => {
    expect(generateUpdateMutationDefinitionForType("Person")).toBe(simpleUpdateGql);
    expect(generateUpdateMutationDefinition({modelDefinition: PersonDefinition})).toBe(simpleUpdateGql);

    expect(generateUpdateMutationDefinitionForType("Person", "My description", "My batch description")).toBe(withDescriptionUpdateGql);
    expect(generateUpdateMutationDefinition({modelDefinition: PersonDefinition, description: "My description", batchDescription: "My batch description"})).toBe(withDescriptionUpdateGql);
  });

  it('tests create mutation generator resolvers', async () => {
    expect(JSON.stringify(generateCreateMutationResolverForType("Person"))).toEqual(JSON.stringify({
      Mutation: {
        createPerson: createObjectInConnectionResolver.bind(this)
      }
    }));

    let resolver = jest.fn();
    expect(generateCreateMutationResolverForType("Person", resolver)).toEqual({
      Mutation: {
        createPerson: resolver
      }
    });

    expect(JSON.stringify(generateCreateMutationResolver({modelDefinition: PersonDefinition}))).toEqual(JSON.stringify({
      Mutation: {
        createPerson: createObjectInConnectionResolver.bind(this)
      }
    }));

    expect(generateCreateMutationResolver({modelDefinition: PersonDefinition, resolver})).toEqual({
      Mutation: {
        createPerson: resolver
      }
    });
  });


  it('tests update mutation generator resolvers', async () => {
    expect(JSON.stringify(generateUpdateMutationResolverForType("Person"))).toEqual(JSON.stringify({
      Mutation: {
        updatePerson: updateObjectResolver.bind(this)
      }
    }));

    let resolver = jest.fn();
    expect(generateUpdateMutationResolverForType("Person", resolver, resolver)).toEqual({
      Mutation: {
        updatePerson: resolver,
        batchUpdatePerson: resolver
      }
    });

    expect(JSON.stringify(generateUpdateMutationResolver({modelDefinition: PersonDefinition}))).toEqual(JSON.stringify({
      Mutation: {
        updatePerson: updateObjectResolver.bind(this),
        batchUpdatePerson: batchUpdateObjectResolver.bind(this)
      }
    }));

    expect(generateUpdateMutationResolver({modelDefinition: PersonDefinition, resolver, batchResolver: resolver})).toEqual({
      Mutation: {
        updatePerson: resolver,
        batchUpdatePerson: resolver
      }
    });
  });
});


let simpleCreateGql = `
input CreatePersonInput {
  objectInput: PersonInput
  
}

type CreatePersonPayload {
  createdObject: Person
  createdEdge: PersonEdge
  success: Boolean
}

extend type Mutation{
  """
  Mutation to create a person.
  
  This mutation input is protected by the Synaptix.js. Following i18nkey errors can be sent :

  - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous.
  """
  createPerson(input: CreatePersonInput!): CreatePersonPayload
}
`;

let withDescriptionCreateGql = `
input CreatePersonInput {
  objectInput: PersonInput
  
}

type CreatePersonPayload {
  createdObject: Person
  createdEdge: PersonEdge
  success: Boolean
}

extend type Mutation{
  """
  My description
  """
  createPerson(input: CreatePersonInput!): CreatePersonPayload
}
`;

let withExtraInputCreateGql = `
input CreatePersonInput {
  objectInput: PersonInput
  extraProp: String
}

type CreatePersonPayload {
  createdObject: Person
  createdEdge: PersonEdge
  success: Boolean
}

extend type Mutation{
  """
  Mutation to create a person.
  
  This mutation input is protected by the Synaptix.js. Following i18nkey errors can be sent :

  - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous.
  """
  createPerson(input: CreatePersonInput!): CreatePersonPayload
}
`;

let withEdgeChangedCreateGql = `
input CreatePersonInput {
  objectInput: PersonFooInput
  extraProp: String
}

type CreatePersonPayload {
  createdObject: Person
  createdEdge: PersonFooEdge
  success: Boolean
}

extend type Mutation{
  """
  Mutation to create a person.
  
  This mutation input is protected by the Synaptix.js. Following i18nkey errors can be sent :

  - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous.
  """
  createPerson(input: CreatePersonInput!): CreatePersonPayload
}
`;

let simpleUpdateGql = `
input UpdatePersonInput {
  objectId: ID!
  objectInput: PersonInput
  
}

type UpdatePersonPayload {
  updatedObject: Person
  success: Boolean
}

input BatchUpdatePersonInput {
  objectIds: [ID!]
  filters: [String]
  objectsInput: PersonInput
  
}

type BatchUpdatePersonPayload {
  updatedObjects: [Person]
  success: Boolean
}

extend type Mutation{
  """
  Mutation to update a person.
  
  This mutation input is protected by the Synaptix.js. Following i18nkey errors can be sent :

  - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous.
  """
  updatePerson(input: UpdatePersonInput!): UpdatePersonPayload
  
  """
  Mutation to batch update a list of person.
  
  This mutation input is protected by the Synaptix.js. Following i18nkey errors can be sent :

  - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous.
  """
  batchUpdatePerson(input: BatchUpdatePersonInput!): BatchUpdatePersonPayload
}
`;

let withDescriptionUpdateGql = `
input UpdatePersonInput {
  objectId: ID!
  objectInput: PersonInput
  
}

type UpdatePersonPayload {
  updatedObject: Person
  success: Boolean
}

input BatchUpdatePersonInput {
  objectIds: [ID!]
  filters: [String]
  objectsInput: PersonInput
  
}

type BatchUpdatePersonPayload {
  updatedObjects: [Person]
  success: Boolean
}

extend type Mutation{
  """
  My description
  """
  updatePerson(input: UpdatePersonInput!): UpdatePersonPayload
  
  """
  My batch description
  """
  batchUpdatePerson(input: BatchUpdatePersonInput!): BatchUpdatePersonPayload
}
`;