/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import PersonDefinition from "./PersonDefinition";
import OrganizationDefinition from "./OrganizationDefinition";
import DurationDefinition from "../mnx-time/DurationDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import {LinkPath} from "../../toolkit/utils/linkPath";
import AccessPolicyDefinition from "../mnx-acl/AccessPolicyDefinition";
import EntityDefinition from "../mnx-common/EntityDefinition";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";
import DeletionDefinition from "../mnx-contribution/DeletionDefinition";

export default class AffiliationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [DurationDefinition];
  }


  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Affiliation";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'affiliation';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    let parentLinks = super.getLinks();
    let indexOf = parentLinks.findIndex((link) => link.getLinkName() === "hasAccessPolicy");
    if(indexOf > 0){
      parentLinks.splice(indexOf, 1);
    }

    const hasDefaultDeletionLinkDefinition = parentLinks.find(
      link => link.getLinkName() === "hasDeletionAction"
    );

    if (hasDefaultDeletionLinkDefinition) {
      parentLinks.splice(
        parentLinks.indexOf(hasDefaultDeletionLinkDefinition),
        1
      );
    }

    const hasPersonLinkDefinition = new LinkDefinition({
      linkName: 'hasPerson',
      symmetricLinkName: 'hasAffiliation',
      rdfObjectProperty: "mnx:hasPerson",
      relatedModelDefinition: PersonDefinition,
      isCascadingUpdated: true,
      graphQLPropertyName: "person",
      graphQLInputName: "personInput"
    });

    const hasOrganizationLinkDefinition = new LinkDefinition({
      linkName: 'hasOrg',
      symmetricLinkName: 'hasAffiliation',
      rdfObjectProperty: "mnx:hasOrg",
      relatedModelDefinition: OrganizationDefinition,
      isCascadingUpdated: true,
      graphQLPropertyName: "organization",
      graphQLInputName: "orgInput"
    });


    //
    // This is a reification class,
    // If one the reified object is deleted, reification must be deleted.
    //
    const hasDeletionLinkDefinition = new LinkDefinition({
      linkName: 'hasDeletionAction',
      relatedModelDefinition: DeletionDefinition,
      graphQLPropertyName: "deletionAction",
      linkPath: new LinkPath().union({
        linkPaths: [
          new LinkPath()
            .step({linkDefinition: hasDefaultDeletionLinkDefinition }),
          new LinkPath()
            .step({linkDefinition: hasPersonLinkDefinition })
            .step({linkDefinition: hasDefaultDeletionLinkDefinition }),
          new LinkPath()
            .step({linkDefinition: hasOrganizationLinkDefinition })
            .step({linkDefinition: hasDefaultDeletionLinkDefinition })
        ]
      })
    });

    return [
      ...parentLinks,
      hasPersonLinkDefinition,
      hasOrganizationLinkDefinition,
      hasDeletionLinkDefinition,
      new LinkDefinition({
        linkName: 'hasAccessPolicy',
        relatedModelDefinition: AccessPolicyDefinition,
        graphQLPropertyName: "accessPolicy",
        linkPath: () => new LinkPath()
          .step({linkDefinition: AffiliationDefinition.getLink("hasPerson")})
          .step({linkDefinition: EntityDefinition.getLink("hasAccessPolicy")})
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'role',
        rdfDataProperty: "mnx:role"
      })
    ];
  }
};