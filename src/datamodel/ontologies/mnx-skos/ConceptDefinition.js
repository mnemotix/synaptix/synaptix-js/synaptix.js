/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import LinkDefinition  from "../../toolkit/definitions/LinkDefinition";
import VocabularyDefinition from "./VocabularyDefinition";
import SchemeDefinition from "./SchemeDefinition";
import CollectionDefinition from "./CollectionDefinition";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import EntityDefinition from "../mnx-common/EntityDefinition";
import FilterDefinition from "../../toolkit/definitions/FilterDefinition";
import {GraphQLTypeDefinition} from "../../toolkit/graphql/schema";
import {LinkPath} from "../../toolkit/utils/linkPath";
import TaggingDefinition from "../mnx-contribution/TaggingDefinition";

export default class ConceptDefinition extends ModelDefinitionAbstract {
  /**
   * @return {boolean}
   */
  static isExtensible(){
    return true;
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "skos": "http://www.w3.org/2004/02/skos/core#"
    };
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'skos:Concept';
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Concept';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'concept';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    let parentLinks = super.getLinks();
    let indexOf = parentLinks.findIndex((link) => link.getLinkName() === "hasTagging");
    if(indexOf > 0){
      parentLinks.splice(indexOf, 1);
    }

    return [
      ...parentLinks,
      new LinkDefinition({
        linkName: 'hasVocabulary',
        pathInIndex: "vocabulary",
        rdfObjectProperty: "mnx:conceptOf",
        relatedModelDefinition: VocabularyDefinition,
        graphQLPropertyName: "vocabulary",
        graphQLInputName: "vocabularyInput"
      }),
      new LinkDefinition({
        linkName: 'inScheme',
        rdfObjectProperty: "skos:inScheme",
        relatedModelDefinition: SchemeDefinition,
        isPlural: true,
        graphQLPropertyName: "schemes",
        graphQLInputName: "schemeInputs"
      }),
      new LinkDefinition({
        linkName: 'topInScheme',
        pathInIndex: 'topConceptOf',
        rdfObjectProperty: "skos:topConceptOf",
        relatedModelDefinition: SchemeDefinition,
        isPlural: true,
        graphQLPropertyName: "topInSchemes",
        graphQLInputName: "topInSchemeInputs"
      }),
      new LinkDefinition({
        linkName: 'hasCollection',
        pathInIndex: "collection",
        rdfObjectProperty: "skos:member",
        relatedModelDefinition: CollectionDefinition,
        isPlural: true,
        graphQLPropertyName: "collections",
        graphQLInputName: "collectionInputs"
      }),
      new LinkDefinition({
        linkName: 'hasBroader',
        pathInIndex: "broader",
        rdfObjectProperty: "skos:broader",
        relatedModelDefinition: ConceptDefinition,
        graphQLPropertyName: "broaderConcept",
        graphQLInputName: "broaderConceptInput"
      }),
      new LinkDefinition({
        linkName: 'hasBroaderTransitive',
        pathInIndex: "broaderTransitive",
        rdfObjectProperty: "skos:broaderTransitive",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        graphQLPropertyName: "broaderTransitiveConcepts",
        graphQLInputName: "broaderTransitiveConceptInputs"
      }),
      new LinkDefinition({
        linkName: 'hasRelated',
        pathInIndex: "related",
        rdfObjectProperty: "skos:related",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        graphQLPropertyName: "relatedConcepts",
        graphQLInputName: "relatedConceptInputs"
      }),
      new LinkDefinition({
        linkName: 'hasNarrower',
        pathInIndex: "narrower",
        symmetricLinkName: "hasBroader",
        rdfReversedObjectProperty: "skos:broader",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        graphQLPropertyName: "narrowerConcepts",
        graphQLInputName: "narrowerConceptInputs"
      }),
      new LinkDefinition({
        linkName: 'hasNarrowerTransitive',
        pathInIndex: "narrowerTransitives",
        symmetricLinkName: "hasBroader",
        rdfReversedObjectProperty: "skos:broader",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        graphQLPropertyName: "narrowerTransitiveConcepts",
        graphQLInputName: "narrowerTransitiveConceptInputs"
      }),
      new LinkDefinition({
        linkName: 'hasCloseMatchConcept',
        pathInIndex: "closeMatch",
        rdfObjectProperty: "skos:closeMatch",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        graphQLPropertyName: "closeMatchConcepts",
        graphQLInputName: "closeMatchConceptInputs"
      }),
      new LinkDefinition({
        linkName: 'hasExactMatchConcept',
        pathInIndex: "exactMatch",
        rdfObjectProperty: "skos:exactMatch",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        graphQLPropertyName: "exactMatchConcepts",
        graphQLInputName: "exactMatchConceptInputs"
      }),
      new LinkDefinition({
        linkName: 'hasRelatedtMatchConcept',
        pathInIndex: "relatedMatch",
        rdfObjectProperty: "skos:relatedMatch",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        graphQLPropertyName: "relatedMatchConcepts",
        graphQLInputName: "relatedMatchConceptInputs"
      }),
      new LinkDefinition({
        linkName: 'hasNarrowerMatchConcept',
        pathInIndex: "narrowerMatch",
        rdfObjectProperty: "skos:narrowerMatch",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        graphQLPropertyName: "narrowerMatchConcepts",
        graphQLInputName: "narrowerMatchConceptInputs"
      }),
      new LinkDefinition({
        linkName: 'hasBroaderMatchConcept',
        pathInIndex: "broaderMatch",
        rdfObjectProperty: "skos:broaderMatch",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        graphQLPropertyName: "broaderMatchConcepts",
        graphQLInputName: "broaderMatchConceptInputs"
      }),
      new LinkDefinition({
        linkName: 'isTaggedConceptOf',
        symmetricLinkName: 'hasConcept',
        description: "Upcycled Concept of taggings",
        rdfReversedObjectProperty: "mnx:hasConcept",
        relatedModelDefinition: TaggingDefinition,
        isPlural: true,
        graphQLPropertyName: "taggings",
        graphQLInputName: "taggingInputs"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    const prefLabelDefinition = new LabelDefinition({
      labelName: 'prefLabel',
      rdfDataProperty: "skos:prefLabel",
      searchBoost: 10,
    });

    return [
      ...super.getLabels(),
      prefLabelDefinition,
      new LabelDefinition({
        labelName: 'altLabel',
        rdfDataProperty: "skos:altLabel",
        isSearchable: false,
      }),
      new LabelDefinition({
        labelName: 'altLabels',
        rdfDataProperty: "skos:altLabel",
        isPlural: true,
        isSearchable: false,
        searchBoost: 5,
      }),
      new LabelDefinition({
        labelName: 'hiddenLabel',
        rdfDataProperty: "skos:hiddenLabel",
        isSearchable: false
      }),
      new LabelDefinition({
        labelName: 'hiddenLabels',
        rdfDataProperty: "skos:hiddenLabel",
        isPlural: true,
        isSearchable: false,
      }),
      new LabelDefinition({
        labelName: 'scopeNote',
        rdfDataProperty: "skos:scopeNote",
        isSearchable: false
      }),
      new LabelDefinition({
        labelName: 'definition',
        rdfDataProperty: "skos:definition",
        isSearchable: false
      }),
      new LabelDefinition({
        labelName: 'changeNote',
        rdfDataProperty: "skos:changeNote",
        isSearchable: false
      }),
      new LabelDefinition({
        labelName: 'notation',
        rdfDataProperty: "skos:notation",
        isSearchable: false
      }),
      new LabelDefinition({
        labelName: 'historyNote',
        rdfDataProperty: "skos:historyNote",
        isSearchable: false
      }),
      new LabelDefinition({
        labelName: 'example',
        rdfDataProperty: "skos:example",
        isSearchable: false
      }),
      new LabelDefinition({
        labelName: 'editorialNote',
        rdfDataProperty: "skos:editorialNote",
        isSearchable: false
      }),
      new LabelDefinition({
        labelName: 'note',
        rdfDataProperty: "skos:note",
        isSearchable: false
      }),
      new LabelDefinition({
        labelName: 'schemePrefLabel',
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("inScheme")})
          .property({propertyDefinition: SchemeDefinition.getLabel("prefLabel")}),
        isSearchable: false
      }),
      new LabelDefinition({
        labelName: 'vocabularyPrefLabel',
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("hasVocabulary")})
          .property({propertyDefinition: VocabularyDefinition.getLabel("prefLabel")}),
        isSearchable: false
      }),
      new LabelDefinition({
        labelName: 'broaderTransitivePrefLabels',
        isPlural: true,
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("hasBroaderTransitive")})
          .property({propertyDefinition: prefLabelDefinition}),
        isSearchable: false
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'isDraft',
        rdfDataProperty: "mnx:isDraft"
      }),
      new LiteralDefinition({
        literalName: 'color',
        rdfDataProperty: "mnx:color"
      })
    ];
  }

  static getFilters() {
    return [
      ...super.getFilters(),
      new FilterDefinition({
        filterName: "isTopInScheme",
        indexFilter: () => ({
          "exists": {
            "field": ConceptDefinition.getLink("topInScheme").getPathInIndex()
          }
        })
      })
    ];
  }
};