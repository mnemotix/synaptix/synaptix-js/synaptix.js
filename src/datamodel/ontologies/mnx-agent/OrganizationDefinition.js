/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import AffiliationDefinition from "./AffiliationDefinition";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import AgentDefinition from "./AgentDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";

export default class OrganizationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isExtensible() {
    return true;
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [AgentDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'mnx:Organization';
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["foaf:Organization"];
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'agent';
  }

  /**
   * @inheritDoc
   */
  static getIndexFilters(){
    // While action index is shared between Creation/Update/Deletion, we must filter types
    return [{
      "term":  {
        "types": "http://ns.mnemotix.com/ontologies/2019/8/generic-model/Organization"
      }
    }]
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    let parentLinks = super.getLinks();
    let indexOfAffilation = parentLinks.findIndex(link => link.getLinkName() === "hasAffiliation");

    if (indexOfAffilation >= 0){
      // Little hack to override hasAffiliation lin
      parentLinks.splice(indexOfAffilation, 1, new LinkDefinition({
        linkName: 'hasAffiliation',
        symmetricLinkName: "hasOrg",
        relatedModelDefinition: AffiliationDefinition,
        rdfReversedObjectProperty: "mnx:hasOrg",
        isPlural: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "affiliations",
        graphQLInputName: "affiliationInputs",
        isHerited: true
      }))
    }

    return [
      ...parentLinks,
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'name',
        rdfDataProperty: "mnx:name",
        searchBoost: 3
      }),
      new LabelDefinition({
        labelName: 'shortName',
        rdfDataProperty: "mnx:shortName",
        searchBoost: 2
      }),
      new LabelDefinition({
        labelName: 'description',
        rdfDataProperty: "dc:description",
        inputFormOptions: 'type: "textarea"'
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        rdfDataProperty: "mnx:shortDescription",
        inputFormOptions: 'type: "textarea"'
      }),
      new LabelDefinition({
        labelName: 'other',
        rdfDataProperty: "mnx:other",
        inputFormOptions: 'type: "textarea"'
      })
    ];
  }
};