import {
    getObjectResolver,
    getObjectsCountResolver,
    getObjectsResolver
} from "../helpers/resolverHelpers";
import {
    connectionArgs,
    filteringArgs
} from "./types/generators";


const lowerFirstCase = (str) =>  `${str[0].toLowerCase()}${str.slice(1)}`;


/**
 * Class representing a GraphQL query
 * 
 */
export class GraphQLQuery {

    constructor({ description }={}) {
        this._description = description;
    }

    /**
     * Returns field name for this mutation
     * @param {ModelDefinitionAbstract} modelDefinition 
     * @returns String
     */
    generateFieldName(modelDefinition) {
        throw new Error(`You must implement ${this.constructor.name}::generateFieldName`);
    }

    /**
     * Returns a GraphQL schema for this query
     * @param {ModelDefinitionAbstract} modelDefinition 
     */
    generateType(modelDefinition) {
        throw new Error(`You must implement ${this.constructor.name}::generateType`);
    }

    /**
     * Returns a GraphQL resolver object for this query
     * @param {ModelDefinitionAbstract} modelDefinition 
     */
    generateResolver(modelDefinition) {
        throw new Error(`You must implement ${this.constructor.name}::generateResolver`);
    }

    /**
     * Wrapper for query extension type
     */
    _wrapQueryType(content) {
        return ` extend type Query { ${content} }`;
    }

    /**
     * Wrapper for query extension resolver
     */
    _wrapQueryResolver(content) {
        return {
            Query: content
        };
    }

}


/**
 * Class representing a type query
 * 
 */
export class GraphQLTypeQuery extends GraphQLQuery {

    /**
     * @inheritdoc
     */
    generateFieldName(modelDefinition) {
        return lowerFirstCase(modelDefinition.getGraphQLType());
    }

    /**
     * @inheritdoc
     */
    generateType(modelDefinition) {
        const nodeType = modelDefinition.getGraphQLType();
        const graphQLType = modelDefinition.isInstantiable() ? nodeType : `${nodeType}Interface`;
        let description = this._description || `Get ${nodeType} `;
        return this._wrapQueryType(`
            """ ${description} """
            ${this.generateFieldName(modelDefinition)}(id:ID!): ${graphQLType}
        `);
    }

    /**
     * @inheritdoc
     */
    generateResolver(modelDefinition) {
        return this._wrapQueryResolver({
            [this.generateFieldName(modelDefinition)]: getObjectResolver.bind(this, modelDefinition)
        });
    }

}


/**
 * Class representing a type connection query
 * 
 */
export class GraphQLTypeConnectionQuery extends GraphQLQuery {

    constructor({ extraArgs, description }={}) {
        super({ description });
        this._extraArgs = extraArgs;
    }

    /**
     * @inheritdoc
     */
    generateFieldName(modelDefinition) {
        let singularFiedName = lowerFirstCase(modelDefinition.getGraphQLType());

        if(!!singularFiedName.match(/y$/)) {
            return singularFiedName.replace(/y$/, "ies");
        }else if(!!singularFiedName.match(/us$/)){
            return singularFiedName.replace(/us$/, "i");
        } else {
            return `${singularFiedName}s`;
        }
    }

    /**
     * @inheritdoc
     */
    generateCountName(modelDefinition) {
        return `${this.generateFieldName(modelDefinition)}Count`;
    }
    
    /**
     * @inheritdoc
     */
    generateType(modelDefinition) {
        const nodeType = modelDefinition.getGraphQLType();
        const graphQLType = modelDefinition.isInstantiable() ? nodeType : `${nodeType}Interface`;
        let description = this._description || `Search for ${nodeType} `;
        let extraArgs = this._extraArgs ? `, ${this._extraArgs}` : '';
        return this._wrapQueryType(`
            """ ${description} """
            ${this.generateFieldName(modelDefinition)}(${connectionArgs}, ${filteringArgs}, ${extraArgs}): ${graphQLType}Connection
            
            """ ${description} count """
            ${this.generateCountName(modelDefinition)}(${filteringArgs}, ${extraArgs}): Int
        `);
    }

    /**
     * @inheritdoc
     */
    generateResolver(modelDefinition) {
        return this._wrapQueryResolver({
            [this.generateFieldName(modelDefinition)]: getObjectsResolver.bind(this, modelDefinition),
            [this.generateCountName(modelDefinition)]: getObjectsCountResolver.bind(this, modelDefinition)
        });
    }

}
