/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {getFragmentDefinitionsRegisterFromGqlInfo} from "../fragmentHelpers";

import ModelDefinitionsRegister from "../../../definitions/ModelDefinitionsRegister";
import {gqlQueryToResolveInfo} from "../../../../../testing/gqlQueryToResolveInfo";
import gql from "graphql-tag";
import {FragmentDefinition} from "../../../definitions/FragmentDefinition";
import BarDefinitionMock from "../../../../__tests__/mocks/definitions/BarDefinitionMock";
import BazDefinitionMock from "../../../../__tests__/mocks/definitions/BazDefinitionMock";
import FooDefinitionMock from "../../../../__tests__/mocks/definitions/FooDefinitionMock";
import NotInstantiableDefinitionMock from "../../../../__tests__/mocks/definitions/NotInstantiableDefinitionMock";

let modelDefinitionsRegister = new ModelDefinitionsRegister([NotInstantiableDefinitionMock, BarDefinitionMock, FooDefinitionMock]);

describe("Test graphQL fragments helpers", () => {

  it('should extract inline fragments from object query', async () => {
    let fragmentDefinitions = getFragmentDefinitionsRegisterFromGqlInfo({
      info: gqlQueryToResolveInfo(gql`
        query{
          notInstantiable{
            avatar
            ...on Bar{
              barLiteral1
            }
            ...on Foo{
                  fooLabel1
                }
          }
        }
      `),
      modelDefinitionsRegister,
      rootModelDefinition: NotInstantiableDefinitionMock
    });

    expect(fragmentDefinitions).toEqual({
      "notInstantiable": [
        new FragmentDefinition({
          modelDefinition: NotInstantiableDefinitionMock,
          properties: [NotInstantiableDefinitionMock.getProperty("avatar")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")]
        }),
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty("fooLabel1")]
        })
      ]
    });
  });

  it('should extract named fragments from object query', async () => {
    let fragmentDefinitions = getFragmentDefinitionsRegisterFromGqlInfo({
      info: gqlQueryToResolveInfo(gql`
        query{
          notInstantiable{
            ...F1
          }
        }
        
        fragment F1 on NotInstantiableMock {
          avatar
          ...F2
          ...F3
        }

        fragment F2 on Bar {
          barLiteral1
        }
        fragment F3 on Foo {
          fooLabel1
        }
      `),
      modelDefinitionsRegister,
      rootModelDefinition: NotInstantiableDefinitionMock
    });

    expect(fragmentDefinitions).toEqual({
      "notInstantiable": [
        new FragmentDefinition({
          modelDefinition: NotInstantiableDefinitionMock,
          properties: [NotInstantiableDefinitionMock.getProperty("avatar")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")]
        }),
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty("fooLabel1")]
        })
      ]
    });
  });

  it('should extract mixed fragments from object query', async () => {
    let fragmentDefinitions = getFragmentDefinitionsRegisterFromGqlInfo({
      info: gqlQueryToResolveInfo(gql`
        query{
          notInstantiable{
            ...F1
          }
        }

        fragment F1 on NotInstantiableMock {
          avatar
          ...on Bar {
            barLiteral1
          }
          ...on Foo {
            fooLabel1
          }
        }
      `),
      modelDefinitionsRegister,
      rootModelDefinition: NotInstantiableDefinitionMock
    });

    expect(fragmentDefinitions).toEqual({
      "notInstantiable": [
        new FragmentDefinition({
          modelDefinition: NotInstantiableDefinitionMock,
          properties: [NotInstantiableDefinitionMock.getProperty("avatar")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")]
        }),
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty("fooLabel1")]
        })
      ]
    });
  });

  it('should extract inline fragments from objects connection query', async () => {
    let fragmentDefinitions = getFragmentDefinitionsRegisterFromGqlInfo({
      info: gqlQueryToResolveInfo(gql`
        query{
          notInstantiables{
            edges{
              node{
                avatar
                ...on Bar{
                  barLiteral1
                }
                ...on Foo{
                  fooLabel1
                }
              }
            }
          }
        }
      `),
      isConnection: true,
      modelDefinitionsRegister,
      rootModelDefinition: NotInstantiableDefinitionMock
    });

    expect(fragmentDefinitions).toEqual({
      "notInstantiables": [
        new FragmentDefinition({
          modelDefinition: NotInstantiableDefinitionMock,
          properties: [NotInstantiableDefinitionMock.getProperty("avatar")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")]
        }),
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty("fooLabel1")]
        })
      ]
    });
  });

  it('should extract named fragments from objects connection query', async () => {
    let fragmentDefinitions = getFragmentDefinitionsRegisterFromGqlInfo({
      info: gqlQueryToResolveInfo(gql`
        query{
          notInstantiables{
            edges{
              node{
                ...F1
              }
            }
          }
        }
        fragment F1 on NotInstantiableMock {
          avatar
          ...F2
          ...F3
        }
        fragment F2 on Bar {
          barLiteral1
        }
        fragment F3 on Foo {
          fooLabel1
        }
      `),
      isConnection: true,
      modelDefinitionsRegister,
      rootModelDefinition: NotInstantiableDefinitionMock
    });

    expect(fragmentDefinitions).toEqual({
      "notInstantiables": [
        new FragmentDefinition({
          modelDefinition: NotInstantiableDefinitionMock,
          properties: [NotInstantiableDefinitionMock.getProperty("avatar")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")]
        }),
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty("fooLabel1")]
        })
      ]
    });
  });

  it('should extract mixed fragments from objects connection query', async () => {
    let fragmentDefinitions = getFragmentDefinitionsRegisterFromGqlInfo({
      info: gqlQueryToResolveInfo(gql`
        query{
          notInstantiables{
            edges{
              node{
                ...F1
              }
            }
          }
        }
        fragment F1 on NotInstantiableMock {
          avatar
          ...on Bar{
            barLiteral1
          }
          ...on Foo{
            fooLabel1
          }
        }
      `),
      isConnection: true,
      modelDefinitionsRegister,
      rootModelDefinition: NotInstantiableDefinitionMock
    });

    expect(fragmentDefinitions).toEqual({
      "notInstantiables": [
        new FragmentDefinition({
          modelDefinition: NotInstantiableDefinitionMock,
          properties: [NotInstantiableDefinitionMock.getProperty("avatar")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")]
        }),
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty("fooLabel1")]
        })
      ]
    });
  });

  it('should extract all fragments from object query', async () => {
    let fragmentDefinitions = getFragmentDefinitionsRegisterFromGqlInfo({
      info: gqlQueryToResolveInfo(gql`
        query{
          notInstantiable{
            avatar
            ...on Bar{
              barLiteral1
              hasBaz{edges{node{
                bazLabel1
                hasFoo {
                  ...F2
                }
              }}}
            }
            ...on Foo{
              fooLabel1
            }
          }
        }

        fragment F2 on Foo {
          fooLabel2

        }
      `),
      modelDefinitionsRegister,
      rootModelDefinition: NotInstantiableDefinitionMock
    });

    expect(fragmentDefinitions).toEqual({
      "notInstantiable": [
        new FragmentDefinition({
          modelDefinition: NotInstantiableDefinitionMock,
          properties: [NotInstantiableDefinitionMock.getProperty("avatar")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")],
          links: [BarDefinitionMock.getLink("hasBaz")]
        }),
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty("fooLabel1")]
        })
      ],
      "notInstantiable-hasBaz": [
        new FragmentDefinition({
          modelDefinition: BazDefinitionMock,
          properties: [BazDefinitionMock.getProperty('bazLabel1')],
          links: [BazDefinitionMock.getLink('hasFoo')]
        })
      ],
      "notInstantiable-hasBaz-hasFoo": [
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty('fooLabel2')]
        })
      ]
    });
  });

  it('should extract and merge properties on two fragments with a same level and property', async () => {
    let fragmentDefinitions = getFragmentDefinitionsRegisterFromGqlInfo({
      info: gqlQueryToResolveInfo(gql`
        query{
          notInstantiable{
            avatar
            ...B1
            ...on Bar {
              barLiteral1
              hasFoo {  # <-- use some fragments with different properties on hasBar
                hasBar {
                  ...B1
                }
                ...F2
              }
            }
          }
        }

        fragment F2 on Foo {
          fooLabel2
          hasBar {
            barLiteral1
            hasBaz {   # <-- this fragment should get "hasBaz" link for "hasBar" resolver
              edges {
                node {
                  bazLabel1
                  hasFoo {
                    fooLabel2
                  }
                }
              }
            }
            bazesAlias: hasBaz {   # <-- this fragment should get "hasBaz" link for "hasBar" resolver BUT with an alias...
              edges {
                bazAlias : node {
                  bazLabel1
                }
              }
            }
          }
        }

        fragment B1 on Bar {
          barLiteral1
          hasFoo {  # <-- this fragment should get "hasFoo" link for "hasBar" resolver
            fooLabel2
          }
        }
      `),
      modelDefinitionsRegister,
      rootModelDefinition: NotInstantiableDefinitionMock
    });

    expect(fragmentDefinitions).toEqual({
      "notInstantiable": [
        new FragmentDefinition({
          modelDefinition: NotInstantiableDefinitionMock,
          properties: [NotInstantiableDefinitionMock.getProperty("avatar")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")],
          links: [BarDefinitionMock.getLink("hasFoo")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")],
          links: [BarDefinitionMock.getLink("hasFoo")]
        })
      ],
      "notInstantiable-hasFoo": [
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty('fooLabel2')]
        }),
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty('fooLabel2')],
          links: [
            FooDefinitionMock.getLink('hasBar'),
            FooDefinitionMock.getLink('hasBar')
          ]
        })
      ],
      "notInstantiable-hasFoo-hasBar": [
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty('barLiteral1')],
          links: [BarDefinitionMock.getLink("hasFoo")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty('barLiteral1')],
          links: [BarDefinitionMock.getLink("hasBaz"), BarDefinitionMock.getLink("hasBaz")]
        })
      ],
      "notInstantiable-hasFoo-hasBar-hasFoo": [
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty('fooLabel2')]
        })
      ],
      "notInstantiable-hasFoo-hasBar-bazesAlias": [
        new FragmentDefinition({
          modelDefinition: BazDefinitionMock,
          properties: [BazDefinitionMock.getProperty('bazLabel1')],
        })
      ],
      "notInstantiable-hasFoo-hasBar-hasBaz": [
        new FragmentDefinition({
          modelDefinition: BazDefinitionMock,
          properties: [BazDefinitionMock.getProperty('bazLabel1')],
          links: [BazDefinitionMock.getLink("hasFoo")]
        })
      ],
      "notInstantiable-hasFoo-hasBar-hasBaz-hasFoo": [
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty('fooLabel2')],
        })
      ]
    });
  });

  it('should extract all fragments from returned object on mutation', async () => {
    let fragmentDefinitions = getFragmentDefinitionsRegisterFromGqlInfo({
      info: gqlQueryToResolveInfo(gql`
        mutation {
          createBar {
            updatedObject {
              barLiteral1
              hasBaz{edges{node{
                bazLabel1
                hasFoo {
                  ...F2
                }
              }}}
            }
          }
        }

        fragment F2 on Foo {
          fooLabel2
        }
      `),
      modelDefinitionsRegister,
      rootModelDefinition: BarDefinitionMock
    });

    expect(fragmentDefinitions).toEqual({
      "updatedObject": [
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")],
          links: [BarDefinitionMock.getLink("hasBaz")]
        })
      ],
      "updatedObject-hasBaz": [
        new FragmentDefinition({
          modelDefinition: BazDefinitionMock,
          properties: [BazDefinitionMock.getProperty('bazLabel1')],
          links: [BazDefinitionMock.getLink('hasFoo')]
        })
      ],
      "updatedObject-hasBaz-hasFoo": [
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty('fooLabel2')]
        })
      ]
    });
  });

  it('should extract all fragments from returned edge on mutation', async () => {
    let fragmentDefinitions = getFragmentDefinitionsRegisterFromGqlInfo({
      info: gqlQueryToResolveInfo(gql`
        mutation {
          createBar {
            createdEdge {
              node {
                barLiteral1
                hasBaz{edges{node{
                  bazLabel1
                  hasFoo {
                    ...F2
                  }
                }}}
              }
            }
          }
        }

        fragment F2 on Foo {
          fooLabel2
        }
      `),
      modelDefinitionsRegister,
      rootModelDefinition: BarDefinitionMock
    });

    expect(fragmentDefinitions).toEqual({
      "node": [
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")],
          links: [BarDefinitionMock.getLink("hasBaz")]
        })
      ],
      "node-hasBaz": [
        new FragmentDefinition({
          modelDefinition: BazDefinitionMock,
          properties: [BazDefinitionMock.getProperty('bazLabel1')],
          links: [BazDefinitionMock.getLink('hasFoo')]
        })
      ],
      "node-hasBaz-hasFoo": [
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty('fooLabel2')]
        })
      ]
    });
  });

});
