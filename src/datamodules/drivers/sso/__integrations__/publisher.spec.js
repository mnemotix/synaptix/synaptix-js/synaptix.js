/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import Client from '../SSOApiClient';

process.env.OAUTH_BASE_URL = "http://192.168.99.100:8080";

const REALMS = 'integration_tests';
const CLIENT = 'test';

const client = new Client(REALMS, CLIENT);

describe("Keycloak client integration tests", () => {
  beforeEach((done) => {
    client.loginUser('admin', 'admin')
      .then(data => {
        client.refreshToken(data.access_token);
        done();
      })
      .catch(err => {
        console.log(err);
        done();
      });
  });


  it('gets clients', (done) => {
    client.getClients()
      .then(clients => {
        expect(clients.length).toEqual(6);
        done();
      });
  });

  let availableUsers;

  it('gets users', (done) => {
    client.getUsers()
      .then(users => {
        availableUsers = users;

        expect(users.length).toEqual(2);
        done();
      });
  });

  it('gets a user', (done) => {
    client.getUser(availableUsers[0].id)
      .then(user => {
        expect(user.id).toEqual(availableUsers[0].id);
        done();
      });
  });

  it('tests that is user admin', (done) => {
    client.isUserAdmin(availableUsers[0].id)
      .then(isAdmin => {
        expect(isAdmin).toEqual(true);
        done();
      });
  });

  it('tests that user not admin', (done) => {
    client.isUserAdmin(availableUsers[1].id)
      .then(isAdmin => {
        expect(isAdmin).toEqual(false);
        done();
      });
  });

  it('get avalailable roles for client', (done) => {
    client.getAvailableRolesForClient(availableUsers[0].id, CLIENT)
      .then(roles => {
        expect(roles.length).toEqual(1);
        done();
      });
  });

  it('gets available roles for client', (done) => {
    client.getAvailableRolesForClient(availableUsers[0].id, CLIENT)
      .then(roles => {
        expect(roles.length).toEqual(1);
        done();
      });
  });

  it('gets available roles for realm for admin user', (done) => {
    client.getAvailableRolesForRealm(availableUsers[0].id)
      .then(roles => {
        expect(roles.length).toEqual(2);
        done();
      });
  });

  it('gets available roles for realm for guest user', (done) => {
    client.getAvailableRolesForRealm(availableUsers[1].id)
      .then(roles => {
        expect(roles.length).toEqual(1);
        done();
      });
  });

  it('sets a role to user', (done) => {
    client.setRealmRoleToUser(availableUsers[0].id, 'guest')
      .then(() => {
        client.getAvailableRolesForRealm(availableUsers[0].id)
          .then(roles => {
            expect(roles.length).toEqual(1);
            done();
          });
      });
  });

  it('try to set twice a role to user', (done) => {
    client.setRealmRoleToUser(availableUsers[0].id, 'guest')
      .then(() => {
        fail("Should not pas here");
      })
      .catch(done);
  });

  it('remove a role to user', (done) => {
    client.removeRealmRoleForUser(availableUsers[0].id, 'guest')
      .then(() => {
        client.getAvailableRolesForRealm(availableUsers[0].id)
          .then(roles => {
            expect(roles.length).toEqual(2);
            done();
          });
      });
  });

  it('sets a user as admin', (done) => {
    client.setAdminRoleToUser(availableUsers[1].id)
      .then(() => {
        client.isUserAdmin(availableUsers[1].id)
          .then(isAdmin => {
            expect(isAdmin).toEqual(true);
            done();
          });
      });
  });

  it('remove a user as admin', (done) => {
    client.removeAdminRoleForUser(availableUsers[1].id)
      .then(() => {
        client.isUserAdmin(availableUsers[1].id)
          .then(isAdmin => {
            expect(isAdmin).toEqual(false);
            done();
          });
      });
  });

  var createdUser;

  it('adds a user with role but not admin', (done) => {
    client.createUser('blabla@new.fr', {
      email: 'blabla@new.fr',
      firstName: 'Jean',
      lastName: 'Claude'
    }, '1234', ['guest'])
      .then(resp => client.getUserByUsername('blabla@new.fr')
        .then(user => {
          createdUser = user;
          return client.isUserAdmin(createdUser.id)
        })
        .then(isAdmin => {
          expect(isAdmin).toEqual(false);
          return client.getRolesForRealm(createdUser.id)
        })
        .then(roles => {
          expect(roles.find(role => role.name === 'guest')).toBeTruthy();
          done();
        })
        .catch(err => {
          console.log(err);
          done();
        }));
  });


  it('removes a user', (done) => {
    client.removeUser(createdUser.id)
      .then(resp => {
        return client.getUserByUsername('blabla@new.fr')
      })
      .then(user => {
        fail('Should not pass here');
        done();
      })
      .catch(err => done());
  });

  var createdUser;

  it('adds a user with admin creadentials', (done) => {
    client.createUser('blabla@new.fr', {
      email: 'blabla@new.fr',
      firstName: 'Jean',
      lastName: 'Claude'
    }, '1234', null, true)
      .then(resp => client.getUserByUsername('blabla@new.fr')
        .then(user => {
          createdUser = user;
          return client.isUserAdmin(createdUser.id)
        })
        .then(isAdmin => {
          expect(isAdmin).toEqual(true);
          done();
          //return client.removeUser(createdUser.id)
        })
        .then(() => done())
        .catch(err => {
          console.log(err);
          done();
        }));
  });
});